# gwenevieve
A very simple image viewer for the Nokia Internet Tablet N800

There was no image viewer available for the Nokia Internet Tablet which was able to handle a large amount of files, so I wrote this one for myself.

The project should build under Linux and Microsoft Windows(TM) as well, but this only makes sense for development purposes since this is an application which mainly deals with limitations of the Nokia Internet Tablet, for example storing a file list because of slow scanning of directories.

Be aware that the source code is very poorly commented.
