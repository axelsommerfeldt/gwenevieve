# Makefile for Linux and Maemo
# Copyright (C) 2010-2013 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)
#
# --------------------------------------------------------------------
#
# This file is part of the Gwenevieve application.
#
# Gwenevieve is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gwenevieve is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
#
# --------------------------------------------------------------------
#
# Available actions:
# make            - will build Gwenevieve
# make gwenevieve - will build Gwenevieve, too
# make run        - will run Gwenevieve
# make install    - will install Gwenevieve
# make deb        - will build a distribution package of Gwenevieve
# make tar        - will archive the directory content into ../gwenevieve.tar.gz
# make clean      - cleanup
#
# --------------------------------------------------------------------

# Log level:
# 0 = upnp_error + upnp_critical
# 1 = + ASSERT
# 2 = + TRACE
LOGLEVEL = 0

# If we run inside a Scratchbox environment, we will make a Maemo binary/package by default
ifdef SBOX_UNAME_MACHINE
MAEMO = 1
else
MAEMO = 0
endif

# Osso service, needed for Maemo
OSSO_SERVICE = de.sommerfeldt.gwenevieve

# Directories
BIN = $(DESTDIR)/usr/bin
SHARE = $(DESTDIR)/usr/share
APPLICATIONS = $(DESTDIR)/usr/share/applications
DBUS_SERVICES = $(DESTDIR)/usr/share/dbus-1/services
ICONS = $(DESTDIR)/usr/share/icons/hicolor

# Compiler flags
CFLAGS = `pkg-config gtk+-2.0 gthread-2.0 --cflags --libs` \
	-Wall -pedantic -DLOGLEVEL=$(LOGLEVEL)
ifneq ($(MAEMO),0)
CFLAGS += `pkg-config hildon-1 hildon-fm-2 libosso --cflags --libs` \
	-DMAEMO -DOSSO_SERVICE=\"$(OSSO_SERVICE)\"
endif
ifndef SBOX_UNAME_MACHINE
CFLAGS += -Wno-overlength-strings -Wno-format-security
endif

# Source files
SRC = src/gwenevieve.c src/database.c \
	src/selection.c src/viewer.c src/list.c \
	src/about.c src/settings.c \
	src/hildon_gtk.c src/debug.c

# Header files
HDR = src/gwenevieve.h src/hildon_gtk.h src/debug.h

# make gwenevieve
gwenevieve: $(SRC) $(HDR)
	gcc -o gwenevieve $(SRC) $(CFLAGS)
	strip gwenevieve

# make all
.PHONY:	all
all:	gwenevieve

# make clean
.PHONY: clean
clean:
	rm -fr bin obj gwenevieve debian/gwenevieve core

# make run
.PHONY: run
run:	gwenevieve
ifdef SBOX_UNAME_MACHINE
	run-standalone.sh ./gwenevieve
else
	./gwenevieve
endif

# make tar
.PHONY: tar
tar:	clean
	-rm -f ../gwenevieve.tar.gz
	tar cvfz ../gwenevieve.tar.gz *

# make install
.PHONY: install
install: gwenevieve
ifeq ($(MAEMO),0)
	install -d $(BIN) $(APPLICATIONS) \
	           $(ICONS)/16x16/apps $(ICONS)/26x26/apps $(ICONS)/32x32/apps \
	           $(ICONS)/40x40/apps $(ICONS)/48x48/apps $(ICONS)/64x64/apps \
	           $(ICONS)/scalable/apps
	install -m 755 ./gwenevieve $(BIN)
#	install -m 644 ./share/* $(SHARE)/gwenevieve
	install -m 644 ./gwenevieve.desktop $(APPLICATIONS)
	install -m 644 ./icons/16x16/gwenevieve.png $(ICONS)/16x16/apps
	install -m 644 ./icons/26x26/gwenevieve.png $(ICONS)/26x26/apps
	install -m 644 ./icons/32x32/gwenevieve.png $(ICONS)/32x32/apps
	install -m 644 ./icons/40x40/gwenevieve.png $(ICONS)/40x40/apps
	install -m 644 ./icons/48x48/gwenevieve.png $(ICONS)/48x48/apps
	install -m 644 ./icons/64x64/gwenevieve.png $(ICONS)/64x64/apps
	install -m 644 ./icons/scalable/gwenevieve.png $(ICONS)/scalable/apps
else
	install -d $(BIN) $(APPLICATIONS)/hildon $(DBUS_SERVICES) \
	           $(ICONS)/26x26/hildon $(ICONS)/40x40/hildon $(ICONS)/scalable/hildon
	install -m 755 ./gwenevieve $(BIN)
#	install -m 644 ./share/* $(SHARE)/gwenevieve
	install -m 644 ./gwenevieve.desktop $(APPLICATIONS)/hildon
	install -m 644 ./$(OSSO_SERVICE).service $(DBUS_SERVICES)
	install -m 644 ./icons/26x26/gwenevieve.png $(ICONS)/26x26/hildon
	install -m 644 ./icons/40x40/gwenevieve.png $(ICONS)/40x40/hildon
	install -m 644 ./icons/scalable/gwenevieve.png $(ICONS)/scalable/hildon
endif

# make deb
.PHONY: deb
deb:	clean
	dpkg-buildpackage -rfakeroot

