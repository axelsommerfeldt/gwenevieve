/*
	debug.h
	Copyright (C) 2008-2013 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)

	--------------------------------------------------------------------

	This file is part of the Gwenevieve application.

	Gwenevieve is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Gwenevieve is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <glib.h>
#include <stdio.h>

/*-----------------------------------------------------------------------------------------------*/

#if LOGLEVEL
#define ASSERT(a) g_assert(a)
#define VERIFY(a) g_assert(a)
#else
#define ASSERT(a)
#define VERIFY(a) a
#endif

#if LOGLEVEL >= 2
#define TRACE(a) g_print a
#else
#define TRACE(a)
#endif

/*-----------------------------------------------------------------------------------------------*/

void debug_init( const gchar *basename, const gchar *log_domain, ... );
void debug_exit( gboolean remove_logfile );

/*-----------------------------------------------------------------------------------------------*/

FILE *fopen_utf8( const char *filename, const char *mode );

/*-----------------------------------------------------------------------------------------------*/
