/*
	gwenevieve.h
	Copyright (C) 2010-2013 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)

	--------------------------------------------------------------------

	This file is part of the Gwenevieve application.

	Gwenevieve is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Gwenevieve is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
*/

#define G_LOG_DOMAIN "Gwenevieve"

#include "hildon_gtk.h"
#include "debug.h"

#define NAME      "Gwenevieve"
#define VERSION   "0.4.3" /*" (" __DATE__ ")"*/
#define COPYRIGHT "Copyright (C) 2010-2023 Axel Sommerfeldt"

/*-----------------------------------------------------------------------------------------------*/
/* gwenevieve.c */

extern GtkWindow *MainWindow;   /* TODO! */
extern GtkStatusbar *Statusbar;
extern guint StatusbarContext;
extern GtkMenuItem *StatusMenuItem;

extern GtkNotebook *notebook;
extern gint selection_page, viewer_page, list_page;
extern gboolean sorting_in_progress;

extern const char go_back[]      /*= "- Go Back -"*/;
extern const char all_pictures[] /*= "- All Pictures -"*/;

void handle_error( GError *error, const gchar *format, ... );
gboolean quit( void );

struct settings;
gboolean needs_fullscreen( struct settings *settings );
void     set_fullscreen( gboolean fullscreen );

/*-----------------------------------------------------------------------------------------------*/
/* database.c */

struct folder  /* or container!? */
{
	struct folder *parent, *children, *next;

	const char *name;
	unsigned num_files;
	int index;

	void *user_data;
};

struct file    /* or item!? */
{
	struct folder *folder;

	const char *name;
	unsigned long size;
	time_t mtime;
};

enum database_location { NO_DB = 0, DB_IN_USER_CONFIG_DIR, DB_IN_PATH };
enum database_sorting { SORT_BY_NAME = 0, SORT_BY_SIZE, SORT_BY_DATE, SHUFFLE };

gboolean open_database( GError **error );
gboolean close_database( GError **error );
void discard_database( void );
gboolean database_is_open( void );

enum database_sorting get_database_sorting( void );
void set_database_sorting( enum database_sorting sorting );

struct folder *get_root_folder( void );
struct folder *get_first_folder( const struct folder *node );
struct folder *get_next_folder( const struct folder *node, const struct folder *folder );
unsigned get_num_folder( const struct folder *node );

struct file *get_first_file( const struct folder *folder, gboolean including_sub_folder );
struct file *get_last_file( const struct folder *folder, gboolean including_sub_folder );
struct file *get_previous_file( const struct folder *folder, gboolean including_sub_folder, const struct file *file );
struct file *get_next_file( const struct folder *folder, gboolean including_sub_folder, const struct file *file );
struct file *get_random_file( const struct folder *folder, gboolean including_sub_folder );
/*unsigned get_num_files( const struct folder *folder, gboolean including_sub_folder );*/

gpointer get_file_id( const struct file *file );
struct file *get_file_by_id( gpointer id );

gchar *build_filename( const struct file *file );
/*struct file *find_file( const struct folder *folder, const char *file_name );*/
void remove_file( struct file *file );

gchar *get_folder_name( const struct folder *node, const struct folder *folder, const char *separator );
gchar *get_file_name( const struct folder *node, const struct file *file );
char  *_get_file_name( char *buffer, size_t bufsize, const struct folder *node, const struct file *file );

struct database_contents;

struct database_contents *new_database_contents( void );
void append_database_contents( struct database_contents *contents, const char *folder, const char *name, unsigned long size, time_t mtime );
gboolean set_database_contents( struct database_contents *contents, GError **error );
void free_database_contents( struct database_contents *contents );

gboolean database_file_exists( const gchar *path, enum database_location location );
void remove_database_file( const gchar *path, enum database_location location );

/*-----------------------------------------------------------------------------------------------*/
/* selection.c */

extern const struct folder *selected_folder;
extern gboolean selected_subfolder;

GtkWidget *create_selection( void );
void set_selection_page( void );
void set_selection_title( void );
void reset_selection( void );
void update_selection( void );
gboolean selection_key_press_event( GtkWidget *widget, GdkEventKey *event );

/*-----------------------------------------------------------------------------------------------*/
/* viewer.c */

extern const struct file *viewer_image;
extern gint viewer_image_width, viewer_image_height;

GtkWidget *create_viewer( void );
void set_viewer_page( void );
void set_viewer_cursor( void );
void clear_viewer( void );
void invalidate_viewer( void );
void update_viewer( const struct file *image );
void update_viewer_pixbuf( const struct file *image );
gboolean viewer_key_press_event( GtkWidget *widget, GdkEventKey *event );

struct file *get_viewer_image( void );
void viewer_image_removed( const struct file *image );

GdkPixbuf *new_pixbuf_from_image( const struct file *image, GError **pixbuf_error );
void set_viewer_image( const struct file *image );
void set_viewer_pixbuf( GdkPixbuf *pixbuf );

void start_slide_show( void );
gboolean stop_slide_show( gboolean ask );

void clear_viewer_cache( void );
GdkPixbuf *take_from_viewer_cache( const struct file *image );
void add_to_viewer_cache( const struct file *image, GdkPixbuf *pixbuf );

/*-----------------------------------------------------------------------------------------------*/
/* list.c */

GtkWidget *create_list( void );
void set_list_page( void );
void clear_list( void );
void update_list( void );
gboolean list_key_press_event( GtkWidget *widget, GdkEventKey *event );

struct file *get_list_image( void );
void list_image_removed( struct file *image );

/*-----------------------------------------------------------------------------------------------*/
/* about.c */

void about( GdkPixbuf *logo );

/*-----------------------------------------------------------------------------------------------*/
/* settings.c */

extern struct settings
{
	struct screen_settings
	{
		gint width, height;
	} screen;

	struct database_settings
	{
		gchar *path;
		enum database_location location;
		enum database_sorting sorting;
		gint offer_all;
		gboolean linear_mode;
		gboolean random_start;
	} db;

	struct view_settings
	{
		gint width, height;
		gboolean auto_rotate;
		gboolean scale_up;
		gboolean scroll_zone;
		gboolean wrap_around;
		gint cache_size;
		gboolean cache_prefetch;
		gint pause_between_slides;
	} view;

} *settings;

gboolean load_settings( void );
gboolean save_settings( void );
gchar *build_user_config_filename( const char *filename );

gboolean options( void );

/*--- EOF ---------------------------------------------------------------------------------------*/
