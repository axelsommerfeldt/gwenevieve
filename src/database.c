/*
	database.c
	Copyright (C) 2010-2013 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)

	--------------------------------------------------------------------

	This file is part of the Gwenevieve application.

	Gwenevieve is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Gwenevieve is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gwenevieve.h"

#define INITIAL_DATABASE_SIZE  (64*1024)
#define INITIAL_FILENAME_SIZE   256

/*-----------------------------------------------------------------------------------------------*/

static const char db_header_1[] = "[Gwenevieve Database]\n";
static const char db_header_2[] = "                     \n";

static gchar *db_path, *db_filename, *db_contents;
static struct folder *db_folder;
static struct file *db_files;
static enum database_sorting db_sorting;
static gboolean db_dirty;

void index_folder( void );
void sort_files( void );

struct folder *_get_first_folder( void );
struct folder *_get_next_folder( const struct folder *folder );

struct file *first_file( void );
struct file *end_of_files( void );
struct file *next_file( const struct file *file );

static gchar *finalize_database_contents( struct database_contents *contents );

/*-----------------------------------------------------------------------------------------------*/

static int compare_names( const char *name1, const char *name2 )
{
	int result = 0;

	for (;;)
	{
		char c1, c2;

		c1 = *name1++;
		c2 = *name2++;

		if ( c1 >= '0' && c1 <= '9' && c2 >= '0' && c2 <= '9' )
		{
			/* Compare numbers */
			unsigned long l1 = strtoul( --name1, (char **)&name1, 10 );
			unsigned long l2 = strtoul( --name2, (char **)&name2, 10 );
			if ( l1 < l2 ) return -1;
			if ( l1 != l2 ) return 1;
		}
		else if ( c1 != c2 )
		{
			/* Treat '.' < ' ', so "a.png" < "a-2.png" */
			if ( c1 == '.' ) return -1;
			if ( c2 == '.' ) return 1;

			/* If the names are the same except case, the first different character counts */
			if ( result == 0 ) result = c1 - c2;

			/* Compare characters case-insensitive */
			/* Note: ' ' will get '\0' here, so we have to test for '\0' before */
			if ( c1 == '\0' || c2 == '\0' ) break;
			c1 &= ~0x20;
			c2 &= ~0x20;
			if ( c1 != c2 ) return c1 - c2;
		}
		else if ( c1 == '\0' )
		{
			break;
		}
	}

	return result;
}

static gboolean set_database_from_contents( GError **error )
{
	char *s = db_contents;
	guint n;

	TRACE(( "-> set_database_from_contents()\n" ));
	ASSERT( db_contents != NULL );
	ASSERT( db_folder == NULL && db_files == NULL );

	if ( s != NULL && strncmp( s, db_header_1, strlen( db_header_1 ) ) == 0 )  /* Header ok */
	{
		struct folder *folder;
		struct file *file;

		/* skip header */
		s += strlen( db_header_1 );

		/* get total number of files */
		n = strtoul( s, &s, 10 );
		TRACE(( "set_database_from_contents(): n = %u\n", n ));
		s = strchr( s, '\n' );
		s++;

		/* allocate start-marker + files + end-marker */
		db_files = g_new0( struct file, n + 2 );

		/* allocate root folder */
		TRACE(( "set_database_from_contents(): g_new0( \"%s\" )\n", "" ));
		db_folder = g_new0( struct folder, 1 );
		db_folder->name = "";

		folder = db_folder;   /* current folder */
		file = db_files + 1;  /* current file */

		for (;;)
		{
			char *t = strchr( s, '\n' );
			if ( t == NULL ) break;
			*t++ = '\0';

			if ( *s == '*' )  /* folder? */
			{
				s++;  /* skip asterisk */

				for ( folder = get_root_folder();; )
				{
					struct folder *d;

					/* get folder name */
					char *t = s + strcspn( s, "/\\" );
					if ( *t != '\0' ) *t++ = '\0';

					/* finished? */
					if ( *s == '\0' || strcmp( s, "." ) == 0 ) break;

					/* do we already have an appropriate folder struct? */
					for ( d = folder->children; d != NULL; d = d->next )
					{
						if ( strcmp( d->name, s ) == 0 ) break;  /* yes! */
					}
					if ( d == NULL )
					{
						struct folder *prev, *l;

						/* no: allocate and fill it */
						TRACE(( "set_database_from_contents(): g_new0( \"%s\" )\n", s ));
						d = g_new0( struct folder, 1 );

						d->name = s;
						d->parent = folder;

						/* put it into the (sorted) children list */
						prev = NULL;
						for ( l = folder->children; l != NULL; l = l->next )
						{
							if ( compare_names( d->name, l->name ) < 0 ) break;
							prev = l;
						}

						if ( prev == NULL )
						{
							folder->children = d;
						}
						else
						{
							ASSERT( prev->next == l );
							prev->next = d;
						}
						d->next = l;
					}

					folder = d;
					s = t;
				}
			}
			else
			{
				/* fill file struct */

				file->folder = folder;
				file->name = s;

				s = strchr( s, '\t' );
				if ( s != NULL )
				{
					*s++ = '\0';
					file->size = strtoul( s, NULL, 10 );

					s = strchr( s, '\t' );
					if ( s != NULL )
					{
						*s++ = '\0';
						file->mtime = (time_t)strtoul( s, NULL, 16 );
					}
				}

				file++;
				folder->num_files++;
			}

			s = t;
		}

		index_folder();
		sort_files();
		db_dirty = FALSE;

		TRACE(( "<- set_database_from_contents() = TRUE\n" ));
		return TRUE;
	}
	else
	{
		g_set_error( error, 0, 0, "Invalid database format" );

		TRACE(( "<- set_database_from_contents() = FALSE\n" ));
		return FALSE;
	}
}

static gchar *get_database_filename( const gchar *path, enum database_location location )
{
	if ( location == DB_IN_USER_CONFIG_DIR )
		return build_user_config_filename( "gwenevieve.dat" );
	else if ( location == DB_IN_PATH )
		return g_build_filename( path, ".gwenevieve", NULL );
	else
		return NULL;
}

gboolean open_database( GError **error )
{
	TRACE(( "-> open_database()\n" ));

	discard_database();

	db_path = g_strdup( settings->db.path );

	db_filename = get_database_filename( settings->db.path, settings->db.location );
	if ( db_filename == NULL )
	{
		/* nothing to do */
		return TRUE;
	}
	else if ( g_file_get_contents( db_filename, &db_contents, NULL, error ) )
	{
		if ( set_database_from_contents( error ) )
		{
			TRACE(( "<- open_database() = TRUE\n" ));
			return TRUE;
		}
	}

	TRACE(( "<- open_database() = FALSE\n" ));
	return FALSE;
}

gboolean close_database( GError **error )
{
	gboolean success = TRUE;

	TRACE(( "-> close_database(): dirty = %s\n", db_dirty ? "TRUE" : "FALSE" ));

	if ( settings->db.location != NO_DB && db_dirty )
	{
		struct database_contents *contents = new_database_contents();
		struct folder *folder;
		struct file *file;
		gchar *str;

		for ( folder = get_first_folder( NULL ); folder != NULL; folder = get_next_folder( NULL, folder ) )
		{
			gchar *folder_name = get_folder_name( NULL, folder, G_DIR_SEPARATOR_S );

			for ( file = get_first_file( folder, FALSE ); file != NULL; file = get_next_file( folder, FALSE, file ) )
			{
				append_database_contents( contents, folder_name, file->name, file->size, file->mtime );
			}

			g_free( folder_name );
		}

		str = finalize_database_contents( contents );

		ASSERT( db_filename != NULL );
		TRACE(( "close_database(): g_file_set_contents( \"%s\" )...\n", db_filename ));
		success = g_file_set_contents( db_filename, str, -1, error );

		g_free( str );
		free_database_contents( contents );
	}

	discard_database();

	TRACE(( "<- close_database() = %s\n", (success) ? "TRUE" : "FALSE" ));
	return success;
}

static void free_folder( struct folder *node )
{
	struct folder *folder;

	ASSERT( node != NULL );

	for ( folder = get_first_folder( node ); folder != NULL; )
	{
		struct folder *next = get_next_folder( node, folder );
		free_folder( folder );
		folder = next;
	}

	TRACE(( "free_folder(): g_free( \"%s\" )\n", node->name ));
	g_free( node->user_data );
	g_free( node );
}

void discard_database( void )
{
	TRACE(( "-> discard_database()\n" ));

	if ( db_folder != NULL ) free_folder( db_folder );
	db_folder = NULL;

	g_free( db_files );
	db_files = NULL;

	g_free( db_contents );
	db_contents = NULL;

	g_free( db_filename );
	db_filename = NULL;

	g_free( db_path );
	db_path = NULL;

	db_dirty = FALSE;

	TRACE(( "<- discard_database()\n" ));
}

gboolean database_is_open( void )
{
	return db_path != NULL;
}
/*-----------------------------------------------------------------------------------------------*/

#define LCG(X) (1103515245 * (X) + 12345)  /* See http://en.wikipedia.org/wiki/Linear_congruential_generator */
#define LCG_INIT() time( NULL )
#define LCG_RANDOM(X,n) (((X = LCG( X )) >> 8) % n)

#ifdef COMPARE_FOLDER_FUNC  /* works ok, but is rather slow, so we invented index_folder() instead */

static int compare_folder( const struct folder *a, const struct folder *b )
{
	if ( a != b )
	{
		const struct folder *i;

		for ( i = _get_first_folder(); i != NULL; i = _get_next_folder( i ) )
		{
			if ( i == a ) return -1;
			if ( i == b ) return 1;
		}

		ASSERT( FALSE );  /* should never happen */
	}

	return 0;
}

#else

void index_folder( void )
{
	struct folder *folder;
	int index = 0;

	for ( folder = _get_first_folder(); folder != NULL; folder = _get_next_folder( folder ) )
		folder->index = index++;
}

#define compare_folder( a, b ) ((a)->index - (b)->index)

#endif

static int compare_files( const struct file *a, const struct file *b )
{
	int x = 0;

	switch ( db_sorting )
	{
	case SORT_BY_SIZE:
		if ( a->size < b->size )
			x = -1;
		else if ( a->size > b->size )
			x = 1;
		break;

	case SORT_BY_DATE:
		if ( a->mtime < b->mtime )
			x = -1;
		else if ( a->mtime > b->mtime )
			x = 1;
		break;
	}

	if ( x == 0 )
	{
		x = compare_folder( a->folder, b->folder );
		if ( x == 0 ) x = compare_names( a->name, b->name );
	}

	return x;
}

void sort_files( void )
{
	struct file *files, *first;
	unsigned n;  /* the number of files left to sort or shuffle */

	first = first_file();
	n     = end_of_files() - first;

	/* prevent access during sorting */
	files = db_files;
	db_files = NULL;
	if ( files == NULL ) return;  /* nothing to do */

	if ( db_sorting == SHUFFLE )
	{
		/* Shuffle: taken from http://en.wikipedia.org/wiki/Fisher-Yates_shuffle */
		unsigned X = (unsigned)LCG_INIT();            /* i.e., java.util.Random. */

		while ( n > 1 )
		{
			guint k = (guint)LCG_RANDOM( X, n );              /* 0 <= k < n. */
			n--;                       /* n is now the last pertinent index; */

			/* swap array[n] with array[k] (does nothing if k == n). */
			if ( k != n )
			{
				struct file *l = first + k;
				struct file *m = first + n;

				struct file t = *l;
				*l = *m;
				*m = t;
			}
		}
	}
	else
	{
		qsort( first, n, sizeof( struct file ), (int (*)(const void *, const void *))compare_files );
	}

	db_files = files;
}

enum database_sorting get_database_sorting( void )
{
	return db_sorting;
}

void set_database_sorting( enum database_sorting sort )
{
	if ( db_sorting != sort )
	{
		db_sorting = sort;
		sort_files();
	}
}

/*-----------------------------------------------------------------------------------------------*/

struct folder *_get_first_folder( void )
{
	return db_folder;
}

struct folder *_get_next_folder( const struct folder *folder )
{
	ASSERT( folder != NULL );

	if ( folder->children != NULL )
	{
		folder = folder->children;
	}
	else
	{
		do
		{
			if ( folder->next != NULL )
			{
				folder = folder->next;
				break;
			}

			folder = folder->parent;
		}
		while ( folder != NULL );
	}

	return (struct folder *)folder;
}

struct folder *get_root_folder( void )
{
	return db_folder;
}

struct folder *get_first_folder( const struct folder *node )
{
	if ( node == NULL )  /* linear mode */
	{
		const struct folder *folder = get_root_folder();
		while ( folder != NULL && folder->num_files == 0 )
			folder = _get_next_folder( folder );
		return (struct folder *)folder;
	}

	return node->children;
}

struct folder *get_next_folder( const struct folder *node, const struct folder *folder )
{
	ASSERT( folder != NULL );
	if ( folder == NULL ) return NULL;

	if ( node == NULL )  /* linear mode */
	{
		do folder = _get_next_folder( folder );
		while ( folder != NULL && folder->num_files == 0 );
		return (struct folder *)folder;
	}

	ASSERT( folder->parent == node );
	return folder->next;
}

#if 0

static unsigned _get_num_folder( const struct folder *node, gboolean linear_mode )
{
	const struct folder *folder;
	int n = 0;

	for ( folder = node->children; folder != NULL; folder = folder->next )
	{
		if ( !linear_mode || folder->num_files > 0 ) n++;
		n += _get_num_folder( folder, linear_mode );
	}

	return n;
}

unsigned get_num_folder( const struct folder *node, gboolean including_sub_folder )
{
	gboolean linear_mode = FALSE;
	unsigned n = 0;

	if ( node == NULL )  /* linear mode */
	{
		node = _get_first_folder();
		if ( node == NULL ) return 0;

		if ( node->num_files > 0 ) n++;  /* count root folder */
		linear_mode = including_sub_folder = TRUE;
	}

	/* count children */
	if ( including_sub_folder )
	{
		n += _get_num_folder( node, linear_mode );
	}
	else
	{
		const struct folder *folder;

		/* count children */
		n = 0;
		for ( folder = node->children; folder != NULL; folder = folder->next )
			n++;
	}

	return n;
}

#else

unsigned get_num_folder( const struct folder *node )
{
	const struct folder *folder;
	unsigned n = 0;

	for ( folder = get_first_folder( node ); folder != NULL; folder = get_next_folder( node, folder ) )
		n++;

	return n;
}

#endif

/*-----------------------------------------------------------------------------------------------*/

struct file *first_file( void )
{
	struct file *file = db_files;

	if ( file == NULL ) return NULL;
	return ++file;
}

struct file *end_of_files( void )
{
	struct file *file = db_files;

	if ( file == NULL ) return NULL;

	for (;;)
	{
		file++;
		if ( file->folder == NULL ) break;  /* reached end of files */
	}

	return file;
}

struct file *next_file( const struct file *file )
{
	ASSERT( file != NULL );

	if ( (++file)->folder == NULL ) file = NULL;  /* reached end of files */
	return (struct file *)file;
}

struct file *get_first_file( const struct folder *folder, gboolean including_sub_folder )
{
	const struct file *file = db_files;

	if ( file == NULL ) return NULL;
	return get_next_file( folder, including_sub_folder, file );
}

struct file *get_last_file( const struct folder *folder, gboolean including_sub_folder )
{
	const struct file *file = end_of_files();

	if ( file == NULL ) return NULL;
	return get_previous_file( folder, including_sub_folder, file );
}

struct file *get_previous_file( const struct folder *folder, gboolean including_sub_folder, const struct file *file )
{
	ASSERT( file != NULL );

	if ( file != NULL ) for (;;)
	{
		file--;
		if ( file->folder == NULL ) break;  /* reached start of files */

		if ( folder == NULL )  /* linear mode */
			return (struct file *)file;

		if ( including_sub_folder )
		{
			const struct folder *file_folder;

			for ( file_folder = file->folder; file_folder != NULL; file_folder = file_folder->parent )
			{
				if ( file_folder == folder )
					return (struct file *)file;
			}
		}
		else
		{
			if ( file->folder == folder )
				return (struct file *)file;
		}
	}

	return NULL;
}

struct file *get_next_file( const struct folder *folder, gboolean including_sub_folder, const struct file *file )
{
	ASSERT( file != NULL );

	if ( file != NULL ) for (;;)
	{
		file++;
		if ( file->folder == NULL ) break;  /* reached end of files */

		if ( folder == NULL )  /* linear mode */
			return (struct file *)file;

		if ( including_sub_folder )
		{
			const struct folder *file_folder;

			for ( file_folder = file->folder; file_folder != NULL; file_folder = file_folder->parent )
			{
				if ( file_folder == folder )
					return (struct file *)file;
			}
		}
		else
		{
			if ( file->folder == folder )
				return (struct file *)file;
		}
	}

	return NULL;
}

struct file *get_random_file( const struct folder *folder, gboolean including_sub_folder )
{
	struct file *image = get_first_file( folder, including_sub_folder );
	if ( image != NULL )
	{
		static unsigned _X = 0;

		unsigned n = end_of_files() - first_file();   /* Total number of images */
		unsigned X = (unsigned)LCG_INIT() + _X++;    /* i.e., java.util.Random. */
		unsigned k = (unsigned)LCG_RANDOM( X, n );               /* 0 <= k < n. */
		unsigned i;

		for ( i = 1; i <= k; i++ )
		{
			image = get_next_file( folder, including_sub_folder, image );
			if ( image == NULL )
			{
				image = get_first_file( folder, including_sub_folder );
				k = i + k % i;
			}
		}
	}

	return image;
}

/*
unsigned get_num_files( const struct folder *folder, gboolean including_sub_folder )
{
	int n;

	n = 0;
	for ( image = get_first_file( folder, including_sub_folder ); image != NULL; image = get_next_file( folder, including_sub_folder, image ) )
		n++;

	return n;
}
*/

gpointer get_file_id( const struct file *file )
{
	/* Since the `name' pointer is unique, we can return this as unique id */
	return ( file != NULL ) ? (gpointer)file->name : NULL;
}

struct file *get_file_by_id( gpointer id )
{
	struct file *file;

	for ( file = first_file(); file != NULL; file = next_file( file ) )
	{
		if ( (gpointer)file->name == id ) return file;  /* found */
	}

	return NULL;  /* not found */
}

/*-----------------------------------------------------------------------------------------------*/

static void __get_folder_name( GString *folder_name, const struct folder *node, const struct folder *folder, const char *separator )
{
	ASSERT( folder->parent != NULL );
	if ( folder->parent != node )
	{
		__get_folder_name( folder_name, node, folder->parent, separator );
		g_string_append( folder_name, separator );
	}

	g_string_append( folder_name, folder->name );
}

static GString *_get_folder_name( const struct folder *node, const struct folder *folder, const char *separator )
{
	GString *folder_name = g_string_sized_new( INITIAL_FILENAME_SIZE );

	if ( node == NULL ) node = get_root_folder();

#if 0
	if ( folder != node ) for (;;)
	{
		g_string_prepend( folder_name, folder->name );
		folder = folder->parent;
		ASSERT( folder != NULL );
		if ( folder == node ) break;
		g_string_prepend( folder_name, separator );
	}
#else
	if ( folder != node ) __get_folder_name( folder_name, node, folder, separator );
#endif

	return folder_name;
}

#if 0
static void __get_file_name( char **buffer, size_t *buflen, const struct folder *node, const struct folder *folder )
{
	size_t len;

	ASSERT( folder->parent != NULL );
	if ( folder->parent != node ) __get_file_name( buffer, buflen, node, folder->parent );

	strncpy( *buffer, folder->name, *buflen )[*buflen] = '\0';
	len = strlen( *buffer );
	*buffer += len;
	*buflen -= len;

	if ( *buflen > 0 )
	{
		*(*buffer)++ = G_DIR_SEPARATOR;
		(*buflen)--;
	}
}
#endif

gchar *build_filename( const struct file *file )
{
	GString *folder_name;
	gchar *result;

	ASSERT( file != NULL );

	folder_name = _get_folder_name( get_root_folder(), file->folder, G_DIR_SEPARATOR_S );
	result = g_build_filename( db_path, folder_name->str, file->name, NULL );
	g_string_free( folder_name, TRUE );

	return result;
}

/*
struct file *find_file( const struct folder *folder, const char *file_name )
{
	const struct file *file;

	for ( file = get_first_file( folder ); file != NULL; file = get_next_file( folder, file ) )
	{
		if ( strcmp( file->name, file_name ) == 0 ) break;
	}

	return file;
}
*/

void remove_file( struct file *file )
{
	struct file *f;

	for ( f = first_file(); f != NULL; f = next_file( f ) )
	{
		if ( f == file )
		{
			struct file *last = end_of_files() - 1;

			f->folder->num_files--;
			memmove( f, f + 1, last - f );
			memset( last, 0, sizeof( struct file ) );

			db_dirty = TRUE;
			break;
		}
	}
}

gchar *get_folder_name( const struct folder *node, const struct folder *folder, const char *separator )
{
	ASSERT( folder != NULL );

	if ( node == NULL ) node = get_root_folder();
	if ( folder == node ) return g_strdup( "." );

	return g_string_free( _get_folder_name( node, folder, separator ), FALSE );
}

gchar *get_file_name( const struct folder *node, const struct file *file )
{
	GString *file_name;

	ASSERT( file != NULL );

	file_name = _get_folder_name( node, file->folder, G_DIR_SEPARATOR_S );
	if ( file_name->len > 0 ) g_string_append_c( file_name, G_DIR_SEPARATOR );
	g_string_append( file_name, file->name );

	return g_string_free( file_name, FALSE );
}

static void __get_file_name( char **buffer, size_t *buflen, const struct folder *node, const struct folder *folder )
{
	size_t len;

	ASSERT( folder->parent != NULL );
	if ( folder->parent != node ) __get_file_name( buffer, buflen, node, folder->parent );

	strncpy( *buffer, folder->name, *buflen )[*buflen] = '\0';
	len = strlen( *buffer );
	*buffer += len;
	*buflen -= len;

	if ( *buflen > 0 )
	{
		*(*buffer)++ = G_DIR_SEPARATOR;
		(*buflen)--;
	}
}

char *_get_file_name( char *buffer, size_t bufsize, const struct folder *node, const struct file *file )
{
	char *s = buffer;
	size_t buflen = bufsize - 1;

	ASSERT( file != NULL );

	if ( node == NULL ) node = get_root_folder();
	if ( file->folder != node ) __get_file_name( &s, &buflen, node, file->folder );
	strncpy( s, file->name, buflen )[buflen] = '\0';

	return buffer;
}

/*-----------------------------------------------------------------------------------------------*/

struct database_contents
{
	GString *str;
	guint n;
};

static gchar *last_folder;

struct database_contents *new_database_contents( void )
{
	struct database_contents *contents = g_new0( struct database_contents, 1 );
	contents->str = g_string_sized_new( INITIAL_DATABASE_SIZE );

	g_free( last_folder ); last_folder = g_strdup( "?" );
	g_string_append( contents->str, db_header_1 );
	g_string_append( contents->str, db_header_2 );

	return contents;
}

void append_database_contents( struct database_contents *contents, const char *folder, const char *name, unsigned long size, time_t mtime )
{
	ASSERT( last_folder != NULL && folder != NULL && name != NULL );

	if ( strcmp( last_folder, folder ) != 0 )
	{
		TRACE(( "*%s\n", folder ));
		g_free( last_folder );
		g_string_append_printf( contents->str, "*%s\n", last_folder = g_strdup( folder ) );
	}

	TRACE(( "%s\t%lu\t%lx\n", name, size, mtime ));
	g_string_append_printf( contents->str, "%s\t%lu\t%lx\n", name, size, mtime );
	contents->n++;
}

gboolean set_database_contents( struct database_contents *contents, GError **error )
{
	gboolean success;

	ASSERT( contents != NULL && contents->str != NULL );

	g_free( last_folder ); last_folder = NULL;
	discard_database();

	db_path = g_strdup( settings->db.path );

	db_filename = get_database_filename( settings->db.path, settings->db.location );
	db_contents = finalize_database_contents( contents );
	success = set_database_from_contents( error );
	if ( success ) db_dirty = TRUE;

	return success;
}

void free_database_contents( struct database_contents *contents )
{
	if ( contents != NULL )
	{
		if ( contents->str != NULL ) g_string_free( contents->str, TRUE );
		g_free( contents );
	}
}

static gchar *finalize_database_contents( struct database_contents *contents )
{
	gchar *result = NULL;

	if ( contents != NULL && contents->str != NULL )
	{
		char buffer[sizeof( db_header_2 )];

		/* patch total number of files into database header */
		sprintf( buffer, "%u", contents->n );
		strncpy( contents->str->str + strlen( db_header_1 ), buffer, strlen( buffer ) );

		result = g_string_free( contents->str, FALSE );
		contents->str = NULL;
	}

	return result;
}

/*-----------------------------------------------------------------------------------------------*/

gboolean database_file_exists( const gchar *path, enum database_location location )
{
	gboolean result = FALSE;

	gchar *filename = get_database_filename( path, location );
	if ( filename != NULL )
	{
		result = g_file_test( filename, G_FILE_TEST_EXISTS );
		g_free( filename );
	}

	return result;
}

void remove_database_file( const gchar *path, enum database_location location )
{
	gchar *filename = get_database_filename( path, location );
	if ( filename != NULL )
	{
		g_remove( filename );
		g_free( filename );
	}

	db_dirty = TRUE;
}

/*-----------------------------------------------------------------------------------------------*/
