/*
	hildon_gtk2.h
	Copyright (C) 2008-2013 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)

	--------------------------------------------------------------------

	This file is part of the Gwenevieve application.

	Gwenevieve is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Gwenevieve is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
*/

/*-----------------------------------------------------------------------------------------------*/

#ifdef OSSO_SERVICE
#include <hildon/hildon.h>
#include <hildon-uri.h>
#endif

/*-----------------------------------------------------------------------------------------------*/

/*#define GTK_DISABLE_DEPRECATED*/
#include <gtk/gtk.h>
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
void gtk_tool_item_set_tooltip_text( GtkToolItem *tool_item, const gchar *text );  /* Since 2.12 */
#endif
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 14
GdkWindow *gtk_widget_get_window( GtkWidget *widget );                             /* Since 2.14 */
#endif
#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 18
void gtk_cell_renderer_set_visible( GtkCellRenderer *cell, gboolean visible );     /* Since 2.18 */
void gtk_widget_set_visible( GtkWidget *widget, gboolean visible );                /* Since 2.18 */
#endif

#include <gdk/gdkkeysyms.h>

#if defined(_MSC_VER) && (_MSC_VER < 1300)
#define _stat32 stat
#endif
#include <glib/gstdio.h>  /* for using g_remove() & g_stat() */
#if GLIB_MAJOR_VERSION == 2 && GLIB_MINOR_VERSION < 24
#define GStatBuf struct stat
#elif GLIB_MAJOR_VERSION == 2 && GLIB_MINOR_VERSION < 26
#ifdef G_OS_WIN32
#define GStatBuf struct _g_stat_struct
#else
#define GStatBuf struct stat
#endif
#endif

/*-----------------------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <string.h>
#ifndef WIN32
  #include <strings.h>
  #define stricmp strcasecmp
  #define strnicmp strncasecmp
#endif

/*-----------------------------------------------------------------------------------------------*/

#define BORDER    3
#define SPACING   3

void hildon_gtk_initialize( const gchar *name, const gchar *version, const guint8 icon[], ... );
void hildon_gtk_deinitialize( void );

#ifdef OSSO_SERVICE
extern const gchar *osso_product_name, *osso_product_hardware;
#endif

/* Main Window */

void set_main_window( GtkWindow *window );
void set_main_window_cursor( GdkCursorType cursor_type );
void set_main_window_cursor_default( void );
gboolean get_main_window_fullscreen( void );
void set_main_window_fullscreen( gboolean value );
void set_main_window_menu( GtkMenuShell *menu, gboolean popup );
void set_main_window_statusbar( GtkStatusbar *statusbar, guint context_id );
void set_main_window_title( const char *title );
void set_main_window_toolbar( GtkToolbar *toolbar );
void set_main_window_widget( GtkWidget *widget );

gboolean main_window_popup_menu( GtkWidget *widget, GdkEventButton *event, gpointer user_data );

void busy_enter( void );
void busy_leave( void );

/* Status Bar */

void show_information( const gchar *icon_name, const gchar *text_format, const gchar *text );

guint push_to_statusbar( const gchar *text_format, const gchar *text );
void  remove_from_statusbar( guint message_id );

/* Device Status */

gboolean isMID( void );
void setMID( gboolean value );
gboolean isSmartQ( void );

void take_screenshot( guint delay, const char *filename );

/* Stock Icons */

GtkIconFactory *get_icon_factory( void );
const gchar *register_stock_icon( const gchar *stock_id, const guint8 *data );
const gchar *register_bidi_stock_icon( const gchar *stock_id, const guint8 *data_ltr, const guint8 *data_rtl );
/*void register_sized_icon( GtkIconSet *icon_set, GtkIconSize size, const guint8 *data );*/
void register_di_icon( GtkIconSet *icon_set, GtkTextDirection direction, const guint8 *data );
void register_gtk_stock_icon( const gchar *stock_id );

/* Animation */

struct animation
{
	const gchar *action;
	gchar *item;
	guint timeout_id;
	GtkWidget *widget;
};

struct animation *show_animation( const char *action, const gchar *item, guint interval );
void cancel_animation( struct animation *animation );
void clear_animation( struct animation *animation );

/* Message Dialog */

#define GTK_MESSAGE_QUESTION_WITH_CANCEL_AS_DEFAULT_BUTTON ((GtkMessageType)0x1000)
gint message_dialog( GtkMessageType type, const gchar *description );

/* Miscellaneous */

void attach_to_table( GtkTable *table, GtkWidget *child, guint top_attach, guint left_attach, guint right_attach );
gboolean get_alternative_button_order( void );
GtkIconSize get_icon_size_for_toolbar( GtkToolbar *toolbar );
gint get_icon_width_for_toolbar( GtkToolbar *toolbar );
GList *get_selected_tree_row_references( GtkTreeSelection *selection );
GtkSettings *get_settings_for_widget( GtkWidget *widget );
const GdkColor *get_widget_base_color( GtkWidget *widget, GtkStateType state );
gboolean foreach_selected_tree_view_row( GtkTreeView *tree_view,
	gboolean (*func)( GtkTreeView *tree_view, GtkTreePath *path, gchar *path_str, gpointer user_data ), gpointer user_data );
GtkWidget *new_entry( gint max_length );
GtkWidget *new_file_chooser_dialog( const gchar *title, GtkWindow *parent, GtkFileChooserAction action );
GtkBox *new_hbox( void );
GtkWidget *new_label( const gchar *str );
GtkWidget *new_label_with_colon( const gchar *str );
GtkMenuShell *new_menu_bar( void );
GtkDialog *new_modal_dialog_with_ok_cancel( const gchar *title, GtkWindow *parent );
GtkScrolledWindow *new_scrolled_window( GtkPolicyType hscrollbar_policy, GtkPolicyType vscrollbar_policy );
GtkStatusbar *new_statusbar();
GtkTable *new_table( guint rows, guint columns );
GtkWindow *new_window( void );
void set_dialog_size_request( GtkDialog *dialog, gboolean set_height );
void tree_view_queue_resize( GtkTreeView *tree_view, int num_columns );

/*
	Workaround for flaw in older GTK+ versions:
	A double click on a selected row will produce *two* "row-activated" events
*/
extern gboolean row_activated_flag;
void set_row_activated_flag( void );

/*
	fix_hildon_tool_button() is a workaround
	for flaws in older GTK+ versions (MAEMO only!?):

	1. The toolbar item width is wrong when using an item image which must be scaled
	2. The toolbar item height is wrong in toolbars with GTK_ORIENTATION_VERTICAL
*/
#ifdef MAEMO
void fix_hildon_tool_button( GtkToolItem *item );
#else
#define fix_hildon_tool_button( item )
#endif

/*-----------------------------------------------------------------------------------------------*/
