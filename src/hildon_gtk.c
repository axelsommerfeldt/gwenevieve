/*
	hildon_gtk2.c
	Copyright (C) 2008-2013 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)

	--------------------------------------------------------------------

	This file is part of the Gwenevieve application.

	Gwenevieve is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Gwenevieve is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "hildon_gtk.h"
#include "debug.h"

#ifdef OSSO_SERVICE
#include <libosso.h>
#endif

#ifdef MAEMO
#include <hildon/hildon-file-chooser-dialog.h>
#endif

#define DIALOG_WIDTH_REQUEST      480
#define DIALOG_WIDTH_REQUEST_MID  640

/*-----------------------------------------------------------------------------------------------*/

#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12

void gtk_tool_item_set_tooltip_text( GtkToolItem *tool_item, const gchar *text )  /* Since 2.12 */
{
	static GtkTooltips *tooltips;

	if ( tooltips != NULL ) tooltips = gtk_tooltips_new();
	gtk_tool_item_set_tooltip( tool_item, tooltips, text, NULL );
}

#endif

#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 14

GdkWindow *gtk_widget_get_window( GtkWidget *widget )                             /* Since 2.14 */
{
	/*g_return_val_if_fail( GTK_IS_WIDGET(widget), NULL );*/
	if ( !GTK_IS_WIDGET(widget) ) return NULL;
	return widget->window;
}

#endif

#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 18

void gtk_cell_renderer_set_visible( GtkCellRenderer *cell, gboolean visible )     /* Since 2.18 */
{
	g_return_if_fail( GTK_IS_CELL_RENDERER(cell) );
	g_object_set( cell, "visible", visible, NULL );
}

void gtk_widget_set_visible( GtkWidget *widget, gboolean visible )                /* Since 2.18 */
{
	g_return_if_fail( GTK_IS_WIDGET(widget) );
	g_object_set( widget, "visible", visible, NULL );
}

#endif

/*-----------------------------------------------------------------------------------------------*/

#ifdef OSSO_SERVICE
static osso_context_t *osso;
const gchar *osso_product_name, *osso_product_hardware;
#endif

#ifdef MAEMO
static HildonProgram *Program;
#endif

static const gchar *ProgramName;

static gint screen_width, screen_height;
static gboolean MID;

void hildon_gtk_initialize( const gchar *name, const gchar *version, const guint8 icon[], ... )
{
#ifdef OSSO_SERVICE
	/* OSSO initialize */
	TRACE(( "osso_initialize( \"" OSSO_SERVICE "\", \"" VERSION "\", FALSE, NULL )\n" ));
	osso = osso_initialize( OSSO_SERVICE, version, FALSE, NULL );

	osso_product_name = g_getenv( "OSSO_PRODUCT_NAME" );
	if ( osso_product_name == NULL ) osso_product_name = "?";
	osso_product_hardware = g_getenv( "OSSO_PRODUCT_HARDWARE" );
	if ( osso_product_hardware == NULL ) osso_product_hardware = "?";
	/*
		OSSO_VERSION                 = "RX-34+RX-44+RX-48_DIABLO_4.2008.36-5_PR_MR0"
		OSSO_PRODUCT_RELEASE_NAME    = "OS 2008"
		OSSO_PRODUCT_RELEASE_VERSION = "4.2008.36-5"
		OSSO_PRODUCT_NAME            = "N800"
		OSSO_PRODUCT_FULL_NAME       = "Nokia N800 Internet Tablet"
		OSSO_PRODUCT_SHORT_NAME      = "Nokia N800"
		OSSO_PRODUCT_HARDWARE        = "RX-34"
	*/
#endif

#ifdef MAEMO
	/* Create the hildon program */
	Program = HILDON_PROGRAM( hildon_program_get_instance() );
#endif

	screen_width = gdk_screen_width();
	screen_height = gdk_screen_height();
#ifdef MAEMO
	MID = TRUE;
#else
	MID = screen_width <= 800 && screen_height <= 480;  /* WVGA (800×480) */
#endif

	/* Setup the application */
	g_set_application_name( ProgramName = name );

#ifndef MAEMO
	/* Use Gwenevieve icon as main icon */
	if ( icon != NULL )
	{
		GList *list = NULL;
		va_list argp;

		va_start( argp, icon );
		do
		{
			list = g_list_append( list, gdk_pixbuf_new_from_inline( -1, icon, FALSE, NULL ) );
			icon = va_arg( argp, const guint8 * );
		}
		while ( icon != NULL );
		va_end( argp );

		gtk_window_set_default_icon_list( list );
		/*gtk_window_set_icon_list( MainWindow, list );*/
	}
#endif
}

void hildon_gtk_deinitialize( void )
{
#ifdef OSSO_SERVICE
	/* Exit */
	if ( osso != NULL ) osso_deinitialize( osso );
#endif
}

/*-----------------------------------------------------------------------------------------------*/
/* Main Window */

static GtkWindow *main_window;
static GtkMenuShell *main_menu;
static gboolean main_menu_popup;
static GtkToolbar *main_toolbar;
static GtkStatusbar *main_statusbar;
static guint main_statusbar_context_id;
#ifndef MAEMO
static GtkMenuItem *main_statusbar_menu_item;
#endif

static gint _busy_count;
static GdkCursorType _cursor_type;
static gboolean _cursor_type_is_valid;

void set_main_window( GtkWindow *window )
{
	main_window = window;
	if ( window != NULL ) set_main_window_title( NULL );
}

static void __set_cursor( GdkCursor *cursor )
{
	if ( GTK_IS_WIDGET(main_window) )
	{
		GdkWindow *window = gtk_widget_get_window( GTK_WIDGET(main_window) );
		if ( window != NULL ) gdk_window_set_cursor( window, cursor );
	}
}

static void _set_cursor( GdkCursorType cursor_type )
{
	GdkCursor *cursor = gdk_cursor_new/*_for_display*/(
		/*gtk_widget_get_display( widget, GDK_XTERM ),*/
		cursor_type );
	__set_cursor( cursor );
	gdk_cursor_unref( cursor );
}

static void set_non_busy_cursor( void )
{
	if ( _busy_count == 0 )
	{
		if ( _cursor_type_is_valid )
		{
			/* Set custom cursor */
			_set_cursor( _cursor_type );
		}
		else
		{
			/* Set default cursor */
			__set_cursor( NULL );
		}
	}
}

void set_main_window_cursor( GdkCursorType cursor_type )
{
	if ( !_cursor_type_is_valid || _cursor_type != cursor_type )
	{
		_cursor_type = cursor_type;
		_cursor_type_is_valid = TRUE;
		set_non_busy_cursor();
	}
}

void set_main_window_cursor_default( void )
{
	if ( _cursor_type_is_valid )
	{
		_cursor_type_is_valid = FALSE;
		set_non_busy_cursor();
	}
}

gboolean get_main_window_fullscreen( void )
{
	if ( GTK_IS_WIDGET(main_window) )
	{
		GdkWindow *window = gtk_widget_get_window( GTK_WIDGET(main_window) );
		if ( GDK_IS_WINDOW( window ) )
			return (gdk_window_get_state( window ) & GDK_WINDOW_STATE_FULLSCREEN) != 0;
	}
	return FALSE;
}

void set_main_window_fullscreen( gboolean value )
{
	if ( value )
	{
		TRACE(( "gtk_window_fullscreen()\n" ));
		gtk_window_fullscreen( main_window );
	}
	else
	{
		TRACE(( "gtk_window_unfullscreen()\n" ));
		gtk_window_unfullscreen( main_window );
	}
}

void set_main_window_menu( GtkMenuShell *menu, gboolean popup )
{
	main_menu = menu;
	main_menu_popup = popup;
}

void set_main_window_statusbar( GtkStatusbar *statusbar, guint context_id )
{
	if ( (main_statusbar = statusbar) != NULL )
	{
		if ( context_id == 0 )
			context_id = gtk_statusbar_get_context_id( statusbar, "context-description" );
	}
	else
	{
		context_id = 1;
	}

	main_statusbar_context_id = context_id;
}

void set_main_window_title( const char *title )
{
#ifdef MAEMO
	gtk_window_set_title( main_window, (title != NULL) ? title : "" );
#else
	if ( title != NULL && *title != '\0' )
	{
		gchar *window_title = g_strdup_printf( "%s - %s", ProgramName, title );
		gtk_window_set_title( main_window, window_title );
		g_free( window_title );
	}
	else
	{
		gtk_window_set_title( main_window, ProgramName );
	}
#endif
}

void set_main_window_toolbar( GtkToolbar *toolbar )
{
	main_toolbar = toolbar;
}

void set_main_window_widget( GtkWidget *widget )
{
#ifdef MAEMO

	if ( main_menu != NULL )
		hildon_window_set_menu( HILDON_WINDOW(main_window), GTK_MENU(main_menu) );

	if ( main_toolbar != NULL )
		hildon_window_add_toolbar( HILDON_WINDOW(main_window), GTK_TOOLBAR(main_toolbar) );

	gtk_container_add( GTK_CONTAINER( main_window ), widget );

#else

	/* Create vbox */
	GtkBox *vbox = GTK_BOX( gtk_vbox_new( FALSE, 0 ) );

	/* Create menu */
	if ( main_menu != NULL )
	{
		if ( main_menu_popup )
		{
			g_signal_connect( G_OBJECT(main_window), "button-release-event", G_CALLBACK(main_window_popup_menu), NULL );
		}
		else
		{
			gtk_box_pack_start( vbox, GTK_WIDGET(main_menu), FALSE, FALSE, 0 );
		}
	}

	/* Create toolbar */
	if ( main_toolbar != NULL )
		(isMID() ? gtk_box_pack_end : gtk_box_pack_start)
			( vbox, GTK_WIDGET(main_toolbar), FALSE, FALSE, 0 );

	gtk_box_pack_start( vbox, widget, TRUE, TRUE, 0 );
	gtk_container_set_focus_child( GTK_CONTAINER(vbox), widget );

	if ( main_statusbar_context_id != 0 )
	{
		if ( main_statusbar != NULL )
		{
			gtk_box_pack_end( vbox, GTK_WIDGET(main_statusbar), FALSE, FALSE, 0 );
		}
		else if ( main_menu != NULL )
		{
			main_statusbar_menu_item = GTK_MENU_ITEM(gtk_menu_item_new_with_label( "" ));
			gtk_widget_set_sensitive( GTK_WIDGET(main_statusbar_menu_item), FALSE );
			gtk_menu_item_set_right_justified( main_statusbar_menu_item, TRUE );
			gtk_menu_shell_append( main_menu, GTK_WIDGET(main_statusbar_menu_item) );
		}
	}

	gtk_container_add( GTK_CONTAINER( main_window ), GTK_WIDGET(vbox) );

#endif
}

gboolean main_window_popup_menu( GtkWidget *widget, GdkEventButton *event, gpointer user_data )
{
	TRACE(( "# main_window_popup_menu( event->button = %u )\n", event->button ));

	if ( event->button == 3 )
	{
		GtkMenuShell *menu = g_object_ref( main_menu );

		gtk_menu_attach_to_widget( GTK_MENU(menu), GTK_WIDGET(main_window), NULL );
		gtk_widget_show_all( GTK_WIDGET(menu) );
		gtk_menu_popup( GTK_MENU(menu), NULL, NULL, NULL, NULL, 0, event->time );
		return TRUE;
	}

	return FALSE;
}

void busy_enter( void /*GtkWidget* widget*/ )
{
	if ( _busy_count++ == 0 ) _set_cursor( GDK_WATCH );
}

void busy_leave( void /*GtkWidget* widget*/ )
{
	ASSERT( _busy_count > 0 );
	if ( _busy_count > 0 ) { --_busy_count; set_non_busy_cursor(); }
}

/*-----------------------------------------------------------------------------------------------*/
/* Status Bar */

#ifndef MAEMO

#define SHOW_INFORMATION_INTERVAL 2500

static struct show_information_data
{
	guint id, timeout_id;
	gboolean old_sensitive;
	gchar *old_label, *new_label;
} *sid;

static gboolean show_information_timeout( gpointer user_data )
{
	guint message_id = GPOINTER_TO_UINT( user_data );

	gdk_threads_enter();
	remove_from_statusbar( message_id );
	gdk_threads_leave();

	return FALSE;
}

#endif

void show_information( const gchar *icon_name, const gchar *text_format, const gchar *text )
{
#ifdef MAEMO

	hildon_banner_show_information( GTK_WIDGET(main_window), icon_name, text );

#else

	guint message_id = push_to_statusbar( text_format, text );
	guint timeout_id = g_timeout_add_full( G_PRIORITY_LOW, SHOW_INFORMATION_INTERVAL, show_information_timeout, GUINT_TO_POINTER( message_id ), NULL );
	if ( sid != NULL ) sid->timeout_id = timeout_id;

#endif
}

guint push_to_statusbar( const gchar *text_format, const gchar *text )
{
	guint message_id = 0;

#ifndef MAEMO
	if ( main_statusbar != NULL )
	{
		gchar *statusbar_text;

		if ( text_format != NULL )
		{
			statusbar_text = g_strdup_printf( text_format, text );
		}
		else
		{
			statusbar_text = g_strdup( text );
		}

		ASSERT( statusbar_text != NULL );
		message_id = gtk_statusbar_push( main_statusbar, main_statusbar_context_id, statusbar_text );
		g_free( statusbar_text );
	}
	else if ( main_statusbar_menu_item != NULL )
	{
		if ( sid != NULL )
		{
			gchar *label = NULL;

			if ( sid->timeout_id != 0 )
			{
				g_source_remove( sid->timeout_id );
				sid->timeout_id = 0;
			}

			g_object_get( main_statusbar_menu_item, "label", &label, NULL );
			if ( label != NULL && sid->new_label != NULL && strcmp( sid->new_label, label ) == 0 )
			{
				;  /* menu_item_old_label is still valid */
			}
			else
			{
				/* invalidate menu_item_old_label */
				g_free( sid->old_label );
				sid->old_label = NULL;
			}
		}
		else
		{
			sid = g_new0( struct show_information_data, 1 );
		}

		if ( sid->old_label == NULL )
		{
			sid->old_sensitive = GTK_WIDGET_SENSITIVE( main_statusbar_menu_item );
			g_object_get( main_statusbar_menu_item, "label", &sid->old_label, NULL );
		}

		g_free( sid->new_label );
		sid->new_label = g_strdup( text );
		g_object_set( main_statusbar_menu_item, "label", sid->new_label, NULL );
		gtk_widget_set_sensitive( GTK_WIDGET(main_statusbar_menu_item), TRUE );

		message_id = ++(sid->id);
		if ( message_id == 0 ) message_id = ++(sid->id);
	}
#endif

	return message_id;
}

void remove_from_statusbar( guint message_id )
{
#ifndef MAEMO
	if ( main_statusbar != NULL )
	{
		gtk_statusbar_remove( main_statusbar, main_statusbar_context_id, message_id );
	}
	else if ( main_statusbar_menu_item != NULL )
	{
		ASSERT( sid != NULL );
		if ( sid != NULL && sid->id == message_id )
		{
			gchar *label = NULL;
			g_object_get( main_statusbar_menu_item, "label", &label, NULL );

			if ( label != NULL && strcmp( sid->new_label, label ) == 0 )
			{
				gtk_widget_set_sensitive( GTK_WIDGET(main_statusbar_menu_item), sid->old_sensitive );
				g_object_set( main_statusbar_menu_item, "label", sid->old_label, NULL );
			}

			g_free( label );

			g_free( sid->new_label );
			g_free( sid->old_label );
			g_free( sid );
			sid = NULL;
		}
	}
#endif
}

/*-----------------------------------------------------------------------------------------------*/
/* Device Status */

gboolean isMID( void )
{
	return MID;
}

void setMID( gboolean value )
{
	MID = value;
}

gboolean isSmartQ( void )
{
#if !defined( MAEMO ) && !defined( WIN32 )
	const gchar *host_name = g_get_host_name();
	return ( host_name != NULL && strcmp( host_name, "SmartQ" ) == 0 );
#else
	return FALSE;
#endif
}

/*-----------------------------------------------------------------------------------------------*/

static void screenshot_show_info( const gchar *icon_name, const gchar *result )
{
	gdk_threads_enter();
	show_information( icon_name, "Screenshot: %s", result );
	gdk_threads_leave();
}

static void screenshot_save( GdkPixbuf *pb, const gchar *filename )
{
	if ( gdk_pixbuf_save( pb, filename, "png", NULL, NULL ) )
	{
		gchar *basename = g_path_get_basename( filename );
		gchar *result = g_strdup_printf( "Screenshot saved to %s.", basename );

		screenshot_show_info( GTK_STOCK_DIALOG_INFO, result );

		g_free( result );
		g_free( basename );
	}
	else
	{
		screenshot_show_info( GTK_STOCK_DIALOG_ERROR, "Unable to save the screenshot." );
	}
}

static gboolean screenshot_ask_filename( gpointer user_data )
{
	GdkPixbuf *pb = (GdkPixbuf *)user_data;

	GtkWidget *chooser;
	GtkFileFilter *filter;
	gchar *filename;

	gdk_threads_enter();

	chooser = new_file_chooser_dialog( "Save Screenshot", NULL, GTK_FILE_CHOOSER_ACTION_SAVE );

	filter = gtk_file_filter_new();
	gtk_file_filter_set_name( filter, "Portable Network Graphics (*.png;*.PNG)" );
	gtk_file_filter_add_pattern( filter, "*.png;*.PNG" );
	gtk_file_chooser_add_filter( GTK_FILE_CHOOSER(chooser), filter );

	/*gtk_file_chooser_set_current_folder( GTK_FILE_CHOOSER(chooser), default_folder_for_saving );*/
	gtk_file_chooser_set_current_name( GTK_FILE_CHOOSER(chooser), "untitled.png" );
	gtk_file_chooser_set_filter( GTK_FILE_CHOOSER(chooser), filter );
	gtk_file_chooser_set_do_overwrite_confirmation( GTK_FILE_CHOOSER(chooser), TRUE );

	if ( gtk_dialog_run( GTK_DIALOG(chooser) ) == GTK_RESPONSE_OK )
		filename = gtk_file_chooser_get_filename( GTK_FILE_CHOOSER(chooser) );
	else
		filename = NULL;

	gtk_widget_destroy( chooser );

	gdk_threads_leave();

	if ( filename != NULL )
	{
		screenshot_save( pb, filename );
		g_free( filename );
	}

	g_object_unref( pb );
	return FALSE;
}

static gboolean screenshot( gpointer user_data )
	/* taken from http://ubuntuforums.org/showthread.php?t=448160 */
	/* see also http://php-gtk.eu/code-hints/grabbing-a-screenshot-with-gdk */
{
	gchar *filename = (gchar*)user_data;

	GdkWindow *w = gdk_get_default_root_window();
	gint width, height;
	GdkPixbuf *pb;

	gdk_window_get_size( w, &width, &height );
	TRACE(( "The size of the window is %d x %d\n", width, height ));

	pb = gdk_pixbuf_get_from_drawable( NULL, w, gdk_window_get_colormap(w), 0, 0, 0, 0, width, height );
	if ( pb != NULL )
	{
		if ( filename == NULL )
		{
			g_idle_add( screenshot_ask_filename, pb );
		}
		else
		{
			screenshot_save( pb, filename );
			g_object_unref( pb );
		}
	}
	else
	{
		screenshot_show_info( GTK_STOCK_DIALOG_ERROR, "Unable to get the screenshot." );
	}

	g_free( filename );
	return FALSE;
}

void take_screenshot( guint delay, const char *filename )
{
	g_timeout_add( delay, screenshot, g_strdup( filename ) );
}

/*-----------------------------------------------------------------------------------------------*/
/* Stock Icons */

GtkIconFactory *get_icon_factory( void )
{
	static GtkIconFactory *factory;

	if ( factory == NULL )
	{
		factory = gtk_icon_factory_new();
		ASSERT( factory != NULL );
		if ( factory != NULL )
		{
			gtk_icon_factory_add_default( factory );
			g_object_unref( factory );
		}
	}

	return factory;
}

const gchar *register_stock_icon( const gchar *stock_id, const guint8 *data )
{
	ASSERT( stock_id != NULL );

	if ( data != NULL )
	{
		GtkIconFactory *factory = get_icon_factory();

		GdkPixbuf *pixbuf = gdk_pixbuf_new_from_inline( -1, data, FALSE, NULL );
		ASSERT( pixbuf != NULL );
		if ( pixbuf != NULL )
		{
			GtkIconSet *icon_set = gtk_icon_set_new_from_pixbuf( pixbuf );
			gtk_icon_factory_add( factory, stock_id, icon_set );
			gtk_icon_set_unref( icon_set );

			g_object_unref( pixbuf );
			return stock_id;
		}
	}

	return NULL;
}

const gchar *register_bidi_stock_icon( const gchar *stock_id, const guint8 *data_ltr, const guint8 *data_rtl )
{
	ASSERT( stock_id != NULL );

	if ( data_ltr != NULL && data_rtl != NULL )
	{
		GtkIconFactory *factory = get_icon_factory();
		GtkIconSet *icon_set = gtk_icon_set_new();

		register_di_icon( icon_set, GTK_TEXT_DIR_LTR, data_ltr );
		register_di_icon( icon_set, GTK_TEXT_DIR_RTL, data_rtl );

		gtk_icon_factory_add( factory, stock_id, icon_set );
		gtk_icon_set_unref( icon_set );
		return stock_id;
	}

	return NULL;
}

/*
void register_sized_icon( GtkIconSet *icon_set, GtkIconSize size, const guint8 *data )
{
	GdkPixbuf *pixbuf = gdk_pixbuf_new_from_inline( -1, data, FALSE, NULL );
	ASSERT( pixbuf != NULL );
	if ( pixbuf != NULL )
	{
		GtkIconSource *source = gtk_icon_source_new();
		ASSERT( source != NULL );
		if ( source != NULL )
		{
			gtk_icon_source_set_size_wildcarded( source, FALSE );
			gtk_icon_source_set_size( source, size );
			gtk_icon_source_set_pixbuf( source, pixbuf );
			gtk_icon_set_add_source( icon_set, source );
			gtk_icon_source_free( source );
		}

		g_object_unref( pixbuf );
	}
}
*/

void register_di_icon( GtkIconSet *icon_set, GtkTextDirection direction, const guint8 *data )
{
	GdkPixbuf *pixbuf = gdk_pixbuf_new_from_inline( -1, data, FALSE, NULL );
	ASSERT( pixbuf != NULL );
	if ( pixbuf != NULL )
	{
		GtkIconSource *source = gtk_icon_source_new();
		ASSERT( source != NULL );
		if ( source != NULL )
		{
			gtk_icon_source_set_direction_wildcarded( source, FALSE );
			gtk_icon_source_set_direction( source, direction );
			gtk_icon_source_set_pixbuf( source, pixbuf );
			gtk_icon_set_add_source( icon_set, source );
			gtk_icon_source_free( source );
		}

		g_object_unref( pixbuf );
	}
}

void register_gtk_stock_icon( const gchar *stock_id )
{
	GtkIconFactory *factory = get_icon_factory();
	GtkIconSet *set = gtk_icon_set_new();
	GtkIconSource *source = gtk_icon_source_new();

	gtk_icon_source_set_icon_name( source, stock_id );
	gtk_icon_set_add_source( set, source );
	gtk_icon_source_free( source );

	gtk_icon_factory_add( factory, stock_id, set );
	gtk_icon_set_unref( set );
}

/*-----------------------------------------------------------------------------------------------*/
/* Animation */

#ifndef MAEMO
static gboolean animation_func( gpointer user_data )
{
	gboolean result = FALSE;

	gdk_threads_enter();

	if ( GTK_IS_WIDGET( user_data ) )
	{
		GtkProgressBar *progress_bar = GTK_PROGRESS_BAR( user_data );
		gtk_progress_bar_pulse( progress_bar );
		result = TRUE;
	}

	gdk_threads_leave();
	return result;
}
#endif

static GtkWidget *animation_show( const char *action, const gchar *item )
{
	GString *text = g_string_new( action );
	GtkWindow *parent = main_window;
	GtkWidget *animation;

#ifdef MAEMO

	if ( item != NULL )
	{
		g_string_append_c( text, ' ' );
		/*g_string_append_printf( text, Text(QUOTED), item );*/
		g_string_append( text, "`" );
		g_string_append( text, item );
		g_string_append( text, "'" );
	}

	animation = hildon_banner_show_animation( GTK_WIDGET(parent), /*animation_name*/ NULL, text->str );

#else

	GtkVBox *vbox;
	GtkLabel *label;
	GtkProgressBar *progress_bar;
	GdkCursor* cursor;
	guint timeout_id;

	g_string_append( text, "..." );

	animation = gtk_window_new( GTK_WINDOW_TOPLEVEL );
	gtk_window_set_type_hint( GTK_WINDOW(animation), GDK_WINDOW_TYPE_HINT_DIALOG );
	gtk_window_set_title( GTK_WINDOW(animation), ""/*text->str*/ );
	gtk_window_set_decorated( GTK_WINDOW(animation), TRUE );
	gtk_window_set_deletable( GTK_WINDOW(animation), FALSE );
	gtk_window_set_modal( GTK_WINDOW(animation), TRUE );
	gtk_window_set_transient_for( GTK_WINDOW(animation), parent );
	gtk_window_set_destroy_with_parent( GTK_WINDOW(animation), TRUE );
	gtk_container_set_border_width( GTK_CONTAINER(animation), 6 );

	vbox = GTK_VBOX( gtk_vbox_new( FALSE, 6 ) );
	gtk_container_add( GTK_CONTAINER(animation), GTK_WIDGET(vbox) );

	label = GTK_LABEL( gtk_label_new( text->str ) );
	gtk_misc_set_alignment( GTK_MISC(label), 0, 0.5 );
	gtk_box_pack_start_defaults( GTK_BOX(vbox), GTK_WIDGET(label) );

	progress_bar = GTK_PROGRESS_BAR( gtk_progress_bar_new() );
	if ( item != NULL ) gtk_progress_bar_set_text( progress_bar, item );
	/*gtk_widget_set_size_request( progress_bar, 150, -1 );*/
	gtk_box_pack_start_defaults( GTK_BOX(vbox), GTK_WIDGET(progress_bar) );

	gtk_window_set_position( GTK_WINDOW(animation), GTK_WIN_POS_CENTER_ON_PARENT );
	gtk_widget_show_all( animation );
	timeout_id = g_timeout_add( 250, animation_func, progress_bar );
	g_object_set_data( G_OBJECT(animation), "timeout_id", GUINT_TO_POINTER( timeout_id ) );

	cursor = gdk_cursor_new/*_for_display*/(
			/*gtk_widget_get_display( widget, GDK_XTERM ),*/
			GDK_WATCH );
	gdk_window_set_cursor( gtk_widget_get_window( animation ), cursor );
	gdk_cursor_unref( cursor );

#endif

	g_string_free( text, TRUE );
	return animation;
}

static gboolean animation_timeout( gpointer user_data )
{
	struct animation *a = (struct animation *)user_data;

	if ( a->timeout_id != 0 )  /* Thread is still running */
	{
		gdk_threads_enter();

		if ( a->timeout_id != 0 )  /* Thread is still running */
		{
			a->timeout_id = 0;

			a->widget = animation_show( a->action, a->item );
			g_free( a->item );
			a->item = NULL;
		}

		gdk_threads_leave();
	}

	return FALSE;
}

struct animation *show_animation( const char *action, const gchar *item, guint interval )
{
	struct animation *a = g_new0( struct animation, 1 );

	busy_enter();

	if ( interval != 0 )
	{
		a->action = /*g_strdup(*/ action /*)*/;
		a->item = g_strdup( item );
		a->timeout_id = g_timeout_add( interval, animation_timeout, a );
	}
	else
	{
		a->widget = animation_show( action, item );
	}

	return a;
}

void cancel_animation( struct animation *animation )
{
	if ( animation != NULL )
	{
		guint timeout_id = animation->timeout_id;
		animation->timeout_id = 0;
		if ( timeout_id != 0 ) g_source_remove( timeout_id );
	}
}

void clear_animation( struct animation *animation )
{
	if ( animation != NULL )
	{
		cancel_animation( animation );

		if ( animation->widget != NULL )
		{
		#ifndef MAEMO

			guint timeout_id = GPOINTER_TO_UINT( g_object_get_data( G_OBJECT(animation->widget), "timeout_id" ) );
			if ( timeout_id != 0 ) g_source_remove( timeout_id );

		#endif

			gtk_widget_destroy( animation->widget );
		}

		g_free( animation->item );
		/*g_free( animation->action );*/
		g_free( animation );

		busy_leave();
	}
}

/*-----------------------------------------------------------------------------------------------*/
/* Message Dialog */

gint message_dialog( GtkMessageType type, const gchar *description )
{
	GtkWindow* parent = main_window;
	gboolean cancel_as_default_button = FALSE;
#ifdef MAEMO
	const gchar *icon_name;
#endif
	GtkWidget* dialog;
	gint result;

	if ( type == GTK_MESSAGE_QUESTION_WITH_CANCEL_AS_DEFAULT_BUTTON )
	{
		type = GTK_MESSAGE_QUESTION;
		cancel_as_default_button = TRUE;
	}

#ifdef MAEMO

	switch ( type )
	{
	case GTK_MESSAGE_INFO:
		icon_name = GTK_STOCK_DIALOG_INFO;
		break;
	case GTK_MESSAGE_WARNING:
		icon_name = GTK_STOCK_DIALOG_WARNING;
		break;
	case GTK_MESSAGE_QUESTION:
		icon_name = GTK_STOCK_DIALOG_QUESTION;
		break;
	case GTK_MESSAGE_ERROR:
		icon_name = GTK_STOCK_DIALOG_ERROR;
		break;
	default:
		icon_name = NULL;
	}

	dialog = (( type == GTK_MESSAGE_QUESTION )
		? hildon_note_new_confirmation_with_icon_name
		: hildon_note_new_information_with_icon_name)
			( parent, description, icon_name );

#else

	dialog = gtk_message_dialog_new( parent, GTK_DIALOG_MODAL, type,
		( type == GTK_MESSAGE_QUESTION ) ? GTK_BUTTONS_OK_CANCEL : GTK_BUTTONS_OK,
		"%s", description );

#endif

	gtk_dialog_set_default_response( GTK_DIALOG(dialog), ( cancel_as_default_button ) ? GTK_RESPONSE_CANCEL : GTK_RESPONSE_OK );

	gtk_widget_show_all( dialog );
	result = gtk_dialog_run( GTK_DIALOG(dialog) );
	gtk_widget_destroy( dialog );

	return result;
}

/*-----------------------------------------------------------------------------------------------*/
/* Miscellaneous */

void attach_to_table( GtkTable *table, GtkWidget *child, guint top_attach, guint left_attach, guint right_attach )
{
	gtk_table_attach( table, child, left_attach, right_attach, top_attach, top_attach + 1,
		GTK_IS_LABEL(child) ? GTK_FILL : GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0 );
}

gboolean get_alternative_button_order( void )
{
#ifdef MAEMO
	return TRUE;
#else
	GtkSettings *settings = gtk_settings_get_default();
	gboolean result = FALSE;
	g_object_get( settings, "gtk-alternative-button-order", &result, NULL );
	return result;
#endif
}

GtkIconSize get_icon_size_for_toolbar( GtkToolbar *toolbar )
{
	gboolean icon_size_set;
	GtkIconSize icon_size;

	g_object_get( toolbar, "icon-size", &icon_size, "icon-size-set", &icon_size_set, NULL );
	TRACE(( "get_icon_size_for_toolbar(): icon-size = %d, icon-size-set = %d\n", (int)icon_size, (int)icon_size_set ));
	if ( !icon_size_set )
	{
		GtkSettings *settings = get_settings_for_widget( GTK_WIDGET(toolbar) );

		g_object_get( settings, "gtk-toolbar-icon-size", &icon_size, NULL );
		TRACE(( "get_icon_size_for_toolbar(): gtk-toolbar-icon-size = %d\n", (int)icon_size ));
	}

	return icon_size;
}

gint get_icon_width_for_toolbar( GtkToolbar *toolbar )
{
	GtkSettings *settings = get_settings_for_widget( GTK_WIDGET(toolbar) );
	GtkIconSize icon_size = get_icon_size_for_toolbar( toolbar );
	gint width, height;

	if ( !gtk_icon_size_lookup_for_settings( settings, icon_size, &width, &height ) )
		width = height = (icon_size == GTK_ICON_SIZE_SMALL_TOOLBAR) ? 16 : 24;

	TRACE(( "get_icon_width_for_toolbar(): icon-width = %d, icon-height = %d\n", width, height ));
	return MAX( width, height );
}

GList *get_selected_tree_row_references( GtkTreeSelection *selection )
{
	GList *rows, *rrs = NULL, *l;
	GtkTreeModel *model;

	rows = gtk_tree_selection_get_selected_rows( selection, &model );
	for ( l = rows; l != NULL; l = l->next )
	{
		GtkTreePath *path = (GtkTreePath *)l->data;
		rrs = g_list_append( rrs, gtk_tree_row_reference_new( model, path ) );
		gtk_tree_path_free( path );
	}
	g_list_free( rows );

	return rrs;
}

GtkSettings *get_settings_for_widget( GtkWidget *widget )
{
	GtkSettings *settings;

	if ( GTK_IS_WIDGET(widget) && gtk_widget_has_screen( widget ) )
		settings = gtk_settings_get_for_screen( gtk_widget_get_screen( widget ) );
	else
		settings = gtk_settings_get_default();

	return settings;
}

const GdkColor *get_widget_base_color( GtkWidget *widget, GtkStateType state )
{
	/*g_return_val_if_fail( GTK_IS_WIDGET(widget), NULL );*/
	if ( !GTK_IS_WIDGET(widget) ) return NULL;
	return (widget->style != NULL) ? &widget->style->base[state] : NULL;
}

gboolean foreach_selected_tree_view_row( GtkTreeView *tree_view,
	gboolean (*func)( GtkTreeView *tree_view, GtkTreePath *path, gchar *path_str, gpointer user_data ), gpointer user_data )
{
	gboolean result = TRUE;

	GtkTreeSelection *selection = gtk_tree_view_get_selection( tree_view );
	GList *rows = gtk_tree_selection_get_selected_rows( selection, NULL );
	GList *row;

	for ( row = rows; row != NULL; row = row->next )
	{
		GtkTreePath *path = (GtkTreePath *)row->data;
		gchar *path_str = gtk_tree_path_to_string( path );

		if ( path_str != NULL )
		{
			result = (*func)( tree_view, path, path_str, user_data );
			g_free( path_str );

			if ( !result ) break;
		}
	}

	g_list_foreach( rows, (GFunc)gtk_tree_path_free, NULL );
	g_list_free( rows );

	return result;
}

GtkWidget *new_entry( gint max_length )
{
	GtkWidget *widget = gtk_entry_new();
	gtk_entry_set_max_length( GTK_ENTRY(widget), max_length );
	return widget;
}

GtkWidget *new_file_chooser_dialog( const gchar *title, GtkWindow *parent, GtkFileChooserAction action )
{
	GtkWidget *chooser;

#ifdef MAEMO

	if ( parent == NULL ) parent = main_window;
	chooser = hildon_file_chooser_dialog_new( parent, action );

#else

	const gchar *first_button_text, *second_button_text;
	GtkResponseType first_response, second_response;

	if ( parent == NULL ) parent = main_window;

	first_button_text = GTK_STOCK_CANCEL;
	first_response = GTK_RESPONSE_CANCEL;
	second_response = GTK_RESPONSE_OK;

	switch ( action )
	{
	case GTK_FILE_CHOOSER_ACTION_OPEN:
		second_button_text = GTK_STOCK_OPEN;
		break;

	case GTK_FILE_CHOOSER_ACTION_SAVE:
		second_button_text = GTK_STOCK_SAVE;
		break;

	default:
		ASSERT( FALSE );
		return NULL;
	}

	if ( get_alternative_button_order() )
		chooser = gtk_file_chooser_dialog_new( title, parent, action,
				second_button_text, second_response,
				first_button_text, first_response,
				NULL );
	else
		chooser = gtk_file_chooser_dialog_new( title, parent, action,
				first_button_text, first_response,
				second_button_text, second_response,
				NULL );

#endif

	return chooser;
}

GtkBox *new_hbox( void )
{
	return GTK_BOX(gtk_hbox_new( FALSE, SPACING ));
}

GtkWidget *new_label( const gchar *str )
{
	GtkWidget *label = gtk_label_new( str );
	gtk_misc_set_alignment( GTK_MISC(label), 0, 0.5 );
	return label;
}

GtkWidget *new_label_with_colon( const gchar *str )
{
	gchar *str_with_colon = g_strconcat( str, ":", NULL );
	GtkWidget *widget = new_label( str_with_colon );
	g_free( str_with_colon );
	return widget;
}

GtkMenuShell *new_menu_bar( void )
{
#ifdef MAEMO
	return GTK_MENU_SHELL(gtk_menu_new());
#else
	return GTK_MENU_SHELL(gtk_menu_bar_new());
#endif
}

GtkDialog *new_modal_dialog_with_ok_cancel( const gchar *title, GtkWindow *parent )
{
	GtkDialog *dialog;

	if ( parent == NULL ) parent = main_window;

	dialog = GTK_DIALOG(( get_alternative_button_order() )
		? gtk_dialog_new_with_buttons( title, parent, GTK_DIALOG_MODAL,
			GTK_STOCK_OK, GTK_RESPONSE_OK, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, NULL )
		: gtk_dialog_new_with_buttons( title, parent, GTK_DIALOG_MODAL,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, GTK_STOCK_OK, GTK_RESPONSE_OK, NULL ));
	gtk_dialog_set_default_response( dialog, GTK_RESPONSE_OK );

	return dialog;
}

GtkScrolledWindow *new_scrolled_window( GtkPolicyType hscrollbar_policy, GtkPolicyType vscrollbar_policy )
{
	GtkScrolledWindow *scrolled_window = GTK_SCROLLED_WINDOW( gtk_scrolled_window_new( NULL, NULL ) );
	gtk_scrolled_window_set_policy( scrolled_window, hscrollbar_policy, vscrollbar_policy );
	return scrolled_window;
}

GtkStatusbar *new_statusbar()
{
	return isMID() ? NULL : GTK_STATUSBAR(gtk_statusbar_new());
}

GtkTable *new_table( guint rows, guint columns )
{
	GtkTable *table = GTK_TABLE(gtk_table_new( rows, columns, FALSE ));

	gtk_container_set_border_width( GTK_CONTAINER(table), BORDER );
	gtk_table_set_row_spacings( table, SPACING );
	gtk_table_set_col_spacings( table, SPACING );

	return table;
}

GtkWindow *new_window( void )
{
	GtkWindow *window;

#ifdef MAEMO
	/* Create HildonWindow and set it to HildonProgram */
	window = GTK_WINDOW( hildon_window_new() );
	hildon_program_add_window( Program, HILDON_WINDOW(window) );
#else
	/* Create GtkWindow as top level window */
	window = GTK_WINDOW( gtk_window_new( GTK_WINDOW_TOPLEVEL ) );
#endif

	return window;
}

void set_dialog_size_request( GtkDialog *dialog, gboolean set_height )
{
	gint width  = ( MID ) ? DIALOG_WIDTH_REQUEST_MID : DIALOG_WIDTH_REQUEST;
	gint height = ( set_height ) ? (width * 3) / 4 : -1;
	register gint tmp;

	tmp = (screen_width * 8) / 10;
	if ( width > tmp ) width = tmp;
	tmp = (screen_height * 8) / 10;
	if ( height > tmp ) height = tmp;

	gtk_widget_set_size_request( GTK_WIDGET(dialog), width, height );
}

void tree_view_queue_resize( GtkTreeView *tree_view, int num_columns )
{
	int i;

	if ( num_columns > 0 )
	{
		for ( i = 0; i < num_columns; i++ )
		{
			GtkTreeViewColumn *column = gtk_tree_view_get_column( tree_view, i );
			if ( column != NULL ) gtk_tree_view_column_queue_resize( column );
		}
	}
	else
	{
		for ( i = 0;; i++ )
		{
			GtkTreeViewColumn *column = gtk_tree_view_get_column( tree_view, i );
			if ( column == NULL ) break;
			gtk_tree_view_column_queue_resize( column );
		}
	}
}

/*-----------------------------------------------------------------------------------------------*/
/*
	Workaround for flaw in older GTK+ versions:
	A double click on a selected row will produce *two* "row-activated" events
*/

gboolean row_activated_flag;

static gboolean clear_row_activated_flag( gpointer user_data )
{
	row_activated_flag = FALSE;
	return FALSE;
}

void set_row_activated_flag( void )
{
	row_activated_flag = TRUE;
	g_idle_add( clear_row_activated_flag, NULL );
}

/*-----------------------------------------------------------------------------------------------*/
/*
	fix_hildon_tool_button() is a workaround
	for flaws in older GTK+ versions (MAEMO only!?):

	1. The toolbar item width is wrong when using an item image which must be scaled
	2. The toolbar item height is wrong in toolbars with GTK_ORIENTATION_VERTICAL
*/
#ifdef MAEMO
void fix_hildon_tool_button( GtkToolItem *item )
{
	if ( gtk_major_version == 2 && gtk_minor_version < 11 )
	{
		gtk_widget_set_size_request( GTK_WIDGET(item), 40, 40 );

		g_object_set( item, "can-default", FALSE, "can-focus", FALSE, "receives-default", FALSE, NULL );
		GTK_WIDGET_FLAGS( item ) &= ~(GTK_CAN_DEFAULT|GTK_CAN_FOCUS|GTK_RECEIVES_DEFAULT);
	}
}
#endif

/*-----------------------------------------------------------------------------------------------*/
