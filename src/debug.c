/*
	debug.c
	Copyright (C) 2008-2013 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)

	--------------------------------------------------------------------

	This file is part of the Gwenevieve application.

	Gwenevieve is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Gwenevieve is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "debug.h"

/*-----------------------------------------------------------------------------------------------*/

#if LOGLEVEL > 0

#ifdef WIN32
#define STRICT
#define WIN32_LEAN_AND_MEAN  /* Exclude rarely-used stuff from Windows headers */
#include <windows.h>
#endif

#include <gtk/gtkmain.h>
#include <glib/gstdio.h>

#include <stdarg.h>
#include <string.h>

GMutex *log_mutex;
gchar *log_filename;
FILE *log_file;

static void print( const gchar *prefix, const gchar *string )
{
	g_mutex_lock( log_mutex );

#ifdef WIN32
	OutputDebugString( prefix );
	OutputDebugString( string );
#else
	fputs( prefix, stdout );
	fputs( string, stdout );
	/*fflush( stdout );*/
#endif

	if ( log_file != NULL )
	{
		fputs( prefix, log_file );
		fputs( string, log_file );
		fflush( log_file );
	}

	g_mutex_unlock( log_mutex );
}

static void print_handler( const gchar *string )
{
	print( "", string );
}

static void printwarn_handler( const gchar *string )
{
	print( "*** ", string );
}

static void printerr_handler( const gchar *string )
{
	print( "******* ", string );
}

static void log_handler( const gchar *log_domain, GLogLevelFlags log_level, const gchar *message, gpointer user_data )
{
	if ( (log_level & G_LOG_FLAG_FATAL) != 0 )
		printerr_handler( "FATAL:\n" );

	if ( (log_level & (G_LOG_LEVEL_ERROR|G_LOG_LEVEL_CRITICAL)) != 0 )
		printerr_handler( message );
	else if ( (log_level & G_LOG_LEVEL_WARNING) != 0 )
		printwarn_handler( message );
	else
		print_handler( message );
}

static gchar *build_logfilename( const gchar *basename, const gchar *extension )
{
#ifndef NOLOGFILE
	if ( basename != NULL )
	{
	#if defined( WIN32 )
		const gchar *my_docs_dir = g_get_user_data_dir();
	#elif defined( MAEMO )
		const gchar *my_docs_dir = g_getenv( "MYDOCSDIR" );
	#else
		const gchar *my_docs_dir = g_get_home_dir();
	#endif

		if ( my_docs_dir != NULL )
		{
			time_t timer = time( NULL );
			char filename[64];
			size_t i;

			i = strlen( strcpy( filename, basename ) );
			strftime( filename + i, sizeof( filename ) - i - strlen( extension ),
				"_%Y-%m-%d-%H-%M.", localtime( &timer ) );
			strcat( filename, extension );

			return g_build_filename( my_docs_dir, filename, NULL );
		}
	}
#endif

	return NULL;
}

static void open_logfile( const char *basename )
{
	log_filename = build_logfilename( basename, "log" );
	if ( log_filename != NULL )
	{
		log_file = fopen_utf8( log_filename, "w" );
		if ( log_file != NULL )
		{
#ifdef WIN32
			OutputDebugString( "Log file: " );
			OutputDebugString( log_filename );
			OutputDebugString( "\n" );
#else
			printf( "Log file: %s\n", log_filename );
#endif
		}
	}
}

static void close_logfile( gboolean remove_logfile )
{
	if ( log_file != NULL )
	{
		fclose( log_file );
		log_file = NULL;
	}

	if ( log_filename != NULL )
	{
		if ( remove_logfile ) g_remove( log_filename );

		g_free( log_filename );
		log_filename = NULL;
	}
}

#endif

/*-----------------------------------------------------------------------------------------------*/

void debug_init( const char *basename, const char *log_domain, ... )
{
#if LOGLEVEL > 0
	va_list argp;

	log_mutex = g_mutex_new();

	g_set_print_handler( print_handler );
	g_set_printerr_handler( printerr_handler );

	va_start( argp, log_domain );
	for (;;)
	{
		g_log_set_handler( log_domain, (1 << G_LOG_LEVEL_USER_SHIFT)-1, log_handler, NULL );
		if ( log_domain == NULL ) break;
		log_domain = va_arg( argp, const char * );
	}
	va_end( argp );

	open_logfile( basename );

#if defined( WIN32 )
{
	OSVERSIONINFO vi = {0};
	vi.dwOSVersionInfoSize = sizeof( vi );
	GetVersionEx( &vi );
	g_print( "Win32 %lu.%lu.%lu %s\n", vi.dwMajorVersion, vi.dwMinorVersion, vi.dwBuildNumber, vi.szCSDVersion );
}
#elif defined( MAEMO )
	g_print( "Maemo\n" );
#else
	g_print( "Linux\n" );
#endif
	g_print( "GTK %u.%u.%u\n", gtk_major_version, gtk_minor_version, gtk_micro_version );
	g_print( "GLib %u.%u.%u\n", glib_major_version, glib_minor_version, glib_micro_version );
/*	g_print( "sizeof( short ) = %d, sizeof( int ) = %d, sizeof( long ) = %d\n", (int)sizeof( short ), (int)sizeof( int ), (int)sizeof( long ) ); */
#endif
}

void debug_exit( gboolean remove_logfile )
{
#if LOGLEVEL > 0
	close_logfile( remove_logfile );

	g_mutex_free( log_mutex );
	log_mutex = NULL;
#endif
}

/*-----------------------------------------------------------------------------------------------*/

FILE *fopen_utf8( const char *filename, const char *mode )
{
#ifdef G_OS_WIN32
	gchar *locale_filename = g_locale_from_utf8( filename, -1, NULL, NULL, NULL );
	if ( locale_filename != NULL )
	{
		FILE *fp = fopen( locale_filename, mode );
		g_free( locale_filename );
		return fp;
	}

	return NULL;
#else
	return fopen( filename, mode );
#endif
}

/*-----------------------------------------------------------------------------------------------*/
