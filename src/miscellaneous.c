/*
	miscellaneous.c
	Copyright (C) 2008-2013 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)

	--------------------------------------------------------------------

	This file is part of the Gwenevieve application.

	Gwenevieve is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Gwenevieve is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gwenevieve.h"

/*-----------------------------------------------------------------------------------------------*/

#ifdef G_OS_WIN32
#undef fopen
FILE *fopen_utf8( const char *filename, const char *mode )
{
	gchar *locale_filename = g_locale_from_utf8( filename, -1, NULL, NULL, NULL );
	if ( locale_filename != NULL )
	{
		FILE *fp = fopen( locale_filename, mode );
		g_free( locale_filename );
		return fp;
	}

	return NULL;
}
#endif

/*-----------------------------------------------------------------------------------------------*/

int row_activated_flag;

static gboolean clear_row_activated_flag( gpointer user_data )
{
	row_activated_flag = FALSE;
	return FALSE;
}

void set_row_activated_flag( void )
{
	row_activated_flag = TRUE;
	g_idle_add( clear_row_activated_flag, NULL );
}

/*-----------------------------------------------------------------------------------------------*/

static gint _busy_count;
static GdkCursorType _cursor_type;
static gboolean _cursor_type_is_valid;

static void __set_cursor( GdkCursor *cursor )
{
	GtkWidget *widget = GTK_WIDGET(MainWindow);
	ASSERT( widget != NULL );
	gdk_window_set_cursor( gtk_widget_get_window( widget ), cursor );
}

static void _set_cursor( GdkCursorType cursor_type )
{
	GdkCursor *cursor = gdk_cursor_new/*_for_display*/(
		/*gtk_widget_get_display( widget, GDK_XTERM ),*/
		cursor_type );
	__set_cursor( cursor );
	gdk_cursor_unref( cursor );
}

static void set_non_busy_cursor( void )
{
	if ( _busy_count == 0 )
	{
		if ( _cursor_type_is_valid )
		{
			/* Set custom cursor */
			_set_cursor( _cursor_type );
		}
		else
		{
			/* Set default cursor */
			__set_cursor( NULL );
		}
	}
}

void busy_enter( void /*GtkWidget* widget*/ )
{
	if ( _busy_count++ == 0 ) _set_cursor( GDK_WATCH );
}

void busy_leave( void /*GtkWidget* widget*/ )
{
	ASSERT( _busy_count > 0 );
	if ( _busy_count > 0 ) { --_busy_count; set_non_busy_cursor(); }
}

void set_cursor( GdkCursorType cursor_type )
{
	if ( !_cursor_type_is_valid || _cursor_type != cursor_type )
	{
		_cursor_type = cursor_type;
		_cursor_type_is_valid = TRUE;
		set_non_busy_cursor();
	}
}

void set_default_cursor( void )
{
	if ( _cursor_type_is_valid )
	{
		_cursor_type_is_valid = FALSE;
		set_non_busy_cursor();
	}
}

/*-----------------------------------------------------------------------------------------------*/

void set_main_window_title( const char *title )
{
#ifdef MAEMO
	gtk_window_set_title( GTK_WINDOW(MainWindow), (title != NULL) ? title : "" );
#else
	if ( title != NULL && *title != '\0' )
	{
		gchar *window_title = g_strdup_printf( "%s - %s", NAME, title );
		gtk_window_set_title( MainWindow, window_title );
		g_free( window_title );
	}
	else
	{
		gtk_window_set_title( MainWindow, NAME );
	}
#endif
}

void set_screen_settings( void )
{
	settings->screen_width  = gdk_screen_width();
	settings->screen_height = gdk_screen_height();

#ifdef MAEMO
	settings->MID = TRUE;
#else
{
	/*const gchar *desktop_session = g_getenv( "DESKTOP_SESSION" );*/  /* "LXDE" */
	settings->MID = settings->screen_width <= 800 && settings->screen_height <= 480;  /* WVGA (800×480) */
}
#endif
}

/*-----------------------------------------------------------------------------------------------*/

gint message_dialog( GtkMessageType type, const gchar *description )
{
	GtkWindow* parent = GTK_WINDOW(MainWindow);
	gboolean cancel_as_default_button = FALSE;
#ifdef MAEMO
	const gchar *icon_name;
#endif
	GtkWidget* dialog;
	gint result;

	if ( type == GTK_MESSAGE_QUESTION_WITH_CANCEL_AS_DEFAULT_BUTTON )
	{
		type = GTK_MESSAGE_QUESTION;
		cancel_as_default_button = TRUE;
	}

#ifdef MAEMO

	switch ( type )
	{
	case GTK_MESSAGE_INFO:
		icon_name = GTK_STOCK_DIALOG_INFO;
		break;
	case GTK_MESSAGE_WARNING:
		icon_name = GTK_STOCK_DIALOG_WARNING;
		break;
	case GTK_MESSAGE_QUESTION:
		icon_name = GTK_STOCK_DIALOG_QUESTION;
		break;
	case GTK_MESSAGE_ERROR:
		icon_name = GTK_STOCK_DIALOG_ERROR;
		break;
	default:
		icon_name = NULL;
	}

	dialog = (( type == GTK_MESSAGE_QUESTION )
		? hildon_note_new_confirmation_with_icon_name
		: hildon_note_new_information_with_icon_name)
			( parent, description, icon_name );

#else

	dialog = gtk_message_dialog_new( parent, GTK_DIALOG_MODAL, type,
		( type == GTK_MESSAGE_QUESTION ) ? GTK_BUTTONS_OK_CANCEL : GTK_BUTTONS_OK,
		"%s", description );

#endif

	gtk_dialog_set_default_response( GTK_DIALOG(dialog), ( cancel_as_default_button ) ? GTK_RESPONSE_CANCEL : GTK_RESPONSE_OK );

	gtk_widget_show_all( dialog );
	result = gtk_dialog_run( GTK_DIALOG(dialog) );
	gtk_widget_destroy( dialog );

	return result;
}

/*-----------------------------------------------------------------------------------------------*/

#ifndef MAEMO
static gboolean animation_func( gpointer user_data )
{
	gboolean result = FALSE;

	gdk_threads_enter();

	if ( GTK_IS_WIDGET( user_data ) )
	{
		GtkProgressBar *progress_bar = GTK_PROGRESS_BAR( user_data );
		gtk_progress_bar_pulse( progress_bar );
		result = TRUE;
	}

	gdk_threads_leave();
	return result;
}
#endif

static GtkWidget *animation_show( const char *action, const gchar *item )
{
	GString *text = g_string_new( action );
	GtkWidget *animation;

#ifdef MAEMO

	if ( item != NULL )
	{
		g_string_append_c( text, ' ' );
		g_string_append( text, "`" );
		g_string_append( text, item );
		g_string_append( text, "'" );
	}

	animation = hildon_banner_show_animation( GTK_WIDGET(MainWindow), /*animation_name*/ NULL, text->str );

#else

	GtkWindow *parent = GTK_WINDOW(MainWindow);
	GtkVBox *vbox;
	GtkLabel *label;
	GtkProgressBar *progress_bar;
	GdkCursor* cursor;
	guint timeout_id;

	g_string_append( text, "..." );

	animation = gtk_window_new( GTK_WINDOW_TOPLEVEL );
	gtk_window_set_type_hint( GTK_WINDOW(animation), GDK_WINDOW_TYPE_HINT_DIALOG );
	gtk_window_set_title( GTK_WINDOW(animation), ""/*text->str*/ );
	gtk_window_set_decorated( GTK_WINDOW(animation), TRUE );
	gtk_window_set_deletable( GTK_WINDOW(animation), FALSE );
	gtk_window_set_modal( GTK_WINDOW(animation), TRUE );
	gtk_window_set_transient_for( GTK_WINDOW(animation), parent );
	gtk_window_set_destroy_with_parent( GTK_WINDOW(animation), TRUE );
	gtk_container_set_border_width( GTK_CONTAINER(animation), 6 );

	vbox = GTK_VBOX( gtk_vbox_new( FALSE, 6 ) );
	gtk_container_add( GTK_CONTAINER(animation), GTK_WIDGET(vbox) );

	label = GTK_LABEL( gtk_label_new( text->str ) );
	gtk_misc_set_alignment( GTK_MISC(label), 0, 0.5 );
	gtk_box_pack_start_defaults( GTK_BOX(vbox), GTK_WIDGET(label) );

	progress_bar = GTK_PROGRESS_BAR( gtk_progress_bar_new() );
	if ( item != NULL ) gtk_progress_bar_set_text( progress_bar, item );
	/*gtk_widget_set_size_request( progress_bar, 150, -1 );*/
	gtk_box_pack_start_defaults( GTK_BOX(vbox), GTK_WIDGET(progress_bar) );

	gtk_window_set_position( GTK_WINDOW(animation), GTK_WIN_POS_CENTER_ON_PARENT );
	gtk_widget_show_all( animation );
	timeout_id = g_timeout_add( 250, animation_func, progress_bar );
	g_object_set_data( G_OBJECT(animation), "timeout_id", GUINT_TO_POINTER( timeout_id ) );

	cursor = gdk_cursor_new/*_for_display*/(
			/*gtk_widget_get_display( widget, GDK_XTERM ),*/
			GDK_WATCH );
	gdk_window_set_cursor( gtk_widget_get_window( animation ), cursor );
	gdk_cursor_unref( cursor );

#endif

	g_string_free( text, TRUE );
	return animation;
}

static gboolean animation_timeout( gpointer user_data )
{
	struct animation *a = (struct animation *)user_data;

	if ( a->timeout_id != 0 )  /* Thread is still running */
	{
		gdk_threads_enter();

		if ( a->timeout_id != 0 )  /* Thread is still running */
		{
			a->timeout_id = 0;

			a->widget = animation_show( a->action, a->item );
			g_free( a->item );
			a->item = NULL;
		}

		gdk_threads_leave();
	}

	return FALSE;
}

struct animation *show_animation( const char *action, const gchar *item, guint interval )
{
	struct animation *a = g_new0( struct animation, 1 );

	busy_enter();

	if ( interval != 0 )
	{
		a->action = /*g_strdup(*/ action /*)*/;
		a->item = g_strdup( item );
		a->timeout_id = g_timeout_add( interval, animation_timeout, a );
	}
	else
	{
		a->widget = animation_show( action, item );
	}

	return a;
}

void cancel_animation( struct animation *animation )
{
	if ( animation != NULL )
	{
		guint timeout_id = animation->timeout_id;
		animation->timeout_id = 0;
		if ( timeout_id != 0 ) g_source_remove( timeout_id );
	}
}

void clear_animation( struct animation *animation )
{
	if ( animation != NULL )
	{
		cancel_animation( animation );

		if ( animation->widget != NULL )
		{
		#ifndef MAEMO

			guint timeout_id = GPOINTER_TO_UINT( g_object_get_data( G_OBJECT(animation->widget), "timeout_id" ) );
			if ( timeout_id != 0 ) g_source_remove( timeout_id );

		#endif

			gtk_widget_destroy( animation->widget );
		}

		g_free( animation->item );
		/*g_free( animation->action );*/
		g_free( animation );

		busy_leave();
	}
}

/*-----------------------------------------------------------------------------------------------*/

#ifdef MAEMO
#include <hildon/hildon-file-chooser-dialog.h>
#endif

void attach_to_table( GtkTable *table, GtkWidget *child, guint top_attach, guint left_attach, guint right_attach )
{
	gtk_table_attach( table, child, left_attach, right_attach, top_attach, top_attach + 1,
		GTK_IS_LABEL(child) ? GTK_FILL : GTK_FILL|GTK_EXPAND, GTK_FILL, 0, 0 );
}

gboolean get_alternative_button_order( void )
{
#ifdef MAEMO
	return TRUE;
#else
	GtkSettings *settings = gtk_settings_get_default();
	gboolean result = FALSE;
	g_object_get( settings, "gtk-alternative-button-order", &result, NULL );
	return result;
#endif
}

GList *get_selected_tree_row_references( GtkTreeSelection *selection )
{
	GList *rows, *rrs = NULL, *l;
	GtkTreeModel *model;

	rows = gtk_tree_selection_get_selected_rows( selection, &model );
	for ( l = rows; l != NULL; l = l->next )
	{
		GtkTreePath *path = (GtkTreePath *)l->data;
		rrs = g_list_append( rrs, gtk_tree_row_reference_new( model, path ) );
		gtk_tree_path_free( path );
	}
	g_list_free( rows );

	return rrs;
}

GtkSettings *get_settings_for_widget( GtkWidget *widget )
{
	GtkSettings *settings;

	if ( GTK_IS_WIDGET(widget) && gtk_widget_has_screen( widget ) )
		settings = gtk_settings_get_for_screen( gtk_widget_get_screen( widget ) );
	else
		settings = gtk_settings_get_default();

	return settings;
}

const GdkColor *get_widget_base_color( GtkWidget *widget, GtkStateType state )
{
	/*g_return_val_if_fail( GTK_IS_WIDGET(widget), NULL );*/
	if ( !GTK_IS_WIDGET(widget) ) return NULL;
	return (widget->style != NULL) ? &widget->style->base[state] : NULL;
}

GdkWindow *get_widget_window( GtkWidget *widget )
{
	/*g_return_val_if_fail( GTK_IS_WIDGET(widget), NULL );*/
	if ( !GTK_IS_WIDGET(widget) ) return NULL;
	return widget->window;
}

GtkWidget *new_entry( gint max_length )
{
	GtkWidget *widget = gtk_entry_new();
	gtk_entry_set_max_length( GTK_ENTRY(widget), max_length );
	return widget;
}

GtkWidget *new_file_chooser_dialog( const gchar *title, GtkWindow *parent, GtkFileChooserAction action )
{
	GtkWidget *chooser;

#ifdef MAEMO

	if ( parent == NULL ) parent = GTK_WINDOW(MainWindow);
	chooser = hildon_file_chooser_dialog_new( parent, action );

#else

	const gchar *first_button_text, *second_button_text;
	GtkResponseType first_response, second_response;

	if ( parent == NULL ) parent = GTK_WINDOW(MainWindow);

	first_button_text = GTK_STOCK_CANCEL;
	first_response = GTK_RESPONSE_CANCEL;
	second_response = GTK_RESPONSE_OK;

	switch ( action )
	{
	case GTK_FILE_CHOOSER_ACTION_OPEN:
		second_button_text = GTK_STOCK_OPEN;
		break;

	case GTK_FILE_CHOOSER_ACTION_SAVE:
		second_button_text = GTK_STOCK_SAVE;
		break;

	default:
		ASSERT( FALSE );
		return NULL;
	}

	if ( get_alternative_button_order() )
		chooser = gtk_file_chooser_dialog_new( title, parent, action,
				second_button_text, second_response,
				first_button_text, first_response,
				NULL );
	else
		chooser = gtk_file_chooser_dialog_new( title, parent, action,
				first_button_text, first_response,
				second_button_text, second_response,
				NULL );

#endif

	return chooser;
}

GtkBox *new_hbox( void )
{
	return GTK_BOX(gtk_hbox_new( FALSE, SPACING ));
}

GtkWidget *new_label( const gchar *str )
{
	GtkWidget *label = gtk_label_new( str );
	gtk_misc_set_alignment( GTK_MISC(label), 0, 0.5 );
	return label;
}

GtkWidget *new_label_with_colon( const gchar *str )
{
	gchar *str_with_colon = g_strconcat( str, ":", NULL );
	GtkWidget *widget = new_label( str_with_colon );
	g_free( str_with_colon );
	return widget;
}

GtkDialog *new_modal_dialog_with_ok_cancel( const gchar *title, GtkWindow *parent )
{
	GtkDialog *dialog;

	if ( parent == NULL ) parent = GTK_WINDOW(MainWindow);

	dialog = GTK_DIALOG(( get_alternative_button_order() )
		? gtk_dialog_new_with_buttons( title, parent, GTK_DIALOG_MODAL,
			GTK_STOCK_OK, GTK_RESPONSE_OK, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, NULL )
		: gtk_dialog_new_with_buttons( title, parent, GTK_DIALOG_MODAL,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, GTK_STOCK_OK, GTK_RESPONSE_OK, NULL ));
	gtk_dialog_set_default_response( dialog, GTK_RESPONSE_OK );

	return dialog;
}

/*
GtkWidget *new_scrolled_window( void )
{
	GtkWidget *scrolled_window = gtk_scrolled_window_new( NULL, NULL );
	gtk_scrolled_window_set_policy( GTK_SCROLLED_WINDOW(scrolled_window), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC );
	return scrolled_window;
}
*/

GtkTable *new_table( guint rows, guint columns )
{
	GtkTable *table = GTK_TABLE(gtk_table_new( rows, columns, FALSE ));

	gtk_container_set_border_width( GTK_CONTAINER(table), BORDER );
	gtk_table_set_row_spacings( table, SPACING );
	gtk_table_set_col_spacings( table, SPACING );

	return table;
}

void set_cell_renderer_visible( GtkCellRenderer *cell, gboolean visible )
{
	g_return_if_fail( GTK_IS_CELL_RENDERER(cell) );
	g_object_set( cell, "visible", visible, NULL );
}

#define DIALOG_WIDTH_REQUEST      480
#define DIALOG_WIDTH_REQUEST_MID  640

void set_dialog_size_request( GtkDialog *dialog, gboolean set_height )
{
	gint width  = ( settings->MID ) ? DIALOG_WIDTH_REQUEST_MID : DIALOG_WIDTH_REQUEST;
	gint height = ( set_height ) ? (width * 3) / 4 : -1;
	register gint tmp;

	tmp = (settings->screen_width * 8) / 10;
	if ( width > tmp ) width = tmp;
	tmp = (settings->screen_height * 8) / 10;
	if ( height > tmp ) height = tmp;

	gtk_widget_set_size_request( GTK_WIDGET(dialog), width, height );
}

#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 12
void set_tool_item_tooltip_text( GtkToolItem *tool_item, const gchar *text )
{
	static GtkTooltips *tooltips;

	if ( tooltips != NULL ) tooltips = gtk_tooltips_new();
	gtk_tool_item_set_tooltip( tool_item, tooltips, text, NULL );
}
#endif

void set_widget_visible( GtkWidget *widget, gboolean visible )
{
	g_return_if_fail( GTK_IS_WIDGET(widget) );
	g_object_set( widget, "visible", visible, NULL );
}

void tree_view_queue_resize( GtkTreeView *tree_view, int num_columns )
{
	int i;

	for ( i = 0; i < num_columns; i++ )
	{
		GtkTreeViewColumn *column = gtk_tree_view_get_column( tree_view, i );
		if ( column != NULL ) gtk_tree_view_column_queue_resize( column );
	}
}

/*-----------------------------------------------------------------------------------------------*/

gboolean is_smartq( void )
{
#if !defined( MAEMO ) && !defined( WIN32 )
	const gchar *host_name = g_get_host_name();
	return ( host_name != NULL && strcmp( host_name, "SmartQ" ) == 0 );
#else
	return FALSE;
#endif
}

/*-----------------------------------------------------------------------------------------------*/

