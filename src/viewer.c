/*
	viewer.c
	Copyright (C) 2010-2013 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)

	--------------------------------------------------------------------

	This file is part of the Gwenevieve application.

	Gwenevieve is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Gwenevieve is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gwenevieve.h"

/*-----------------------------------------------------------------------------------------------*/

GtkEventBox *viewer_event_box;
GtkImage *viewer_widget;
GError *viewer_error;
const struct file *viewer_image;
gint viewer_image_width, viewer_image_height;
gboolean viewer_is_valid;

GTimer *viewer_timer;
gdouble viewer_pause;

gboolean update_viewer_idle( gpointer user_data );

static gboolean button_press_event( GtkWidget *widget, GdkEventButton *event, gpointer user_data );

/*-----------------------------------------------------------------------------------------------*/

GtkWidget *create_viewer( void )
{
	GtkBox *vbox = GTK_BOX( gtk_vbox_new( FALSE, 0 ) );
	GtkBox *hbox = GTK_BOX( gtk_hbox_new( FALSE, 0 ) );
	GdkColor color;

	viewer_event_box = GTK_EVENT_BOX( gtk_event_box_new() );
	viewer_widget = g_object_new( GTK_TYPE_IMAGE, NULL );

	gtk_box_pack_start( hbox, GTK_WIDGET(viewer_widget), TRUE, FALSE, 0 );
	gtk_box_pack_start( vbox, GTK_WIDGET(hbox), TRUE, FALSE, 0 );

	gdk_color_parse( "#000000", &color );
	gtk_widget_modify_bg( GTK_WIDGET(viewer_event_box), GTK_STATE_NORMAL, &color );
	gtk_container_add( GTK_CONTAINER(viewer_event_box), GTK_WIDGET(vbox) );

	g_signal_connect( G_OBJECT(viewer_event_box), "button-press-event", G_CALLBACK(button_press_event), NULL );

	return GTK_WIDGET(viewer_event_box);
}

void set_viewer_page( void )
{
	gtk_notebook_set_current_page( notebook, viewer_page );
	if ( viewer_image != NULL ) set_main_window_title( viewer_image->name );
	set_viewer_cursor();
}

void set_viewer_cursor( void )
{
#if !(GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 16)  /* GDK_BLANK_CURSOR is defined since 2.16 */
	if ( get_main_window_fullscreen() )
		set_main_window_cursor( GDK_BLANK_CURSOR );
	else
		set_main_window_cursor_default();
#endif
}

void clear_viewer( void )
{
	invalidate_viewer();
	gtk_image_clear( viewer_widget );
}

void invalidate_viewer( void )
{
	viewer_is_valid = FALSE;
	viewer_image = NULL;

	if ( viewer_error != NULL )
	{
		g_error_free( viewer_error );
		viewer_error = NULL;
	}
}

static const struct file *first_image( void )
{
	return get_first_file( selected_folder, selected_subfolder );
}

static const struct file *last_image( void )
{
	return get_last_file( selected_folder, selected_subfolder );
}

static const struct file *previous_image( void )
{
	const struct file *image = get_previous_file( selected_folder, selected_subfolder, viewer_image );
	if ( image == NULL ) image = (settings->view.wrap_around) ? last_image() : viewer_image;
	return image;
}

static const struct file *next_image( void )
{
	const struct file *image = get_next_file( selected_folder, selected_subfolder, viewer_image );
	if ( image == NULL ) image = (settings->view.wrap_around) ? first_image() : viewer_image;
	return image;
}

static const struct file *random_image( void )
{
	return get_random_file( selected_folder, selected_subfolder );
}

static void size_prepared( GdkPixbufLoader *loader, gint width, gint height, gpointer user_data )
{
	GdkPixbufRotation *angle = (GdkPixbufRotation *)user_data;
	gint view_width, view_height, image_width, image_height;

	viewer_image_width = image_width = width;
	viewer_image_height = image_height = height;

#if 0
	if ( is_fullscreen() )
	{
		view_width = settings->screen_width;
		view_height = settings->screen_height;
	}
	else
	{
		view_width = settings->view_width;
		view_height = settings->view_height;
	}
#else
	view_width  = GTK_WIDGET(viewer_event_box)->allocation.width;
	view_height = GTK_WIDGET(viewer_event_box)->allocation.height;
#endif
	TRACE(( "size_prepared(): view_width = %d, view_height = %d\n", view_width, view_height ));

	if ( settings->view.auto_rotate && height > width && ( settings->view.scale_up || height > view_height ) )
	{
		gint tmp = view_width;
		view_width = view_height;
		view_height = tmp;
		*angle = GDK_PIXBUF_ROTATE_CLOCKWISE;
	}
	else
	{
		*angle = GDK_PIXBUF_ROTATE_NONE;
	}

	/* TODO: Allow very small margin, i.e. do not always scale up here */
	if ( image_width > view_width || ( settings->view.scale_up && image_width < view_width ) )
	{
		image_width  = view_width;
		image_height = (height * image_width + width/2 ) / width;
	}
	if ( image_height > view_height )
	{
		image_height = view_height;
		image_width  = (width * image_height + height/2 ) / height;
	}

	TRACE(( "current_size_prepared( %d, %d ) => %d, %d\n", width, height, image_width, image_height ));
	if ( width != image_width )
	{
		TRACE(( "gdk_pixbuf_loader_set_size( %d, %d )\n", image_width, image_height ));
		gdk_pixbuf_loader_set_size( loader, image_width, image_height );
	}
}

GdkPixbuf *new_pixbuf_from_image( const struct file *image, GError **pixbuf_error )
{
	GdkPixbuf *pixbuf = take_from_viewer_cache( image );
	if ( pixbuf == NULL )
	{
		gchar *filename, *contents = NULL;
		gsize length = 0;
		GError *error = NULL;

		GdkPixbufRotation angle = GDK_PIXBUF_ROTATE_NONE;

		filename = build_filename( image );
		g_print( "image: %s (%lu)\n", filename, (unsigned long)image->size );
		g_file_get_contents( filename, &contents, &length, &error );
		g_free( filename );
		handle_error( error, "Error while loading file %s", image->name );

		if ( contents != NULL )
		{
			GdkPixbufLoader *loader = gdk_pixbuf_loader_new();

			/* Scale image */
			g_signal_connect( G_OBJECT(loader), "size-prepared", G_CALLBACK(size_prepared), &angle );

			if ( gtk_major_version == 2 && gtk_minor_version < 12 )
			{
				/* Workaround for bug in GtkPixbufLoader (GTK+ 2.10.12) */
				gchar *part = contents;

				while ( length > 0 )
				{
					gsize len = (length > 16*1024) ? 16*1024 : length;

					if ( !gdk_pixbuf_loader_write( loader, (guchar *)part, len, pixbuf_error ) )
					{
						TRACE(( "*** new_pixbuf_from_image(): gdk_pixbuf_loader_write() failed\n" ));
						break;
					}

					part   += len;
					length -= len;
				}
			}
			else if ( !gdk_pixbuf_loader_write( loader, (guchar *)contents, length, pixbuf_error ) )
			{
				TRACE(( "*** new_pixbuf_from_image(): gdk_pixbuf_loader_write() failed\n" ));
			}

			if ( !gdk_pixbuf_loader_close( loader, (pixbuf_error != NULL && *pixbuf_error == NULL) ? pixbuf_error : NULL ) )
			{
				TRACE(( "*** new_pixbuf_from_image(): gdk_pixbuf_loader_close() failed\n" ));
			}

			pixbuf = gdk_pixbuf_loader_get_pixbuf( loader );
			if ( pixbuf != NULL )
			{
				g_object_ref( pixbuf );
			}
			else
			{
				TRACE(( "*** new_pixbuf_from_image(): gdk_pixbuf_loader_get_pixbuf() failed\n" ));

				if ( pixbuf_error != NULL && *pixbuf_error == NULL )
					g_set_error( pixbuf_error, 0, 0, "gdk_pixbuf_loader_get_pixbuf() failed" );
			}

			g_object_unref( loader );  /* Free pixbuf loader */
			g_free( contents );        /* Free file contents */
		}

		/* Set GtkImage to GdkPixbuf */

		if ( pixbuf != NULL )
		{
			TRACE(( "pixbuf->ref_count = %d\n", G_OBJECT(pixbuf)->ref_count ));
			ASSERT( G_OBJECT(pixbuf)->ref_count == 1 );

			if ( angle != GDK_PIXBUF_ROTATE_NONE )
			{
				GdkPixbuf *new_pixbuf = gdk_pixbuf_rotate_simple( pixbuf, angle );
				ASSERT( G_OBJECT(new_pixbuf)->ref_count == 1 );

				g_object_unref( pixbuf );
				pixbuf = new_pixbuf;
			}
		}
	}

	add_to_viewer_cache( image, pixbuf );
	return pixbuf;
}

void set_viewer_image( const struct file *image )
{
	invalidate_viewer();
	viewer_image = image;
	set_main_window_title( (image != NULL) ? image->name : NULL );
}

static gboolean set_viewer_pixbuf_idle( gpointer user_data )
{
	if ( viewer_image != NULL ) viewer_is_valid = TRUE;
	return FALSE;
}

void set_viewer_pixbuf( GdkPixbuf *pixbuf )
{
	if ( pixbuf != NULL )
	{
		ASSERT( G_OBJECT(pixbuf)->ref_count >= 1 );
		gtk_image_set_from_pixbuf( viewer_widget, pixbuf );
		ASSERT( G_OBJECT(pixbuf)->ref_count >= 2 );
		g_object_unref( pixbuf );
		ASSERT( G_OBJECT(pixbuf)->ref_count >= 1 );
	}
	else
	{
		gtk_image_set_from_stock( viewer_widget, GTK_STOCK_MISSING_IMAGE, GTK_ICON_SIZE_BUTTON );
	}

	g_idle_add( set_viewer_pixbuf_idle, NULL );
}

void update_viewer( const struct file *image )
{
	TRACE(( "update_viewer(): image = %p => %p\n", (void *)viewer_image, (void *)image ));

	if ( image != viewer_image )
	{
		if ( image != NULL )
		{
			busy_enter();
			set_viewer_image( image );
			g_idle_add( update_viewer_idle, (gpointer)image );
		}
		else
		{
			set_main_window_title( NULL );
			clear_viewer();
		}
	}
}

void update_viewer_pixbuf( const struct file *image )
{
	busy_enter();
	viewer_image = image;
	update_viewer_idle( (gpointer)image );
}

gboolean update_viewer_idle( gpointer user_data )
{
	const struct file *image = (const struct file *)user_data;

	if ( image == viewer_image && !viewer_is_valid )
	{
		GdkPixbuf *pixbuf = new_pixbuf_from_image( image, &viewer_error );

		gdk_threads_enter();
		set_viewer_pixbuf( pixbuf );
		gdk_threads_leave();
	}

	busy_leave();
	return FALSE;
}

/*-----------------------------------------------------------------------------------------------*/

gboolean viewer_key_press_event( GtkWidget *widget, GdkEventKey *event )
{
	if ( stop_slide_show( TRUE ) ) return TRUE;

	switch ( event->keyval )
	{
	case GDK_BackSpace:
	case GDK_Escape:
		/*clear_viewer();*/
		invalidate_viewer();
		set_main_window_cursor_default();
		set_selection_page();
		break;

	case GDK_Left:
	case GDK_Up:
	case GDK_Page_Up:
	case GDK_F8:
		if ( !sorting_in_progress && viewer_image != NULL && viewer_is_valid )
			update_viewer( previous_image() );
		break;

	case GDK_space:
		/* pass through */

	case GDK_Right:
	case GDK_Down:
	case GDK_Page_Down:
	case GDK_F7:
		if ( !sorting_in_progress && viewer_image != NULL && viewer_is_valid )
			update_viewer( next_image() );
		break;

	case GDK_Home:
		if ( !sorting_in_progress && viewer_image != NULL && viewer_is_valid )
			update_viewer( first_image() );
		break;

	case GDK_End:
		if ( !sorting_in_progress && viewer_image != NULL && viewer_is_valid )
			update_viewer( last_image() );
		break;

	case GDK_Return:
		if ( !sorting_in_progress && viewer_image != NULL )
		{
			if ( settings->db.random_start )
			{
				if ( viewer_is_valid )
					update_viewer( random_image() );
			}
			else
			{
				set_main_window_cursor_default();
				set_list_page();
			}
		}
		break;

	default:
		return FALSE;
	}

	return TRUE;
}

/*-----------------------------------------------------------------------------------------------*/

struct file *get_viewer_image( void )
{
	return (struct file *)viewer_image;
}

void viewer_image_removed( const struct file *image )
{
	if ( image == viewer_image )
	{
		image = next_image();
		if ( image == viewer_image ) image = NULL;
		update_viewer( image );
	}
}

/*-----------------------------------------------------------------------------------------------*/

static gboolean button_press_event( GtkWidget *widget, GdkEventButton *event, gpointer user_data )
{
	TRACE(( "# button_press_event( event->type = %u, event->button = %u )\n", event->type, event->button ));

	if ( event->button == 1 && viewer_is_valid )
	{
		if ( event->type == GDK_BUTTON_PRESS && viewer_timer == NULL )
		{
			if ( settings->view.scroll_zone && !sorting_in_progress && viewer_image != NULL )
			{
				gint x, y, dx, dy;

				/* Get current mouse position */
				gtk_widget_get_pointer( widget, &x, &y );
				y = (widget->allocation.height - 1) - y;

				/* Calculate distance from the middle of the widget */
				dx = abs( widget->allocation.width / 2 - x );
				dy = abs( widget->allocation.height / 2 - y );

				TRACE(( "button_press_event(): x = %d, y = %d, dx = %d, dy = %d\n", x, y, dx, dy ));

				if ( dx >= widget->allocation.width / 4 || dy >= widget->allocation.height / 4 )
				{
					const struct file *image =
						( x * widget->allocation.height < y * widget->allocation.width )
						? previous_image() : next_image();

					update_viewer( image );
					return TRUE;
				}
			}

			if ( viewer_error != NULL )
			{
				handle_error( g_error_copy( viewer_error ), "Error while loading image %s", viewer_image->name );
			}
		}
#if 0  // TODO: Stattdessen über Menüeintrag starten/stoppen
		else if ( event->type == GDK_2BUTTON_PRESS )
		{
			if ( viewer_timer == NULL )
			{
				start_slide_show();
			}
			else
			{
				stop_slide_show( TRUE );
			}
		}
#endif
	}

	return FALSE;
}

/*-----------------------------------------------------------------------------------------------*/

struct slide_show_data
{
	const struct file *image;
	GdkPixbuf *pixbuf;
	GError *error;
};

static void free_slide_show_data( struct slide_show_data *data )
{
	if ( data->error != NULL ) g_error_free( data->error );
	if ( data->pixbuf != NULL ) g_object_unref( data->pixbuf );

	g_free( data );
}

static void slide_show_next( void );

static gboolean slide_show_idle( gpointer user_data )
{
	if ( viewer_timer != NULL ) slide_show_next();
	return FALSE;
}

static gboolean slide_show_timeout( gpointer user_data )
{
	struct slide_show_data *data = (struct slide_show_data *)user_data;

	gdk_threads_enter();

	if ( viewer_timer != NULL )
	{
		g_timer_reset( viewer_timer );

		set_viewer_image( data->image );
		set_viewer_pixbuf( data->pixbuf );
		viewer_error = data->error;

		g_free( data );
	}
	else
	{
		free_slide_show_data( data );
	}

	gdk_threads_leave();

	g_idle_add( slide_show_idle, NULL );
	return FALSE;
}

static void slide_show_next( void )
{
	struct slide_show_data *data = g_new0( struct slide_show_data, 1 );

	data->image = next_image();
	if ( data->image != NULL )
	{
		data->pixbuf = new_pixbuf_from_image( data->image, &data->error );

		if ( viewer_timer != NULL )
		{
			gdouble interval = viewer_pause - g_timer_elapsed( viewer_timer, NULL );
			if ( interval < 0.0 ) interval = 0.0;
			g_timeout_add( (guint)(interval * 1000), slide_show_timeout, data );
			return;
		}
	}

	free_slide_show_data( data );
}

void start_slide_show( void )
{
	GtkDialog *dialog = new_modal_dialog_with_ok_cancel( "Start Slide Show", NULL );
	GtkTable *table = new_table( 1, 2 );
	GtkWidget *seconds;

	attach_to_table( table, new_label( "Pause between slides:" ), 0, 0, 1 );

	seconds = gtk_combo_box_new_text();
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "0.5s" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "1s" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "2s" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "3s" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "4s" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "5s" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "7.5s" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "10s" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "15s" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "20s" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "25s" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "30s" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "45s" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(seconds), "60s" );
	gtk_combo_box_set_active( GTK_COMBO_BOX(seconds), settings->view.pause_between_slides );
	attach_to_table( table, seconds, 0, 1, 2 );

	gtk_box_pack_start_defaults( GTK_BOX(dialog->vbox), GTK_WIDGET(table) );
	gtk_widget_show_all( GTK_WIDGET(dialog) );
	if ( gtk_dialog_run( dialog ) == GTK_RESPONSE_OK )
	{
		gint pause_between_slides = gtk_combo_box_get_active( GTK_COMBO_BOX(seconds) );

		gchar *text = gtk_combo_box_get_active_text( GTK_COMBO_BOX(seconds) );
		viewer_pause = g_ascii_strtod( text, NULL );
		g_free( text );

		if ( settings->view.pause_between_slides != pause_between_slides )
		{
			settings->view.pause_between_slides = pause_between_slides;
			save_settings();
		}

		viewer_timer = g_timer_new();
		slide_show_next();
	}

	gtk_widget_destroy( GTK_WIDGET(dialog) );
}

gboolean stop_slide_show( gboolean ask )
{
	if ( viewer_timer != NULL )
	{
		gint response;

		g_timer_stop( viewer_timer );

		response = (ask)
			? message_dialog( GTK_MESSAGE_QUESTION, "Stop Slide Show?" )
			: GTK_RESPONSE_YES;

		if ( response == GTK_RESPONSE_OK || response == GTK_RESPONSE_YES )
		{
			g_timer_destroy( viewer_timer );
			viewer_timer = NULL;
		}
		else
		{
			g_timer_continue( viewer_timer );
		}

		return TRUE;
	}

	return FALSE;
}

/*-----------------------------------------------------------------------------------------------*/

struct cache_entry
{
	/* const struct file *image; */   /* This one would not survive sorting... */
	const char *name;                 /* ...but this one does */
	GdkPixbuf *pixbuf;
};

static GList *cache;

static void free_cache_entry( GList *l )
{
	struct cache_entry *data = (struct cache_entry *)l->data;
	TRACE(( "free_cache_entry( \"%s\", %p )\n", data->name, (void *)data->pixbuf ));
	g_object_unref( data->pixbuf );
	g_free( data );
}

GdkPixbuf *take_from_viewer_cache( const struct file *image )
{
	int count = 0;
	GList *l;

	for ( l = g_list_first( cache ); l != NULL; l = g_list_next( l ) )
	{
		struct cache_entry *data = (struct cache_entry *)l->data;
		if ( data->name == image->name )
		{
			GdkPixbuf *pixbuf = data->pixbuf;
			TRACE(( "take_from_viewer_cache( \"%s\" ) = %p\n", data->name, (void *)pixbuf ));

			g_free( data );
			cache = g_list_delete_link( cache, l );

			return pixbuf;
		}

		count++;
	}

	for ( ; count > 0 && count >= settings->view.cache_size; count-- )
	{
		l = g_list_first( cache );
		free_cache_entry( l );
		cache = g_list_delete_link( cache, l );
	}

	TRACE(( "take_from_viewer_cache( \"%s\" ) = <NULL>\n", image->name ));
	return NULL;
}

void add_to_viewer_cache( const struct file *image, GdkPixbuf *pixbuf )
{
	if ( settings->view.cache_size > 0 )
	{
		struct cache_entry *data = g_new( struct cache_entry, 1 );

		TRACE(( "add_to_viewer_cache( \"%s\", %p )\n", image->name, (void *)pixbuf ));

		data->name = image->name;
		data->pixbuf = g_object_ref( pixbuf );

		cache = g_list_append( cache, data );
	}
}

void clear_viewer_cache( void )
{
	GList *l;

	TRACE(( "clear_viewer_cache()\n" ));

	for ( l = g_list_first( cache ); l != NULL; l = g_list_next( l ) )
	{
		free_cache_entry( l );
	}

	g_list_free( cache );
	cache = NULL;
}

/*-----------------------------------------------------------------------------------------------*/
