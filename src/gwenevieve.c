/*
	gwenevieve.c
	Copyright (C) 2010-2013 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)

	--------------------------------------------------------------------

	This file is part of the Gwenevieve application.

	Gwenevieve is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Gwenevieve is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gwenevieve.h"

#ifdef MAEMO
#define ICONS "/usr/share/icons/hicolor"
#else
#include "../icons/16x16/gwenevieve.h"
#include "../icons/26x26/gwenevieve.h"
#include "../icons/32x32/gwenevieve.h"
#include "../icons/40x40/gwenevieve.h"
#include "../icons/48x48/gwenevieve.h"
#include "../icons/64x64/gwenevieve.h"
#endif

#define SLIDER_WIDTH           "65"  /* 3 * 21 + (3 - 1) */
#define MIN_SLIDER_LENGTH      "65"

/* #define CLEAR_CACHE_WHEN_SORTING */

/*-----------------------------------------------------------------------------------------------*/

GtkWindow *MainWindow;
GtkStatusbar *Statusbar;
guint StatusbarContext;
GtkMenuItem *StatusMenuItem;

GtkNotebook *notebook;
gint selection_page, viewer_page, list_page;
gboolean sorting_in_progress;

const char go_back[]      = "- Go Back -";
const char all_pictures[] = "- All Pictures -";

static GtkWidget *file_info_item, *file_delete_item;

/*-----------------------------------------------------------------------------------------------*/

void rebuild_database( GError **error );

void file_info( GtkMenuItem *menuitem, gpointer user_data );
void file_delete( GtkMenuItem *menuitem, gpointer user_data );
void sort_toggled( GtkMenuItem *menu_item, gpointer user_data );
void tools_about( GtkMenuItem *menuitem, gpointer user_data );
void tools_options( GtkMenuItem *menuitem, gpointer user_data );
void tools_rebuild_database( GtkMenuItem *menuitem, gpointer user_data );

/*#define TEST_MENU*/
#ifdef TEST_MENU
void test_load_images1( GtkMenuItem *menuitem, gpointer user_data );
void test_load_images2( GtkMenuItem *menuitem, gpointer user_data );
void test_show_images( GtkMenuItem *menuitem, gpointer user_data );
#endif

gboolean key_press_event( GtkWidget *widget, GdkEventKey *event );
gboolean key_release_event( GtkWidget *widget, GdkEventKey *event );

void page_switched( GtkNotebook *notebook, GtkNotebookPage *page, gint page_num /*, gpointer user_data*/ );

/*-----------------------------------------------------------------------------------------------*/

gboolean open_database_idle( gpointer user_data )
{
	struct animation *ani = (struct animation *)user_data;

	gdk_threads_enter();

	clear_animation( ani );

	reset_selection();
	clear_viewer();
	clear_list();

	set_selection_page();
	/*if ( get_first_directory() == NULL ) options( NULL, NULL );*/

	gdk_threads_leave();
	return FALSE;
}

gpointer open_database_thread( gpointer user_data )
{
	struct animation *ani = (struct animation *)user_data;
	GError *error = NULL;

	open_database( &error );
	if ( error == NULL && settings->db.location == NO_DB ) rebuild_database( &error );
	handle_error( error, "Error while opening the database" );

	cancel_animation( ani );
	g_idle_add( open_database_idle, user_data );
	return NULL;
}

GThread *create_open_database_thread( void )
{
	struct animation *ani = show_animation( (settings->db.location == NO_DB) ? "Scanning folders" : "Opening database", NULL, 1000 );
	return g_thread_create( open_database_thread, ani, FALSE, NULL );
}

struct close_database_data
{
	enum close_database_action { ACTION_QUIT, ACTION_RELOAD, ACTION_REBUILD } action_after_closing;
	struct animation *ani;
};

gboolean close_database_idle( gpointer user_data )
{
	struct close_database_data *data = (struct close_database_data *)user_data;

	gdk_threads_enter();

	clear_animation( data->ani );

	switch ( data->action_after_closing )
	{
	case ACTION_QUIT:
		gtk_main_quit();
		break;
	case ACTION_RELOAD:
		create_open_database_thread();
		break;
	case ACTION_REBUILD:
		tools_rebuild_database( NULL, NULL );
		break;
	}

	gdk_threads_leave();

	g_free( data );
	return FALSE;
}

gpointer close_database_thread( gpointer user_data )
{
	struct close_database_data *data = (struct close_database_data *)user_data;
	GError *error = NULL;

	close_database( &error );
	handle_error( error, "Error while closing the database" );

	cancel_animation( data->ani );
	g_idle_add( close_database_idle, user_data );
	return NULL;
}

GThread *create_close_database_thread( enum close_database_action action_after_closing )
{
	struct close_database_data *data;

	clear_viewer_cache();

	data = g_new( struct close_database_data, 1 );
	data->action_after_closing = action_after_closing;
	data->ani = show_animation( "Closing database", NULL, 1000 );
	return g_thread_create( close_database_thread, data, FALSE, NULL );
}

/*-----------------------------------------------------------------------------------------------*/

GtkMenuShell *create_menu( void )
{
	enum database_sorting sorting = get_database_sorting();

	GtkMenuShell *menu = GTK_MENU_SHELL(gtk_menu_new());
	GtkMenuShell *sub_menu;
	GtkWidget *item;
	GSList *group;

	TRACE(( "-> create_menu()\n" ));

	/* File menu */

	item = gtk_menu_item_new_with_label( "File" );
	sub_menu = GTK_MENU_SHELL(gtk_menu_new());
	gtk_menu_item_set_submenu( GTK_MENU_ITEM(item), GTK_WIDGET(sub_menu) );
	gtk_menu_shell_append( menu, item );

	item = gtk_menu_item_new_with_label( "Info" );
	g_signal_connect( G_OBJECT(item), "activate", G_CALLBACK(file_info), NULL );
	gtk_menu_shell_append( sub_menu, item );
	file_info_item = item;

	item = gtk_menu_item_new_with_label( "Delete" );
	g_signal_connect( G_OBJECT(item), "activate", G_CALLBACK(file_delete), NULL );
	gtk_menu_shell_append( sub_menu, item );
	file_delete_item = item;

	/* Sort menu */

	item = gtk_menu_item_new_with_label( "Sort" );
	sub_menu = GTK_MENU_SHELL(gtk_menu_new());
	gtk_menu_item_set_submenu( GTK_MENU_ITEM(item), GTK_WIDGET(sub_menu) );
	gtk_menu_shell_append( menu, item );

	item = gtk_radio_menu_item_new_with_label( NULL, "Name" );
	group = gtk_radio_menu_item_get_group( GTK_RADIO_MENU_ITEM(item) );
	gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM(item), sorting == SORT_BY_NAME );
	g_signal_connect( G_OBJECT(item), "toggled", G_CALLBACK(sort_toggled), GINT_TO_POINTER(SORT_BY_NAME) );
	gtk_menu_shell_append( sub_menu, item );

	item = gtk_radio_menu_item_new_with_label( group, "Size" );
	group = gtk_radio_menu_item_get_group( GTK_RADIO_MENU_ITEM(item) );
	gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM(item), sorting == SORT_BY_SIZE );
	g_signal_connect( G_OBJECT(item), "toggled", G_CALLBACK(sort_toggled), GINT_TO_POINTER(SORT_BY_SIZE) );
	gtk_menu_shell_append( sub_menu, item );

	item = gtk_radio_menu_item_new_with_label( group, "Date" );
	group = gtk_radio_menu_item_get_group( GTK_RADIO_MENU_ITEM(item) );
	gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM(item), sorting == SORT_BY_DATE );
	g_signal_connect( G_OBJECT(item), "toggled", G_CALLBACK(sort_toggled), GINT_TO_POINTER(SORT_BY_DATE) );
	gtk_menu_shell_append( sub_menu, item );

	gtk_menu_shell_append( sub_menu, gtk_separator_menu_item_new() );

	item = gtk_radio_menu_item_new_with_label( group, "Shuffle" );
	group = gtk_radio_menu_item_get_group( GTK_RADIO_MENU_ITEM(item) );
	gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM(item), sorting == SHUFFLE );
	g_signal_connect( G_OBJECT(item), "toggled", G_CALLBACK(sort_toggled), GINT_TO_POINTER(SHUFFLE) );
	gtk_menu_shell_append( sub_menu, item );

	/* Tools menu */

	item = gtk_menu_item_new_with_label( "Tools" );
	sub_menu = GTK_MENU_SHELL(gtk_menu_new());
	gtk_menu_item_set_submenu( GTK_MENU_ITEM(item), GTK_WIDGET(sub_menu) );
	gtk_menu_shell_append( menu, item );

	item = gtk_menu_item_new_with_label( "Rebuild database" );
	g_signal_connect( G_OBJECT(item), "activate", G_CALLBACK(tools_rebuild_database), NULL );
	gtk_menu_shell_append( sub_menu, item );

	item = gtk_menu_item_new_with_label( "Options..." );
	g_signal_connect( G_OBJECT(item), "activate", G_CALLBACK(tools_options), NULL );
	gtk_menu_shell_append( sub_menu, item );

	gtk_menu_shell_append( sub_menu, gtk_separator_menu_item_new() );

	item = gtk_menu_item_new_with_label( "About..." );
	g_signal_connect( G_OBJECT(item), "activate", G_CALLBACK(tools_about), NULL );
	gtk_menu_shell_append( sub_menu, item );

#ifdef TEST_MENU

	/* Test menu */

	item = gtk_menu_item_new_with_label( "Test" );
	sub_menu = GTK_MENU_SHELL(gtk_menu_new());
	gtk_menu_item_set_submenu( GTK_MENU_ITEM(item), GTK_WIDGET(sub_menu) );
	gtk_menu_shell_append( menu, item );

	item = gtk_menu_item_new_with_label( "Load 100 pictures (1)" );
	g_signal_connect( G_OBJECT(item), "activate", G_CALLBACK(test_load_images1), NULL );
	gtk_menu_shell_append( sub_menu, item );

	item = gtk_menu_item_new_with_label( "Load 100 pictures (2)" );
	g_signal_connect( G_OBJECT(item), "activate", G_CALLBACK(test_load_images2), NULL );
	gtk_menu_shell_append( sub_menu, item );

	item = gtk_menu_item_new_with_label( "Show 100 pictures" );
	g_signal_connect( G_OBJECT(item), "activate", G_CALLBACK(test_show_images), NULL );
	gtk_menu_shell_append( sub_menu, item );

#endif

	/* Quit menu entry */

	item = gtk_menu_item_new_with_label( "Quit" );
	g_signal_connect( G_OBJECT(item), "activate", G_CALLBACK(quit), NULL );
	gtk_menu_shell_append( menu, item );

	TRACE(( "<- create_menu()\n" ));
	return menu;
}

#ifdef WIN32
#define STRICT
#define WIN32_LEAN_AND_MEAN  /* Exclude rarely-used stuff from Windows headers */
#include <windows.h>
int APIENTRY WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
#define argc __argc
#define argv __argv
#else
int main( int argc, char *argv[] )
#endif
{
	int exit_code = 0;
	gboolean ok;

	/* Initialize the GTK. */
	g_thread_init( NULL );
	gdk_threads_init();
	gtk_init( &argc, &argv );

	debug_init( "gwenevieve", G_LOG_DOMAIN, NULL );
	TRACE(( NAME " " VERSION " (" __DATE__ " " __TIME__ ")\n" ));

	hildon_gtk_initialize( NAME, VERSION,
	#ifndef MAEMO
		gwenevieve_icon_16x16, gwenevieve_icon_26x26, gwenevieve_icon_32x32,
		gwenevieve_icon_40x40, gwenevieve_icon_48x48, gwenevieve_icon_64x64,
	#endif
		NULL );

	MainWindow = new_window();
	set_main_window( MainWindow );

	ok = load_settings();
	if ( needs_fullscreen( settings ) ) set_fullscreen( TRUE );
	set_database_sorting( settings->db.sorting );

	gtk_rc_parse_string(
		"style \"gwenevieve-scrollbar\"\n"
		"{\n"
		"  GtkRange::slider-width = "SLIDER_WIDTH"\n"
		"  GtkRange::trough-border = 0\n"
		"  GtkRange::trough-side-details = 0\n"
		"  GtkScrollbar::has-backward-stepper = 0\n"
		"  GtkScrollbar::has-forward-stepper = 0\n"
		"  GtkScrollbar::has-secondary-backward-stepper = 0\n"
		"  GtkScrollbar::has-secondary-forward-stepper = 0\n"
		"  GtkScrollbar::min-slider-length = "MIN_SLIDER_LENGTH"\n"
		"}\n"
		/*"class \"GtkScrollbar\" style \"gwenevieve-scrollbar\"\n" );*/
		"widget \"*.GwenevieveScrollbar\" style \"gwenevieve-scrollbar\"\n" );

	/* Create menu */
	set_main_window_menu( create_menu(), TRUE );

	/* Create notebook */
	notebook = GTK_NOTEBOOK(gtk_notebook_new());
	gtk_notebook_set_show_tabs( notebook, FALSE );
	gtk_notebook_set_show_border( notebook, FALSE );
/*#ifndef MAEMO*/
	gtk_widget_set_size_request( GTK_WIDGET(notebook), settings->view.width, settings->view.height );
/*#endif*/
	selection_page = gtk_notebook_append_page( notebook, create_selection(), NULL );
	viewer_page = gtk_notebook_append_page( notebook, create_viewer(), NULL );
	list_page = gtk_notebook_append_page( notebook, create_list(), NULL );
	g_signal_connect( G_OBJECT(notebook), "switch-page", G_CALLBACK(page_switched), NULL );

	set_main_window_widget( GTK_WIDGET(notebook) );

	/* Connect signal to X in the upper corner */
	g_signal_connect( G_OBJECT(MainWindow), "delete-event", G_CALLBACK(quit), NULL );

	/* Connect hardware button listener */
	g_signal_connect( G_OBJECT(MainWindow), "key-press-event", G_CALLBACK(key_press_event), NULL );
	g_signal_connect( G_OBJECT(MainWindow), "key-release-event", G_CALLBACK(key_release_event), NULL );

	/* Begin the main application */
	TRACE(( "gtk_widget_show_all()...\n" ));
	gtk_widget_show_all( GTK_WIDGET(MainWindow) );  /* => "switch-page" event */
	TRACE(( "gtk_widget_show_all()...ok\n" ));

	gdk_threads_enter();

	if ( !ok ) tools_options( NULL, &exit_code );

	if ( exit_code == 0 )
	{
		create_open_database_thread();
		gtk_main();
	}

	gdk_threads_leave();

	hildon_gtk_deinitialize();
	debug_exit( FALSE );

	return exit_code;
}

/*-----------------------------------------------------------------------------------------------*/

static struct file *get_image( void )
{
	gint page = gtk_notebook_get_current_page( notebook );
	struct file *image = NULL;

	if ( page == viewer_page )
		image = get_viewer_image();
	else if ( page == list_page )
		image = get_list_image();

	return image;
}

#define KILOBYTE_FACTOR 1024
#define MEGABYTE_FACTOR (1024 * 1024)
#define GIGABYTE_FACTOR (1024 * 1024 * 1024)

/**
 * g_format_size_for_display:
 * @size: a size in bytes.
 *
 * Formats a size (for example the size of a file) into a human readable string.
 * Sizes are rounded to the nearest size prefix (KB, MB, GB) and are displayed rounded to
 * the nearest  tenth. E.g. the file size 3292528 bytes will be converted into
 * the string "3.1 MB".
 *
 * The prefix units base is 1024 (i.e. 1 KB is 1024 bytes).
 *
 * Returns: a formatted string containing a human readable file size.
 *
 * Since: 2.16
 **/
static char *format_size_for_display( unsigned long size )
{
	if ( size < KILOBYTE_FACTOR )
	{
		return g_strdup_printf( ( size == 1 ) ? "%u byte" : "%u bytes", (guint)size );
	}
	else
	{
		gdouble displayed_size;

		if ( size < MEGABYTE_FACTOR )
		{
			displayed_size = (gdouble)size / KILOBYTE_FACTOR;
			return g_strdup_printf( "%.1f KB", displayed_size );
		}
		else if ( size < GIGABYTE_FACTOR )
		{
			displayed_size = (gdouble)size / MEGABYTE_FACTOR;
			return g_strdup_printf( "%.1f MB", displayed_size );
		}
		else
		{
			displayed_size = (gdouble)size / GIGABYTE_FACTOR;
			return g_strdup_printf( "%.1f GB", displayed_size );
		}
	}
}

void file_info( GtkMenuItem *menuitem, gpointer user_data )
{
	const struct file *image = get_image();

	TRACE(( "# file_info( %s )\n", ( image != NULL ) ? image->name : "<NULL>" ));

	if ( image != NULL )
	{
		GString *message = g_string_new( "" );
		gchar *name, *size;
		char date[64];

		name = get_file_name( selected_folder, image );
		size = format_size_for_display( image->size );
		strftime( date, sizeof( date ), "%c", localtime( &image->mtime ) );

		g_string_printf( message, "%s\n%s\n%s", name, size, date );

		g_free( size );
		g_free( name );

		if ( image == viewer_image )
			g_string_append_printf( message, "\n\n%dx%d Pixel", viewer_image_width, viewer_image_height );

		message_dialog( GTK_MESSAGE_INFO, message->str );
		g_string_free( message, TRUE );
	}
}

void file_delete( GtkMenuItem *menuitem, gpointer user_data )
{
	struct file *image = get_image();

	TRACE(( "# file_delete( %s )\n", ( image != NULL ) ? image->name : "<NULL>" ));

	if ( image != NULL )
	{
		gchar *message, *name;
		int result;

		name = get_file_name( selected_folder, image );
		message = g_strdup_printf( "Delete %s?", name );
		g_free( name );

		result = message_dialog( GTK_MESSAGE_QUESTION_WITH_CANCEL_AS_DEFAULT_BUTTON, message );
		g_free( message );

		if ( result == GTK_RESPONSE_OK || result == GTK_RESPONSE_YES )
		{
			gchar *filename = build_filename( image );
			result = g_remove( filename );

			if ( result == 0 )
			{
				viewer_image_removed( image );
				list_image_removed( image );
				remove_file( image );
			}
			else
			{
				message = g_strdup_printf( "Could not delete %s", filename );
				message_dialog( GTK_MESSAGE_ERROR, message );
				g_free( message );
			}

			g_free( filename );
		}
	}
}

/*-----------------------------------------------------------------------------------------------*/

struct sorting_data
{
	enum database_sorting sort_by;
	struct animation *ani;
};

static const char *sorting[] = { "Sorting by name", "Sorting by size", "Sorting by date", "Shuffling" };

gboolean sorting_idle( gpointer user_data )
{
	struct sorting_data *data = (struct sorting_data *)user_data;

	gdk_threads_enter();

	clear_animation( data->ani );
	g_free( data );

	update_list();

	gdk_threads_leave();
	return FALSE;
}

gpointer sorting_thread( gpointer user_data )
{
	struct sorting_data *data = (struct sorting_data *)user_data;
	gpointer id = NULL;
#if CLEAR_CACHE_WHEN_SORTING
	GdkPixbuf *pixbuf;
#endif

	sorting_in_progress = TRUE;

	id = get_file_id( viewer_image );
#if CLEAR_CACHE_WHEN_SORTING
	pixbuf = take_from_viewer_cache( viewer_image );
#endif

	viewer_image = NULL;
#if CLEAR_CACHE_WHEN_SORTING
	clear_viewer_cache();
#endif

	set_database_sorting( data->sort_by );

	viewer_image = get_file_by_id( id );
#if CLEAR_CACHE_WHEN_SORTING
	if ( pixbuf != NULL )
	{
		if ( viewer_image != NULL ) add_to_viewer_cache( viewer_image, pixbuf );
		g_object_unref( pixbuf );
	}
#endif

	sorting_in_progress = FALSE;

	cancel_animation( data->ani );
	g_idle_add( sorting_idle, data );
	return NULL;
}

void sort( enum database_sorting sort_by )
{
	if ( !sorting_in_progress && get_database_sorting() != sort_by )
	{
		struct sorting_data *data = g_new( struct sorting_data, 1 );

		TRACE(( "# sorting_toggled( %d )\n", sort_by ));

		data->sort_by = sort_by;
		data->ani = show_animation( sorting[sort_by], NULL, 1000 );

		g_thread_create( sorting_thread, data, FALSE, NULL );
	}
}

void sort_toggled( GtkMenuItem *menu_item, gpointer user_data )
{
	if ( menu_item != NULL && gtk_check_menu_item_get_active( GTK_CHECK_MENU_ITEM(menu_item) ) )
	{
		sort( GPOINTER_TO_INT( user_data ) );
	}
}

/*-----------------------------------------------------------------------------------------------*/

void tools_about( GtkMenuItem *menuitem, gpointer user_data )
{
#ifdef MAEMO
	GdkPixbuf *logo = gdk_pixbuf_new_from_file( ICONS"/scalable/hildon/gwenevieve.png", NULL );
#else
	GdkPixbuf *logo = gdk_pixbuf_new_from_inline( -1, gwenevieve_icon_64x64, FALSE, NULL );
#endif

	about( logo );

	g_object_unref( logo );
}

void tools_options( GtkMenuItem *menuitem, gpointer user_data )
{
	struct settings old_settings = *settings;
	old_settings.db.path = g_strdup( settings->db.path );

	if ( options() )
	{
		const struct file *image = get_viewer_image();
		gboolean clear_cache = FALSE;
		gboolean refresh_view = FALSE;

		if ( database_is_open() )
		{
			if ( settings->db.location != old_settings.db.location )
			{
				remove_database_file( old_settings.db.path, old_settings.db.location );
				/* will be rewritten when closing the database */
			}

			if ( strcmp( settings->db.path, old_settings.db.path ) != 0 )
			{
				if ( settings->db.location == DB_IN_USER_CONFIG_DIR )
				{
					tools_rebuild_database( NULL, NULL );
				}
				else
				{
					create_close_database_thread(
						( database_file_exists( settings->db.path, settings->db.location ) )
							? ACTION_RELOAD : ACTION_REBUILD );
				}

				/* No need to update image view */
				old_settings.view.auto_rotate = settings->view.auto_rotate;
				old_settings.view.scale_up    = settings->view.scale_up;
			}
			else
			{
				if ( settings->db.sorting != old_settings.db.sorting )
				{
					sort( settings->db.sorting );
				}

				if ( settings->db.linear_mode != old_settings.db.linear_mode )
				{
					reset_selection();
				}
				else if ( settings->db.offer_all != old_settings.db.offer_all )
				{
					update_selection();
				}
			}
		}

		if ( settings->view.width  != old_settings.view.width ||
			 settings->view.height != old_settings.view.height )
		{
			gtk_widget_set_size_request( GTK_WIDGET(notebook), settings->view.width, settings->view.height );
			gtk_window_unmaximize( MainWindow );
			gtk_window_resize( MainWindow, 1, 1 );

			if ( needs_fullscreen( &old_settings ) )
				set_fullscreen( FALSE );
			else if ( needs_fullscreen( settings ) )
				set_fullscreen( TRUE );

			clear_cache = TRUE;
			refresh_view = TRUE;
		}
		else if ( settings->view.auto_rotate != old_settings.view.auto_rotate )
		{
			clear_cache = TRUE;

			if ( viewer_image_height > viewer_image_width )
				refresh_view = TRUE;
		}
		else if ( settings->view.scale_up != old_settings.view.scale_up )
		{
			clear_cache = TRUE;

			if ( (viewer_image_width < settings->view.width  && viewer_image_height < settings->view.height) ||
				 (viewer_image_width < settings->view.height && viewer_image_height < settings->view.width) )
				refresh_view = TRUE;
		}

		if ( clear_cache )
		{
			clear_viewer_cache();

			if ( refresh_view && image != NULL )
			{
				invalidate_viewer();
				update_viewer( image );
			}
		}
	}
	else if ( user_data != NULL )
	{
		*(int*)user_data = 1;  /* Set exit code for main() */
	}

	g_free( old_settings.db.path );
}

static gboolean is_image_filename( const char *filename )
{
	const char *ext = strrchr( filename, '.' );
	if ( ext++ != NULL )
	{
		return stricmp( ext, "bmp" ) == 0 ||
		       stricmp( ext, "gif" ) == 0 ||
		       stricmp( ext, "jpg" ) == 0 || stricmp( ext, "jpeg" ) == 0 || stricmp( ext, "jpe" ) == 0 || stricmp( ext, "jfif" ) == 0 ||
		       stricmp( ext, "png" ) == 0 ||
		       stricmp( ext, "tif" ) == 0 || stricmp( ext, "tiff" ) == 0 ||
		       stricmp( ext, "webm" ) == 0;
	}

	return FALSE;
}

unsigned rebuild( struct database_contents *contents, const char *path, const char *folder, GError **error )
{
	const gchar *name;
	unsigned n = 0;
	GDir *dir;

	TRACE(( "rebuild( \"%s\", \"%s\" )...\n", path, folder ));

	dir = g_dir_open( path, 0, error );
	if ( dir != NULL )
	{
		while ( (name = g_dir_read_name( dir )) != NULL )
		{
			if ( name[0] != '.' )
			{
				gchar *filename = g_build_filename( path, name, NULL );
				GStatBuf s;

				if ( g_stat( filename, &s ) == 0 )
				{
					if ( (s.st_mode & S_IFDIR) != 0 )
					{
						gchar *new_folder = g_build_filename( folder, name, NULL );
						n += rebuild( contents, filename, new_folder, error );
						g_free( new_folder );
					}
					else if ( (s.st_mode & S_IFREG) != 0 && is_image_filename( name ) )
					{
						append_database_contents( contents, folder, name, s.st_size, s.st_mtime );
						n++;
					}
				}

				g_free( filename );
			}
		}

		g_dir_close( dir );
	}

	TRACE(( "rebuild( \"%s\", \"%s\" ) = %d\n", path, folder, n ));
	return n;
}

void rebuild_database( GError **error )
{
	struct database_contents *contents = new_database_contents();

	rebuild( contents, settings->db.path, "", error );
	if ( *error == NULL ) set_database_contents( contents, error );

	free_database_contents( contents );
}

gpointer rebuild_database_thread( gpointer user_data )
{
	struct animation *ani = (struct animation *)user_data;
	GError *error = NULL;

	rebuild_database( &error );
	handle_error( error, "Error while rebuilding the database" );

	cancel_animation( ani );
	g_idle_add( open_database_idle, user_data );
	return NULL;
}

void tools_rebuild_database( GtkMenuItem *menuitem, gpointer user_data )
{
	gint response = ( menuitem != NULL )
		? message_dialog( GTK_MESSAGE_QUESTION_WITH_CANCEL_AS_DEFAULT_BUTTON, "Rebuild database?" )
		: GTK_RESPONSE_YES;

	if ( response == GTK_RESPONSE_OK || response == GTK_RESPONSE_YES )
	{
		struct animation *ani = show_animation( "Rebuilding database", NULL, 1000 );

		clear_viewer_cache();
		discard_database();
		reset_selection();
		set_selection_page();

		g_thread_create( rebuild_database_thread, ani, FALSE, NULL );
	}
}

/*-----------------------------------------------------------------------------------------------*/

#ifdef TEST_MENU

struct load_images_data
{
	const struct file *image;
	GTimer *timer;
	gboolean show;
	int num;
	gdouble elapsed;
};

gboolean load_images_idle( gpointer user_data )
{
	struct load_images_data *data = (struct load_images_data*)user_data;
	GString *message;

	gdk_threads_enter();
	busy_leave();

	message = g_string_new( "" );
	g_string_printf( message, "Needed %f seconds for %d images", data->elapsed, data->num );
	message_dialog( GTK_MESSAGE_INFO, message->str );
	g_string_free( message, TRUE );

	gdk_threads_leave();

	g_free( data );
	return FALSE;
}

gpointer load_images_thread( gpointer user_data )
{
	struct load_images_data *data = g_new0( struct load_images_data, 1 );
	const struct file *image;

	data->timer = g_timer_new();
	for ( image = get_first_file( NULL, FALSE ); image != NULL; image = get_next_file( NULL, FALSE, image ) )
	{
		gchar *filename = build_filename( image );
		gchar *contents = NULL;
		g_file_get_contents( filename, &contents, NULL, NULL );
		g_free( contents );
		g_free( filename );

		if ( ++data->num == 100 ) break;  /* only up to 100 files */
	}

	data->elapsed = g_timer_elapsed( data->timer, NULL );
	g_timer_destroy( data->timer );

	g_idle_add( load_images_idle, data );
	return NULL;
}

void test_load_images1( GtkMenuItem *menuitem, gpointer user_data )
{
	gint response = message_dialog( GTK_MESSAGE_QUESTION_WITH_CANCEL_AS_DEFAULT_BUTTON, "Load 100 pictures? (1)" );

	if ( response == GTK_RESPONSE_OK || response == GTK_RESPONSE_YES )
	{
		busy_enter();
		g_thread_create( load_images_thread, NULL, FALSE, NULL );
	}
}

gboolean show_images_idle( gpointer user_data )
{
	struct load_images_data *data = (struct load_images_data*)user_data;
	GError *error = NULL;
	GdkPixbuf *pixbuf;

	gdk_threads_enter();

	pixbuf = new_pixbuf_from_image( data->image, &error );
	if ( error != NULL ) g_error_free( error );

	if ( data->show )
	{
		set_viewer_image( data->image );
		set_viewer_pixbuf( pixbuf );
	}
	else if ( pixbuf != NULL )
	{
		g_object_unref( pixbuf );
	}

	data->image = ( ++data->num == 100 ) ? NULL : get_next_file( NULL, FALSE, data->image );
	if ( data->image == NULL )
	{
		data->elapsed = g_timer_elapsed( data->timer, NULL );
		g_timer_destroy( data->timer );

		gdk_threads_leave();
		g_idle_add( load_images_idle, data );
		return FALSE;
	}

	gdk_threads_leave();
	return TRUE;
}

void test_load_images2( GtkMenuItem *menuitem, gpointer user_data )
{
	gint response = message_dialog( GTK_MESSAGE_QUESTION_WITH_CANCEL_AS_DEFAULT_BUTTON, "Load 100 pictures? (2)" );

	if ( response == GTK_RESPONSE_OK || response == GTK_RESPONSE_YES )
	{
		struct load_images_data *data = g_new0( struct load_images_data, 1 );

		busy_enter();

		data->image = get_first_file( NULL, FALSE );
		data->timer = g_timer_new();
		data->show = FALSE;
		g_idle_add( show_images_idle, data );
	}
}

void test_show_images( GtkMenuItem *menuitem, gpointer user_data )
{
	gint response = message_dialog( GTK_MESSAGE_QUESTION_WITH_CANCEL_AS_DEFAULT_BUTTON, "Show 100 pictures?" );

	if ( response == GTK_RESPONSE_OK || response == GTK_RESPONSE_YES )
	{
		struct load_images_data *data = g_new0( struct load_images_data, 1 );

		busy_enter();
		set_viewer_page();

		data->image = get_first_file( NULL, FALSE );
		data->timer = g_timer_new();
		data->show = TRUE;
		g_idle_add( show_images_idle, data );
	}
}

#endif

/*-----------------------------------------------------------------------------------------------*/

struct fullscreen_data
{
	gboolean fullscreen;
	const struct file *image;
};

static gboolean fullscreen_idle( gpointer user_data )
{
	struct fullscreen_data *data = (struct fullscreen_data *)user_data;

	if ( get_main_window_fullscreen() == data->fullscreen )
		return TRUE;

	update_viewer_pixbuf( (gpointer)data->image );
	set_viewer_cursor();
	busy_leave();

	g_free( data );
	return FALSE;
}

gboolean key_press_event( GtkWidget *widget, GdkEventKey *event )
{
	gint page = gtk_notebook_get_current_page( notebook );
	gboolean handled = FALSE;

	TRACE(( "# key_press_event()\n" ));

	if ( page == selection_page )
	{
		handled = selection_key_press_event( widget, event );
	}
	else if ( page == viewer_page )
	{
		handled = viewer_key_press_event( widget, event );
	}
	else if ( page == list_page )
	{
		handled = list_key_press_event( widget, event );
	}

	if ( !handled )
	{
		struct fullscreen_data *data;

		switch ( event->keyval )
		{
		case GDK_Alt_L:  /* Hardkey on SmartQ devices */
			if ( !isSmartQ() ) return FALSE;
			/* pass through */
		case GDK_F6:     /* Hardkey on Nokia IT devices */
		case GDK_F11:
			data = g_new( struct fullscreen_data, 1 );
			data->fullscreen = get_main_window_fullscreen();
			data->image = viewer_image;

			busy_enter();
			clear_viewer_cache();

			if ( data->fullscreen )
			{
				clear_viewer();
				TRACE(( "gtk_window_unfullscreen()\n" ));
				gtk_window_unfullscreen( MainWindow );
			}
			else
			{
				invalidate_viewer();
				TRACE(( "gtk_window_fullscreen()\n" ));
				gtk_window_fullscreen( MainWindow );
			}

			if ( page != selection_page )
			{
				g_idle_add_full( G_PRIORITY_LOW, fullscreen_idle, data, NULL );
				return TRUE;
			}

			busy_leave();

			g_free( data );
			break;

		default:
			return FALSE;
		}
	}

	return TRUE;
}

gboolean key_release_event( GtkWidget *widget, GdkEventKey *event )
{
	TRACE(( "# key_release_event()\n" ));
	return FALSE;
}

/*-----------------------------------------------------------------------------------------------*/

void page_switched( GtkNotebook *notebook, GtkNotebookPage *page, gint page_num /*, gpointer user_data*/ )
{
	gboolean sensitive = ( page_num != selection_page );

	gtk_widget_set_sensitive( file_info_item, sensitive );
	gtk_widget_set_sensitive( file_delete_item, sensitive );
}

/*-----------------------------------------------------------------------------------------------*/

static int handle_error_flag;

static gboolean handle_error_idle( gpointer user_data )
{
	gchar *error_message = (gchar *)user_data;

	gdk_threads_enter();
	message_dialog( GTK_MESSAGE_ERROR, error_message );
	gdk_threads_leave();

	g_atomic_int_set( &handle_error_flag, FALSE );

	g_free( error_message );
	return FALSE;
}

void handle_error( GError *error, const gchar *format, ... )
{
	ASSERT( format != NULL );
	if ( format == NULL ) return;

	if ( error != NULL )
	{
		gchar *message;
		va_list args;

		va_start( args, format );     /* Initialize variable arguments. */
		message = g_strdup_vprintf( format, args );
		va_end( args );               /* Reset variable arguments.      */

		TRACE(( "*** message = %s, error = %d:%s\n", message, error->code, (error->message != NULL) ? error->message : "<NULL>" ));
		TRACE(( "***\n" ));

		if ( g_atomic_int_compare_and_exchange( &handle_error_flag, FALSE, TRUE ) )
		{
			GString *error_message = g_string_new( message );

			if ( error->message != NULL )
			{
				g_string_append( error_message, ":\n\n" );
				g_string_append( error_message, error->message );

				if ( strstr( error->message, "\n\n" ) == NULL )
					g_string_append( error_message, "\n" );

			}
			else
			{
				g_string_append( error_message, ".\n" );
			}

			if ( error->code != 0 )
			{
				g_string_append_printf( error_message, "\n%s = %d", "Error code", error->code );
			}

			/*TRACE(( "*** error_message = %s\n", error_message->str ));*/
			g_idle_add_full( G_PRIORITY_LOW, handle_error_idle, g_string_free( error_message, FALSE ), NULL );
		}

		g_free( message );
		g_error_free( error );
	}
}

gboolean quit( void )
{
	stop_slide_show( FALSE );
	create_close_database_thread( ACTION_QUIT );
	return FALSE;
}

/*-----------------------------------------------------------------------------------------------*/

gboolean needs_fullscreen( struct settings *settings )
{
	return settings->view.width == settings->screen.width || settings->view.height == settings->screen.height;
}

void set_fullscreen( gboolean fullscreen )
{
	if ( fullscreen != get_main_window_fullscreen() )
	{
		set_main_window_fullscreen( fullscreen );
		clear_viewer_cache();
	}
}

/*-----------------------------------------------------------------------------------------------*/

