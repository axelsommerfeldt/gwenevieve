/*
	selection.c
	Copyright (C) 2010-2013 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)

	--------------------------------------------------------------------

	This file is part of the Gwenevieve application.

	Gwenevieve is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Gwenevieve is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gwenevieve.h"

#define BUTTON_HEIGHT  60
#define DIR_SEPARATOR  " / "

/*-----------------------------------------------------------------------------------------------*/

const struct folder *node;
const struct folder *selected_folder;
gboolean selected_subfolder;

GtkScrolledWindow *selection_scrolled_window;
GtkViewport *selection_viewport;
GtkBox *selection_vbox;

GString *selection_vadjustment, *selection_position;
GtkWidget *selection_focus_widget;

void set_selection_title( void );
void selection_clicked( GtkButton *button, gpointer user_data );

/*-----------------------------------------------------------------------------------------------*/

static void ensure_visible( GtkWidget *widget, gint height )
{
	GtkAdjustment *adjustment = gtk_scrolled_window_get_vadjustment( selection_scrolled_window );
	gint value = (gint)gtk_adjustment_get_value( adjustment );

	gint y = widget->allocation.y - (height - widget->allocation.height);
	gint viewport_height = GTK_WIDGET(selection_viewport)->allocation.height;

	TRACE(( "ensure_visible(): y = %d, value = %d\n", y, value ));
	TRACE(( "ensure_visible(): y + height = %d, value + viewport_height = %d\n", y + height, value + viewport_height ));

	if ( y < value )
	{
		value = y;

		TRACE(( "ensure_visible(): gtk_adjustment_set_value( %d )\n", value ));
		gtk_adjustment_set_value( adjustment, value );
	}
	else if ( y + height > value + viewport_height )
	{
		value = (y + height) - viewport_height;

		TRACE(( "ensure_visible(): gtk_adjustment_set_value( %d )\n", value ));
		gtk_adjustment_set_value( adjustment, value );
	}
}

static void grab_focus( GList *(*list_next)( GList *l ) )
{
	GList *children = gtk_container_get_children( GTK_CONTAINER( selection_vbox ) );
	GList *l;

	for ( l = children; l != NULL; l = l->next )
	{
		if ( GTK_WIDGET_HAS_FOCUS( l->data ) )
		{
			GtkWidget *widget;
			gint height = 0;

			for (;;)
			{
				l = list_next( l );
				if ( l == NULL ) break;

				widget = GTK_WIDGET( l->data );
				height += widget->allocation.height;
				if ( GTK_WIDGET_CAN_FOCUS( widget ) )
				{
					ensure_visible( widget, height );
					gtk_widget_grab_focus( widget );
					break;
				}
			}
			break;
		}
	}

	g_list_free( children );
}

static GList *list_prev( GList *l )
{
	return l->prev;
}

static GList *list_next( GList *l )
{
	return l->next;
}

/*-----------------------------------------------------------------------------------------------*/

void append_to_path( gdouble vadjustment, int position )
{
	char dtostr_buf[1+G_ASCII_DTOSTR_BUF_SIZE];

	dtostr_buf[0] = ':';
	g_ascii_dtostr( dtostr_buf + 1, G_ASCII_DTOSTR_BUF_SIZE, vadjustment );
	g_string_append( selection_vadjustment, dtostr_buf );
	sprintf( dtostr_buf + 1, "%d", position );
	g_string_append( selection_position, dtostr_buf );
}

void get_from_path( gdouble *vadjustment, int *position )
{
	if ( vadjustment != NULL )
	{
		const char *s = strrchr( selection_vadjustment->str, ':' );
		ASSERT( s != NULL );
		*vadjustment = g_ascii_strtod( s + 1, NULL );
	}
	if ( position != NULL )
	{
		const char *s = strrchr( selection_position->str, ':' );
		ASSERT( s != NULL );
		*position = atoi( s + 1 );
	}
}

void go_back_in_path( void )
{
	char *s;

	s = strrchr( selection_vadjustment->str, ':' );
	ASSERT( s != NULL );
	g_string_truncate( selection_vadjustment, s - selection_vadjustment->str );
	s = strrchr( selection_position->str, ':' );
	ASSERT( s != NULL );
	g_string_truncate( selection_position, s - selection_position->str );
}

void replace_path( gdouble vadjustment, int position )
{
	go_back_in_path();
	append_to_path( vadjustment, position );
}

/*-----------------------------------------------------------------------------------------------*/

GtkWidget *create_selection( void )
{
	GtkWidget *event_box;

	gtk_rc_parse_string(
		"style \"gwenevieve-button\"\n"
		"{\n"
		"  GtkButton::image-spacing = 16\n"
		"  GtkButton::inner-border = { 24, 0, 0, 0 }\n"
		"}\n"
		"widget \"*.GwenevieveButton\" style \"gwenevieve-button\"\n" );

	selection_vbox = GTK_BOX( gtk_vbox_new( FALSE, 0 ) );

	event_box = gtk_event_box_new();
	gtk_container_add( GTK_CONTAINER(event_box), GTK_WIDGET(selection_vbox) );

	selection_scrolled_window = new_scrolled_window( GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC );
	gtk_scrolled_window_set_shadow_type( selection_scrolled_window, GTK_SHADOW_NONE );
	gtk_widget_set_name( gtk_scrolled_window_get_vscrollbar( selection_scrolled_window ), "GwenevieveScrollbar" );

	/*gtk_scrolled_window_add_with_viewport( scrolled_window, GTK_WIDGET( event_box ) );*/
	selection_viewport = GTK_VIEWPORT( gtk_viewport_new(
		gtk_scrolled_window_get_hadjustment( selection_scrolled_window ),
		gtk_scrolled_window_get_vadjustment( selection_scrolled_window )) );
	gtk_viewport_set_shadow_type( selection_viewport, GTK_SHADOW_NONE );
	gtk_container_add( GTK_CONTAINER( selection_viewport ), GTK_WIDGET( event_box ) );
	gtk_container_add( GTK_CONTAINER( selection_scrolled_window ), GTK_WIDGET( selection_viewport ) );

	selection_vadjustment = g_string_new( "" );
	selection_position    = g_string_new( "" );
	append_to_path( 0.0, 0 );

	return GTK_WIDGET( selection_scrolled_window );
}

void set_selection_focus( void )
{
	if ( selection_focus_widget != NULL ) gtk_widget_grab_focus( GTK_WIDGET(selection_focus_widget) );
}

void set_selection_page( void )
{
	set_selection_title();
	gtk_notebook_set_current_page( notebook, selection_page );
	set_selection_focus();
}

void set_selection_title( void )
{
	set_main_window_title( ( node == NULL || node == get_root_folder() ) ? NULL : node->name );
}

static GtkWidget *add_selection_button( GList **l, const char *dirname, const struct folder *folder, const GtkRequisition *requisition )
{
	extern gboolean button_release_event( GtkWidget *widget, GdkEventButton *event, gpointer user_data );
	GtkWidget *widget, *image, *separator;
	const gchar *stock_id;

	if ( dirname == NULL || dirname[0] == '\0' ) dirname = ".";
	stock_id = (dirname == go_back) ? GTK_STOCK_GO_BACK : GTK_STOCK_DIRECTORY;

	if ( *l != NULL )
	{
		widget = GTK_WIDGET( (*l)->data );

		image = ( g_object_get_data( G_OBJECT(widget), "stock_id" ) == stock_id )
			? gtk_button_get_image( GTK_BUTTON(widget) )
			: gtk_image_new_from_stock( stock_id, GTK_ICON_SIZE_LARGE_TOOLBAR );
		g_object_ref( image );

		gtk_button_set_label( GTK_BUTTON(widget), dirname );  /* removes image from button */

		*l = (*l)->next;

		ASSERT( *l != NULL );
		separator = GTK_WIDGET( (*l)->data );

		*l = (*l)->next;
	}
	else
	{
		widget = gtk_button_new_with_label( dirname );

		image = gtk_image_new_from_stock( stock_id, GTK_ICON_SIZE_LARGE_TOOLBAR );
		g_object_ref( image );

		gtk_widget_set_name( widget, "GwenevieveButton" );
		gtk_widget_set_size_request( widget, -1, BUTTON_HEIGHT - requisition->height );
		gtk_button_set_alignment( GTK_BUTTON(widget), 0.0, 0.5 );
		gtk_button_set_relief( GTK_BUTTON(widget), GTK_RELIEF_NONE );

		gtk_box_pack_start( selection_vbox, widget, FALSE, FALSE, 0 );

		g_signal_connect( G_OBJECT(widget), "clicked", G_CALLBACK(selection_clicked), NULL /*(gpointer)folder*/ );
		g_signal_connect( G_OBJECT(widget), "button-release-event", G_CALLBACK(main_window_popup_menu), NULL );

		separator = gtk_hseparator_new();
		gtk_box_pack_start( selection_vbox, separator, FALSE, FALSE, 0 );
	}

	g_object_set_data( G_OBJECT(widget), "folder", (gpointer)folder );
	g_object_set_data( G_OBJECT(widget), "stock_id", (gpointer)stock_id );

	gtk_button_set_image( GTK_BUTTON(widget), image );
	/*gtk_widget_set_visible( image, TRUE );*/  /* Since GTK+ 2.18 */
	g_object_set( image, "visible", TRUE, NULL );
	g_object_unref( image );

	gtk_widget_show( widget );
	gtk_widget_show( separator );
	return widget;
}

void reset_selection( void )
{
	node = ( settings->db.linear_mode ) ? NULL : get_root_folder();

	g_string_truncate( selection_vadjustment, 0 );
	g_string_truncate( selection_position, 0 );
	append_to_path( 0.0, 0 );

	set_selection_title();
	update_selection();
}

static gboolean should_we_offer_all_pictures( const struct folder *node )
{
#if 0

	unsigned num_folder = get_num_folder( node, TRUE );
	unsigned num_files = (node != NULL) ? node->num_files : 0;

	TRACE(( "should_we_offer_all_pictures(): num_folder = %u, num_files = %u\n", num_folder, num_files ));
	return num_folder > 1 || (num_folder == 1 && num_files > 0);


#else

	unsigned num_folder = get_num_folder( node );
	unsigned num_files = (node != NULL) ? node->num_files : 0;

	if ( num_folder == 1 )
	{
		const struct folder *folder = get_first_folder( node );
		ASSERT( folder != NULL );
		ASSERT( get_next_folder( node, folder ) == NULL );
		num_folder += get_num_folder( folder );
	}

	TRACE(( "should_we_offer_all_pictures(): num_folder = %u, num_files = %u\n", num_folder, num_files ));
	return num_folder > 1 || (num_folder == 1 && num_files > 0);

#endif
}

gboolean update_selection_idle2( gpointer user_data )
{
	gdouble vadjustment;
	get_from_path( &vadjustment, NULL );

	gdk_threads_enter();

	gtk_adjustment_set_value( gtk_scrolled_window_get_vadjustment( selection_scrolled_window ), vadjustment );
	busy_leave();

	gdk_threads_leave();
	return FALSE;
}

gboolean update_selection_idle1( gpointer user_data )
{
	static GtkRequisition requisition;

	const struct folder *dir;
	GList *children, *l;
	GtkWidget *widget;
	int position;

	get_from_path( NULL, &position );
	selection_focus_widget = NULL;

	gdk_threads_enter();

	l = children = gtk_container_get_children( GTK_CONTAINER( selection_vbox ) );
	if ( l != NULL )
	{
		widget = GTK_WIDGET( l->data );
		l = l->next;
	}
	else
	{
		widget = gtk_hseparator_new();
		gtk_widget_size_request( widget, &requisition );
		gtk_box_pack_start( selection_vbox, widget, FALSE, FALSE, 0 );
	}

	if ( node != NULL && node->parent != NULL )
	{
		widget = add_selection_button( &l, go_back, node->parent, &requisition );
		if ( position-- == 0 ) selection_focus_widget = widget;
	}

	if ( settings->db.offer_all == 1 && should_we_offer_all_pictures( node ) )
	{
		widget = add_selection_button( &l, all_pictures, NULL, &requisition );
		if ( position-- == 0 ) selection_focus_widget = widget;
	}

	if ( node != NULL && node->num_files > 0 )
	{
		gchar *name = get_folder_name( node, node, NULL );
		widget = add_selection_button( &l, name, node, &requisition );
		g_free( name );
		if ( position-- == 0 ) selection_focus_widget = widget;
	}

	for ( dir = get_first_folder( node ); dir != NULL; dir = get_next_folder( node, dir ) )
	{
		gchar *name = get_folder_name( node, dir, DIR_SEPARATOR );
		widget = add_selection_button( &l, name, dir, &requisition );
		g_free( name );
		if ( position-- == 0 ) selection_focus_widget = widget;
	}

	if ( settings->db.offer_all == 2 && should_we_offer_all_pictures( node ) )
	{
		widget = add_selection_button( &l, all_pictures, NULL, &requisition );
		if ( position-- == 0 ) selection_focus_widget = widget;
	}

	for ( ; l != NULL; l = l->next )
	{
		/*gtk_container_remove( GTK_CONTAINER( selection_vbox ), GTK_WIDGET( l->data ) );*/
		gtk_widget_hide( GTK_WIDGET( l->data ) );
	}
	g_list_free( children );

	set_selection_focus();
	/*gtk_adjustment_set_value( gtk_scrolled_window_get_vadjustment( selection_scrolled_window ), vadjustment );*/

	gdk_threads_leave();
	g_idle_add( update_selection_idle2, user_data );
	return FALSE;
}

void update_selection( void )
{
	busy_enter();
	g_idle_add( update_selection_idle1, NULL );
}

gboolean selection_key_press_event( GtkWidget *widget, GdkEventKey *event )
{
	gint response;

	switch ( event->keyval )
	{
	case GDK_BackSpace:
	case GDK_Escape:
		if ( node == NULL || node->parent == NULL )
		{
			response = message_dialog( GTK_MESSAGE_QUESTION, "Quit Gwenevieve?" );
			if ( response == GTK_RESPONSE_OK || response == GTK_RESPONSE_YES )
			{
				quit();
			}
		}
		else
		{
			node = node->parent;
			go_back_in_path();
			set_selection_title();
			update_selection();
		}
		break;

	case GDK_Up:
	case GDK_Page_Up:
		grab_focus( list_prev );
		break;

	case GDK_Down:
	case GDK_Page_Down:
		grab_focus( list_next );
		break;

#if FALSE
	case GDK_Alt_L:  /* Hardkey on SmartQ devices */
		if ( IsSmartQ() )
		{
			event->keyval = GDK_Return;
			return FALSE;
		}
		break;
#endif

	default:
		return FALSE;
	}

	return TRUE;
}

/*-----------------------------------------------------------------------------------------------*/

static int get_widget_position( GtkWidget *widget )
{
	GList *children = gtk_container_get_children( GTK_CONTAINER( selection_vbox ) ), *l;
	int position = -1, i = 0;

	for ( l = children; l != NULL; l = l->next )
	{
		GtkWidget *w = GTK_WIDGET( l->data );

		if ( GTK_WIDGET_CAN_FOCUS( w ) )
		{
			if ( w == widget )
			{
				position = i;
				break;
			}

			i++;
		}
	}

	g_list_free( children );
	return position;
}

void selection_clicked( GtkButton *button, gpointer user_data )
{
	const struct folder *folder = (const struct folder *)user_data;

	folder = (const struct folder *)g_object_get_data( G_OBJECT(button), "folder" );

	TRACE(( "# selection_clicked( %s )\n", (folder != NULL) ? folder->name : "<NULL>" ));

	if ( !sorting_in_progress )
	{
		if ( folder == NULL )  /* All Pictures */
		{
			folder = node;
			selected_subfolder = TRUE;
		}
		else
		{
			selected_subfolder = FALSE;
		}

		if ( settings->db.linear_mode || node == folder || folder->children == NULL )
		{
			selection_focus_widget = GTK_WIDGET( button );

			clear_viewer();
			clear_list();

			selected_folder = folder;
			set_viewer_page();

			update_viewer( ((settings->db.random_start) ? get_random_file : get_first_file)( selected_folder, selected_subfolder ) );
		}
		else
		{
			if ( folder == node->parent )
			{
				go_back_in_path();
			}
			else
			{
				GtkAdjustment *adjustment = gtk_scrolled_window_get_vadjustment( selection_scrolled_window );
				replace_path( gtk_adjustment_get_value( adjustment ), get_widget_position( GTK_WIDGET( button ) ) );
				append_to_path( 0.0, 0 );
			}

			node = folder;
			set_selection_title();
			update_selection();
		}
	}
}

/*-----------------------------------------------------------------------------------------------*/
