/*
	list.c
	Copyright (C) 2010-2011 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)

	--------------------------------------------------------------------

	This file is part of the Gwenevieve application.

	Gwenevieve is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Gwenevieve is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gwenevieve.h"

#define COL_IMAGE_DATA  0
#define COL_FILENAME    1

/*-----------------------------------------------------------------------------------------------*/

GtkTreeView *list_tree_view;
gboolean list_is_valid;

gboolean fill_list_1( gpointer user_data );
gboolean fill_list_2( gpointer user_data );

void set_list_cursor( gint position );
void set_list_title( gint count );

struct file *get_list_image_from_iter( GtkTreeModel *model, GtkTreeIter *iter );
gint get_list_image_position( const struct file *image );

void list_row_activated( GtkTreeView *view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer user_data );

/*-----------------------------------------------------------------------------------------------*/

GtkWidget *create_list( void )
{
	GtkScrolledWindow *scrolled_window;
	GtkTreeSelection *selection;
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;
	GtkListStore *store;

	list_tree_view = GTK_TREE_VIEW( gtk_tree_view_new() );
	gtk_tree_view_set_fixed_height_mode( list_tree_view, TRUE );
	gtk_tree_view_set_headers_visible( list_tree_view, TRUE );

	selection = gtk_tree_view_get_selection( list_tree_view );
	gtk_tree_selection_set_mode( selection, GTK_SELECTION_BROWSE );

	column = gtk_tree_view_column_new();
	renderer = gtk_cell_renderer_text_new();
	gtk_tree_view_column_pack_start( column, renderer, FALSE );
	gtk_tree_view_column_set_attributes( column, renderer, "text", COL_FILENAME, NULL );
	gtk_tree_view_column_set_sizing( column, GTK_TREE_VIEW_COLUMN_FIXED );
	gtk_tree_view_append_column( list_tree_view, column );

	store = gtk_list_store_new( 2, G_TYPE_POINTER, G_TYPE_STRING );
	gtk_tree_view_set_model( list_tree_view, GTK_TREE_MODEL(store) );
	g_object_unref( store );

	g_signal_connect( G_OBJECT(list_tree_view), "row-activated", GTK_SIGNAL_FUNC(list_row_activated), NULL );

	scrolled_window = new_scrolled_window( GTK_POLICY_NEVER, GTK_POLICY_ALWAYS );
	gtk_scrolled_window_set_shadow_type( scrolled_window, GTK_SHADOW_NONE );
	gtk_widget_set_name( gtk_scrolled_window_get_vscrollbar( scrolled_window ), "GwenevieveScrollbar" );
	gtk_container_add( GTK_CONTAINER(scrolled_window), GTK_WIDGET(list_tree_view) );

	return GTK_WIDGET(scrolled_window);
}

void set_list_page( void )
{
	TRACE(( "set_list_page(): list_is_valid = %s\n", (list_is_valid) ? "TRUE" : "FALSE" ));

	if ( list_is_valid )
	{
		set_list_cursor( get_list_image_position( viewer_image ) );

		/*set_main_window_title( NULL );*/ set_selection_title();
		gtk_notebook_set_current_page( notebook, list_page );
	}
	else
	{
		GtkListStore *store;

		/* mark list as invalid */
		clear_list();

		/* clear list view */
		store = GTK_LIST_STORE( gtk_tree_view_get_model( list_tree_view ) );
		g_object_ref( store );
		gtk_tree_view_set_model( list_tree_view, NULL );

		/* set page */
		/*set_main_window_title( NULL );*/ set_selection_title();
		gtk_notebook_set_current_page( notebook, list_page );

		/* clear & fill list */
		busy_enter();
		g_idle_add( fill_list_1, store );
	}
}

gboolean fill_list_1( gpointer user_data )
{
	GtkListStore *store = (GtkListStore *)user_data;

	TRACE(( "-> fill_list()\n" ));

	gdk_threads_enter();

	set_list_title( -1 );
	gtk_list_store_clear( store );
	gtk_tree_view_set_model( list_tree_view, GTK_TREE_MODEL( store ) );
	tree_view_queue_resize( list_tree_view, 1 );

	gdk_threads_leave();

	g_idle_add( fill_list_2, store );
	return FALSE;
}

gboolean fill_list_2( gpointer user_data )
{
	GtkListStore *store = (GtkListStore *)user_data;
	gint position = 0, viewer_image_position = 0;
	const struct file *image;

	gdk_threads_enter();

	gtk_tree_view_set_model( list_tree_view, NULL );

	for ( image = get_first_file( selected_folder, selected_subfolder ); image != NULL; image = get_next_file( selected_folder, selected_subfolder, image ) )
	{
		char buffer[256];

		if ( image == viewer_image ) viewer_image_position = position;

		_get_file_name( buffer, sizeof( buffer ), selected_folder, image );
		gtk_list_store_insert_with_values( store, NULL, position++, COL_IMAGE_DATA, image, COL_FILENAME, buffer, -1 );
	}

	gtk_tree_view_set_model( list_tree_view, GTK_TREE_MODEL( store ) );
	g_object_unref( store );

	gtk_tree_view_set_enable_search( list_tree_view, TRUE );
	gtk_tree_view_set_search_column( list_tree_view, COL_FILENAME );

	set_list_title( position );

	list_is_valid = TRUE;
	set_list_cursor( viewer_image_position );

	busy_leave();
	gdk_threads_leave();

	TRACE(( "<- fill_list()\n" ));
	return FALSE;
}

void clear_list( void )
{
	/* Since gtk_list_store_clear() can take a notable amount of time,
	   we clear the list in fill_list() instead. */
	list_is_valid = FALSE;
}

void update_list( void )
{
	list_is_valid = FALSE;

	if ( gtk_notebook_get_current_page( notebook ) == list_page )
		set_list_page();
}

gboolean list_key_press_event( GtkWidget *widget, GdkEventKey *event )
{
	switch ( event->keyval )
	{
	case GDK_BackSpace:
	case GDK_Escape:
		set_viewer_page();
		break;

#if FALSE
	case GDK_Alt_L:  /* Hardkey on SmartQ devices */
		if ( IsSmartQ() )
		{
			event->keyval = GDK_Return;
			return FALSE;
		}
		break;
#endif

	default:
		return FALSE;
	}

	return TRUE;
}

/*-----------------------------------------------------------------------------------------------*/

struct file *get_list_image( void )
{
	GtkTreeSelection *selection = gtk_tree_view_get_selection( list_tree_view );
	GtkTreeModel *model;
	GtkTreeIter iter;

	if ( gtk_tree_selection_get_selected( selection, &model, &iter ) )
		return get_list_image_from_iter( model, &iter );

	return NULL;
}

void list_image_removed( struct file *image )
{
	if ( list_is_valid )
	{
		GtkListStore *store = GTK_LIST_STORE( gtk_tree_view_get_model( list_tree_view ) );
		GtkTreeModel *model = gtk_tree_view_get_model( list_tree_view );
		char path_str[16];
		GtkTreeIter iter;

		sprintf( path_str, "%d", get_list_image_position( image ) );
		if ( gtk_tree_model_get_iter_from_string( model, &iter, path_str ) )
			gtk_list_store_remove( store, &iter );
	}
}

/*-----------------------------------------------------------------------------------------------*/

static void _set_list_title( const char *title )
{
	gtk_tree_view_column_set_title( gtk_tree_view_get_column( list_tree_view, 0 ), title );
}

void set_list_title( gint count )
{
	gchar *name = ( selected_folder == NULL || (selected_folder == get_root_folder() && selected_subfolder) )
		? g_strdup( all_pictures )
		: get_folder_name( NULL, selected_folder, G_DIR_SEPARATOR_S );

	if ( count >= 0 )
	{
		gchar *title = g_strdup_printf( "%s [%d]", name, count );
		_set_list_title( title );
		g_free( title );
	}
	else
	{
		_set_list_title( name );
	}

	g_free( name );
}

void set_list_cursor( gint position )
{
	GtkTreePath *path;
	char path_str[16];

	sprintf( path_str, "%d", position );
	TRACE(( "set_list_cursor(): gtk_tree_view_set_cursor( %s )...\n", path_str ));

	path = gtk_tree_path_new_from_string( path_str );
	gtk_tree_view_set_cursor( list_tree_view, path, NULL, FALSE );
	gtk_tree_view_scroll_to_cell( list_tree_view, path, NULL, TRUE, 0.5, 0.0 );
	gtk_tree_path_free( path );

	gtk_widget_grab_focus( GTK_WIDGET( list_tree_view ) );
}

struct file *get_list_image_from_iter( GtkTreeModel *model, GtkTreeIter *iter )
{
	struct file *image = NULL;
	gtk_tree_model_get( model, iter, COL_IMAGE_DATA, &image, -1 );

	TRACE(( "get_list_image_from_iter() = %p\n", (gpointer)image ));
	return image;
}

gint get_list_image_position( const struct file *image )
{
	const struct file *i;
	gint position = 0;

	for ( i = get_first_file( selected_folder, selected_subfolder ); i != NULL; i = get_next_file( selected_folder, selected_subfolder, i ) )
	{
		if ( i == image )
		{
			TRACE(( "get_list_image_position() = %d\n", position ));
			return position;
		}

		position++;
	}

	TRACE(( "get_list_image_position() = %d\n", -1 ));
	return -1;
}

/*-----------------------------------------------------------------------------------------------*/

void list_row_activated( GtkTreeView *view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer user_data )
{
	GtkTreeModel *model = gtk_tree_view_get_model( view );
	GtkTreeIter iter;

	TRACE(( "# list_row_activated()\n" ));

	if ( gtk_tree_model_get_iter( model, &iter, path ) )
	{
		const struct file *image = get_list_image_from_iter( model, &iter );

		if ( image != viewer_image )
		{
			clear_viewer();
			set_viewer_page();
			update_viewer( image );
		}
		else
		{
			set_viewer_page();
		}
	}
}

/*-----------------------------------------------------------------------------------------------*/
