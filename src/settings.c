/*
	settings.c
	Copyright (C) 2009-2013 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)

	--------------------------------------------------------------------

	This file is part of the Gwenevieve application.

	Gwenevieve is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Gwenevieve is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gwenevieve.h"
#include <glib/gstdio.h>

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN  /* Exclude rarely-used stuff from Windows headers */
#include <windows.h>         /* for GetModuleFileName() */
#endif

static gint viewer_cache_sizes[] = { 0, 1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, -1 };

/*-----------------------------------------------------------------------------------------------*/

struct settings *settings;

static gchar *user_config_dirname;

static void cache_size_changed( GtkComboBox *widget, gpointer user_data );

/*-----------------------------------------------------------------------------------------------*/

static const char user_config_filename[] = "gwenevieve.ini";

gboolean load_settings( void )
{
	gchar *filename = build_user_config_filename( user_config_filename );
	gboolean result = TRUE;
	gchar *data;

	TRACE(( "load_settings()\n" ));

	settings = g_new0( struct settings, 1 );

	settings->screen.width  = gdk_screen_width();
	settings->screen.height = gdk_screen_height();

	settings->view.width = settings->screen.width;
	settings->view.height = settings->screen.height;
	settings->view.auto_rotate = FALSE;
	settings->view.scroll_zone = TRUE;

	if ( g_file_get_contents( filename, &data, NULL, NULL ) )
	{
		GKeyFile *key_file = g_key_file_new();

		if ( g_key_file_load_from_data( key_file, data, -1, G_KEY_FILE_NONE, NULL ) )
		{
			settings->db.path             = g_key_file_get_string( key_file, "Database", "Path", NULL );
			settings->db.location         = g_key_file_get_integer( key_file, "Database", "Location", NULL );
			settings->db.sorting          = g_key_file_get_integer( key_file, "Database", "Sorting", NULL );
			settings->db.offer_all        = g_key_file_get_integer( key_file, "Database", "OfferAll", NULL );
			settings->db.linear_mode      = g_key_file_get_boolean( key_file, "Database", "LinearMode", NULL );
			settings->db.random_start     = g_key_file_get_boolean( key_file, "Database", "RandomStart", NULL );

			settings->view.width          = g_key_file_get_integer( key_file, "View", "Width", NULL );
			settings->view.height         = g_key_file_get_integer( key_file, "View", "Height", NULL );
			settings->view.auto_rotate    = g_key_file_get_boolean( key_file, "View", "AutoRotate", NULL );
			settings->view.scale_up       = g_key_file_get_boolean( key_file, "View", "ScaleUp", NULL );
			settings->view.scroll_zone    = g_key_file_get_boolean( key_file, "View", "ScrollZone", NULL );
			settings->view.wrap_around    = g_key_file_get_boolean( key_file, "View", "WrapAround", NULL );
			settings->view.cache_size     = g_key_file_get_integer( key_file, "View", "CacheSize", NULL );
			settings->view.cache_prefetch = g_key_file_get_boolean( key_file, "View", "CachePrefetch", NULL );

			settings->view.pause_between_slides = g_key_file_get_integer( key_file, "View", "PauseBetweenSlides", NULL );
		}

		g_key_file_free( key_file );
	}

	g_free( filename );

	if ( settings->db.path == NULL )
	{
	#if GTK_MAJOR_VERSION == 2 && GTK_MINOR_VERSION < 14  /* g_get_user_special_dir() is available since 2.14 */
		settings->db.path = g_strdup( g_get_user_data_dir() );
	#else
		settings->db.path = g_strdup( g_get_user_special_dir( G_USER_DIRECTORY_PICTURES ) );
	#endif
		result = FALSE;  /* Needs to be corrected by user */
	}

	if ( settings->view.width <= 0 || settings->view.width > settings->screen.width ||
	     settings->view.height <= 0 || settings->view.height > settings->screen.height )
	{
		settings->view.width  = settings->screen.width;
		settings->view.height = settings->screen.height;
		result = FALSE;  /* Needs to be corrected by user */
	}

	return result;
}

gboolean save_settings( void )
{
	gchar *filename = build_user_config_filename( user_config_filename );
	GKeyFile *key_file = g_key_file_new();
	gboolean result = FALSE;
	gchar *data;

	TRACE(( "save_settings()\n" ));

	g_key_file_set_string ( key_file, "Database", "Path", (settings->db.path != NULL) ? settings->db.path : "" );
	g_key_file_set_integer( key_file, "Database", "Location", settings->db.location );
	g_key_file_set_integer( key_file, "Database", "Sorting", settings->db.sorting );
	g_key_file_set_integer( key_file, "Database", "OfferAll", settings->db.offer_all );
	g_key_file_set_boolean( key_file, "Database", "LinearMode", settings->db.linear_mode );
	g_key_file_set_boolean( key_file, "Database", "RandomStart", settings->db.random_start );

	g_key_file_set_integer( key_file, "View", "Width", settings->view.width );
	g_key_file_set_integer( key_file, "View", "Height", settings->view.height );
	g_key_file_set_boolean( key_file, "View", "AutoRotate", settings->view.auto_rotate );
	g_key_file_set_boolean( key_file, "View", "ScaleUp", settings->view.scale_up );
	g_key_file_set_boolean( key_file, "View", "ScrollZone", settings->view.scroll_zone );
	g_key_file_set_boolean( key_file, "View", "WrapAround", settings->view.wrap_around );
	g_key_file_set_integer( key_file, "View", "CacheSize", settings->view.cache_size );
	g_key_file_set_boolean( key_file, "View", "CachePrefetch", settings->view.cache_prefetch );

	g_key_file_set_integer( key_file, "View", "PauseBetweenSlides", settings->view.pause_between_slides );

	data = g_key_file_to_data( key_file, NULL, NULL );
	if ( data != NULL )
	{
		result = g_file_set_contents( filename, data, -1, NULL );
	}

	g_key_file_free( key_file );
	g_free( filename );
	return result;
}

/*-----------------------------------------------------------------------------------------------*/

#ifdef WIN32
static const char gwenevieve_dir[] = "Gwenevieve";
/* g_get_user_config_dir() = "C:\Documents and settings\User\Application Data" */
#else
static const char gwenevieve_dir[] = "gwenevieve";
/* g_get_user_config_dir() = "/home/user/.config" */
#endif

gchar *build_user_config_filename( const char *filename )
{
	if ( user_config_dirname == NULL )
	{
		user_config_dirname = g_build_filename( g_get_user_config_dir(), gwenevieve_dir, NULL );
		ASSERT( user_config_dirname != NULL );
		g_mkdir( user_config_dirname, 0777 );
	}

	return g_build_filename( user_config_dirname, filename, NULL );
}

/*-----------------------------------------------------------------------------------------------*/

#ifndef MAEMO

struct size
{
	gint width, height;
};

static GList *view_size_list;

static void append_view_size( GtkWidget *view_size, gint width, gint height )
{
	if ( width <= settings->screen.width && height <= settings->screen.height )
	{
		struct size *size;
		GList *l;

		for ( l = view_size_list; l != NULL; l = l->next )
		{
			size = (struct size *)l->data;
			if ( size->width == width && size->height == height ) break;
		}
		if ( l == NULL )
		{
			gchar *text = g_strdup_printf( "%dx%d", width, height );
			gtk_combo_box_append_text( GTK_COMBO_BOX(view_size), text );
			g_free( text );

			if ( width == settings->view.width && height == settings->view.height )
				gtk_combo_box_set_active( GTK_COMBO_BOX(view_size), g_list_length( view_size_list ) );

			size = g_new( struct size, 1 );
			size->width = width;
			size->height = height;
			view_size_list = g_list_append( view_size_list, size );
		}
	}
}

static void get_view_size( GtkWidget *view_size, gint *width, gint *height )
{
	if ( view_size != NULL )
	{
		gint n = gtk_combo_box_get_active( GTK_COMBO_BOX(view_size) );
		if ( n >= 0 )
		{
			GList *l = g_list_nth( view_size_list, n );
			if ( l != NULL )
			{
				struct size *size = (struct size *)l->data;
				*width = size->width;
				*height = size->height;
			}
		}
	}
}

static void free_view_size_list( void )
{
	g_list_foreach( view_size_list, (GFunc)g_free, NULL );
	g_list_free( view_size_list );
	view_size_list = NULL;
}

#endif

gboolean options( void )
{
	GtkDialog *dialog;
	GtkNotebook *notebook;
	GtkTable *table;

	GtkWidget *db_path, *db_location, *db_sorting, *db_offer_all, *db_linear_mode, *db_random_start;

#ifndef MAEMO
	GtkWidget *view_size;
#endif
	GtkWidget *view_auto_rotate, *view_scale_up, *view_scroll_zone, *view_wrap_around;
	GtkWidget *view_cache_size, *view_cache_prefetch;

	GtkBox *hbox;

	guint top_attach;
	gint  index, i;

	gboolean result = FALSE;

	TRACE(( "-> Options()\n" ));

	dialog = new_modal_dialog_with_ok_cancel( "Options", NULL );

	notebook = GTK_NOTEBOOK( gtk_notebook_new() );

	table = new_table( 5, 3 );
	gtk_notebook_append_page( notebook, GTK_WIDGET(table), GTK_WIDGET(gtk_label_new( "Database" )) );
	top_attach = 0;

	attach_to_table( table, new_label( "Picture folder:" ), top_attach, 0, 1 );

	db_path = gtk_file_chooser_button_new( "Select picture folder", GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER );
	gtk_file_chooser_set_filename( GTK_FILE_CHOOSER(db_path), settings->db.path );
	attach_to_table( table, db_path, top_attach++, 1, 3 );

	attach_to_table( table, new_label( "Database location:" ), top_attach, 0, 1 );
	db_location = gtk_combo_box_new_text();
	gtk_combo_box_append_text( GTK_COMBO_BOX(db_location), "- No database -" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(db_location), "User configuration directory" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(db_location), "Path" );
	gtk_combo_box_set_active( GTK_COMBO_BOX(db_location), settings->db.location );
	attach_to_table( table, db_location, top_attach++, 1, 3 );

	attach_to_table( table, new_label( "Offer `All Pictures':" ), top_attach, 0, 1 );
	db_offer_all = gtk_combo_box_new_text();
	gtk_combo_box_append_text( GTK_COMBO_BOX(db_offer_all), "No" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(db_offer_all), "At top of list" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(db_offer_all), "At bottom of list" );
	gtk_combo_box_set_active( GTK_COMBO_BOX(db_offer_all), settings->db.offer_all );
	attach_to_table( table, db_offer_all, top_attach++, 1, 3 );

	attach_to_table( table, new_label( "Sorting:" ), top_attach, 0, 1 );
	db_sorting = gtk_combo_box_new_text();
	gtk_combo_box_append_text( GTK_COMBO_BOX(db_sorting), "By Name" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(db_sorting), "By Size" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(db_sorting), "By Date" );
	gtk_combo_box_append_text( GTK_COMBO_BOX(db_sorting), "Shuffle" );
	gtk_combo_box_set_active( GTK_COMBO_BOX(db_sorting), settings->db.sorting );
	attach_to_table( table, db_sorting, top_attach++, 1, 3 );

	db_linear_mode = gtk_check_button_new_with_label( "Linear mode" );
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(db_linear_mode), settings->db.linear_mode );
	attach_to_table( table, db_linear_mode, top_attach, 1, 2 );

	db_random_start = gtk_check_button_new_with_label( "Random start" );
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(db_random_start), settings->db.random_start );
	attach_to_table( table, db_random_start, top_attach++, 2, 3 );

#ifdef MAEMO
	table = new_table( 4, 2 );
#else
	table = new_table( 5, 2 );
#endif
	gtk_notebook_append_page( notebook, GTK_WIDGET(table), GTK_WIDGET(gtk_label_new( "View" )) );
	top_attach = 0;

#ifndef MAEMO
	attach_to_table( table, new_label( "Size:" ), top_attach, 0, 1 );
	view_size = gtk_combo_box_new_text();
	append_view_size( view_size, 640, 480 );
	append_view_size( view_size, 800, 600 );
	append_view_size( view_size, 1024, 768 );
	append_view_size( view_size, 1152, 864 );
	append_view_size( view_size, 1280, 1024 );
	append_view_size( view_size, settings->screen.width, settings->screen.height );
	append_view_size( view_size, settings->view.width, settings->view.height );
	attach_to_table( table, view_size, top_attach++, 1, 2 );
#endif

	hbox = GTK_BOX(gtk_hbox_new( FALSE, SPACING * 2 ));

	view_auto_rotate = gtk_check_button_new_with_label( "Rotate image if reasonable" );
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(view_auto_rotate), settings->view.auto_rotate );
	gtk_box_pack_start( hbox, view_auto_rotate, FALSE, FALSE, 0 );

	view_scale_up = gtk_check_button_new_with_label( "Scale images up" );
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(view_scale_up), settings->view.scale_up );
	gtk_box_pack_start( hbox, view_scale_up, FALSE, FALSE, 0 );

	attach_to_table( table, GTK_WIDGET(hbox), top_attach++, 1, 2 );

	hbox = GTK_BOX(gtk_hbox_new( FALSE, SPACING * 2 ));

	view_scroll_zone = gtk_check_button_new_with_label( "Enable scroll zone" );
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(view_scroll_zone), settings->view.scroll_zone );
	gtk_box_pack_start( hbox, view_scroll_zone, FALSE, FALSE, 0 );

	view_wrap_around = gtk_check_button_new_with_label( "Wrap around" );
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(view_wrap_around), settings->view.wrap_around );
	gtk_box_pack_start( hbox, view_wrap_around, FALSE, FALSE, 0 );

	attach_to_table( table, GTK_WIDGET(hbox), top_attach++, 1, 2 );

	attach_to_table( table, new_label( "Cache:" ), top_attach, 0, 1 );
	view_cache_size = gtk_combo_box_new_text();
	gtk_combo_box_append_text( GTK_COMBO_BOX(view_cache_size), "- disabled -" );
	index = (settings->view.cache_size == 0) ? 0 : -1;
	for ( i = 1;; i++ )
	{
		gint cache_size = viewer_cache_sizes[i];
		char szT[16];

		if ( cache_size < 0 )
		{
			if ( index >= 0 ) break;
			cache_size = settings->view.cache_size;
		}
		if ( cache_size == settings->view.cache_size ) index = i;
		sprintf( szT, "%d Image%s", cache_size, (i == 1) ? "" : "s" );
		gtk_combo_box_append_text( GTK_COMBO_BOX(view_cache_size), szT );

		if ( viewer_cache_sizes[i] < 0 ) break;
	}
	gtk_combo_box_set_active( GTK_COMBO_BOX(view_cache_size), index );
	attach_to_table( table, view_cache_size, top_attach++, 1, 2 );

	view_cache_prefetch = gtk_check_button_new_with_label( "Prefetch" );
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(view_cache_prefetch), settings->view.cache_prefetch );
	attach_to_table( table, view_cache_prefetch, top_attach++, 1, 2 );

	cache_size_changed( GTK_COMBO_BOX(view_cache_size), view_cache_prefetch );
	g_signal_connect( G_OBJECT(view_cache_size), "changed", G_CALLBACK(cache_size_changed), view_cache_prefetch );

	gtk_box_pack_start_defaults( GTK_BOX(dialog->vbox), GTK_WIDGET(notebook) );
	gtk_widget_show_all( GTK_WIDGET(dialog) );
/*****/ gtk_widget_set_visible( view_cache_prefetch, FALSE );  /* THIS FEATURE IS NOT IMPLEMENTED YET */
	if ( gtk_dialog_run( dialog ) == GTK_RESPONSE_OK )
	{
		int cache_size;

		g_free( settings->db.path );

		settings->db.path             = gtk_file_chooser_get_filename( GTK_FILE_CHOOSER(db_path) );
		settings->db.location         = gtk_combo_box_get_active( GTK_COMBO_BOX(db_location) );
		settings->db.sorting          = gtk_combo_box_get_active( GTK_COMBO_BOX(db_sorting) );
		settings->db.offer_all        = gtk_combo_box_get_active( GTK_COMBO_BOX(db_offer_all) );
		settings->db.linear_mode      = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(db_linear_mode) );
		settings->db.random_start     = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(db_random_start) );

	#ifdef MAEMO
		settings->view.width          = settings->screen.width;
		settings->view.height         = settings->screen.height;
	#else
		get_view_size( view_size, &settings->view.width, &settings->view.height );
	#endif
		settings->view.auto_rotate    = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(view_auto_rotate) );
		settings->view.scale_up       = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(view_scale_up) );
		settings->view.scroll_zone    = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(view_scroll_zone) );
		settings->view.wrap_around    = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(view_wrap_around) );
		cache_size  = viewer_cache_sizes[gtk_combo_box_get_active( GTK_COMBO_BOX(view_cache_size) )];
		if ( cache_size >= 0 ) settings->view.cache_size = cache_size;
		settings->view.cache_prefetch = gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON(view_cache_prefetch) );

		save_settings();
		result = TRUE;
	}

#ifndef MAEMO
	free_view_size_list();
#endif
	gtk_widget_destroy( GTK_WIDGET(dialog) );

	TRACE(( "<- Options()\n" ));
	return result;
}

void cache_size_changed( GtkComboBox *widget, gpointer user_data )
{
	gint index = gtk_combo_box_get_active( GTK_COMBO_BOX(widget) );
	GtkWidget *view_cache_prefetch = GTK_WIDGET(user_data);

	gtk_widget_set_sensitive( view_cache_prefetch, index > 0 );
}

/*-----------------------------------------------------------------------------------------------*/
