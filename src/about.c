/*
	about.c
	Copyright (C) 2010 Axel Sommerfeldt (axel.sommerfeldt@f-m.fm)

	--------------------------------------------------------------------

	This file is part of the Gwenevieve application.

	Gwenevieve is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Gwenevieve is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Gwenevieve.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gwenevieve.h"

/*-----------------------------------------------------------------------------------------------*/

const char COMMENTS[] = "A simple image viewer";

const char LICENSE[] =
	"Gwenevieve is free software: You can redistribute it "
	"and/or modify it under the terms of the GNU "
	"General Public License as published by the Free "
	"Software Foundation, either version 3 of the "
	"License, or (at your option) any later version.\n"
	"\n"
	"Gwenevieve is distributed in the hope that it will be "
	"useful, but WITHOUT ANY WARRANTY; without "
	"even the implied warranty of MERCHANTABILITY "
	"or FITNESS FOR A PARTICULAR PURPOSE.\n"
	"\n"
	"See the GNU General Public License for more details:\n"
	"http://www.gnu.org/licenses/";

/*-----------------------------------------------------------------------------------------------*/

void about( GdkPixbuf *logo )
{
	GtkAboutDialog* dialog = GTK_ABOUT_DIALOG(gtk_about_dialog_new());

	gtk_about_dialog_set_logo( dialog, logo );
	gtk_about_dialog_set_name( dialog, NAME );
	gtk_about_dialog_set_version( dialog, VERSION );
	gtk_about_dialog_set_comments( dialog, COMMENTS );
	gtk_about_dialog_set_copyright( dialog, COPYRIGHT );
#ifdef WEBSITE
	gtk_about_dialog_set_website( dialog, WEBSITE );
	/*gtk_about_dialog_set_website_label( dialog, NAME " Homepage" );*/
#endif

	gtk_about_dialog_set_license( dialog, LICENSE );
	gtk_about_dialog_set_wrap_license( dialog, TRUE );

	gtk_dialog_run( GTK_DIALOG(dialog) );
	gtk_widget_destroy( GTK_WIDGET(dialog) );
}

/*-----------------------------------------------------------------------------------------------*/
